<?php
function get_brands()
{
    $terms = get_terms([
    'taxonomy' => 'yith_product_brand',
    'hide_empty' => false,
        ]);

if (!empty($terms)) {
    foreach ($terms as $term) {
        $thumb_id = get_term_meta($term->term_id, 'thumbnail_id', true);
        $term_img = wp_get_attachment_url($thumb_id);
        $term_img;
        $term->image = $term_img;
    }
    return $terms;
}
}
function get_product_by_brand($bid)
{
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "http://roomserviceq8.com/wp-api.php?action=brand_products&brand=$bid",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_POSTFIELDS => "",
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/x-www-form-urlencoded"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
return json_decode($response, true);	
}
if(isset($_GET['srch']))
$srch = $_GET['srch'];
$brands = get_brands();
$bra = array();
$products = array();
foreach($brands as $sing)
{

    $name = strtolower($sing->name);
    $term_id = $sing->term_id;
    if(strpos($name,$srch))
    {
    	$bra[] = $sing;
    	$products = array_merge(get_product_by_brand($term_id), $products);
    }
}
echo json_encode($products);


?>