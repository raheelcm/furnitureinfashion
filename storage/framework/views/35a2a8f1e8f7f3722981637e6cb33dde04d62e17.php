<?php $__env->startSection('script'); ?>
<script>
		$(document).ready(function(){
		  $(".menu-bars").click(function(){
			$(".nav-ul-wrapper").slideToggle();
		  });
		  
		});
		</script>
<!--Model-->
<script type="text/javascript">
	function confirm()
	{
		var pmethod = $("input[name='pmethod']:checked").val();
		if(pmethod == 'stripe')
		{
			var html = $('#formFields').html();
			$('#exampleModal').modal('show');
			
			$('#popupFields').html(html);
			return false;
		}
		else
		{
			return true;
			 $('#paypalForm').submit();
		}
	}
	function updateADress()
	{
		var address = $("input[name='address']:checked").val();
		var id = $('#addType').val();
		var red= "<?= url('/page/checkout') ?>";
		red=red+'?'+id+'='+address;
		window.location = red;


		var html = $('#addressHtml'+address).html();
		$('#'+id).val(address );
		$('.'+id).html(html);
		$("input[name='"+id+"']").val(address);
		$('#myModal').modal('hide');
	}
	function chngaddress(type)
	{

			$.get(BASE_URL+"/page/adress", function(data, status){
    $('#myModal').modal('show');
    $('#myModal .modal-body').html(data);
    $('#addType').val(type);

  });
	}
	function addmodal()
	{
		$('#modtitle').text('Add Address');
		$('#myModal2').modal('show');

	}
	function editmodal(address_id, entry_firstname,entry_lastname,entry_company,entry_street_address,entry_city,entry_postcode)
	{
		$( "input[name='address_id']" ).val(address_id);
		$( "input[name='entry_firstname']" ).val(entry_firstname);
		$( "input[name='entry_lastname']" ).val(entry_lastname);
		$( "input[name='entry_company']" ).val(entry_company);
		$( "input[name='entry_city']" ).val(entry_city);
		$( "input[name='entry_street_address']" ).val(entry_street_address);
		$( "input[name='entry_postcode']" ).val(entry_postcode);
		$('#modtitle').text('Edit Address');
		$('#myModal2').modal('show');

	}
	$('#addressForm').on("submit", "form", function(e){
    e.preventDefault();
    alert('it works!');
    return  false;
});
	function addAddress()
	{
		var datastring = $("#addressForm").serialize();
		var text = $('#modtitle').text();
		id = 'addShippingAddress';
		if(text == 'Edit Address')
			id = 'updateShippingAddress';

$.ajax({
    type: "POST",
   	url: BASE_URL+"/"+id,
    data: datastring,
    dataType: "json",
    success: function(data) {
    	$('#myModal2').modal('hide');
        chngaddress();
    },
    error: function() {
        alert('error handling here');
    }
});
		//addressForm
	}
</script>
<!---stripe script strt--->	
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});
</script>
<!---stripe script end--->	
<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog1" role="document" style="max-width: 500px;margin: 23px auto !important;">
			    <div class="modal-content1 bg-white" style="padding: 15px;">
			      <div class="modal-header1">
			        <h3 class="modal-title" id="exampleModalLabel" style="float: left;">Payment Details</h3>
			        <img class="payment_pic" style="margin-left: 88px;" src="http://i76.imgup.net/accepted_c22e0.png">
			      </div>
			      <div class="modal-body1">
			        <form role="form" action="<?php echo e(route('stripe.post')); ?>" method="post" class="require-validation"
                                                     data-cc-on-file="false"
                                                    data-stripe-publishable-key="<?php echo e(env('STRIPE_KEY')); ?>"
                                                    id="payment-form">
                                                    <span id="popupFields"></span>
                                                    <input type="hidden" name="pmethod" value="stripe">
					  <div class="form-group">
					    <label for="exampleInputEmail1">Name on Card</label>
					    <input
                                    class='form-control' size='4' type='text'>
					  </div>
					  <div class="form-group">
					    <label for="exampleInputPassword1">Card Number</label>
					    <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text'>
					  </div>
					  <div class="row">
					  	<div class="col-md-4">
					  		<div class="form-group">
							    <label for="exampleInputPassword1">CVC</label>
							    <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text'>
							</div>
					  	</div>
					  	<div class="col-md-4">
					  		<div class="form-group">
							    <label for="exampleInputPassword1">Expiriaton Month</label>
							    <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
							</div>
					  	</div>
					  	<div class="col-md-4">
					  		<div class="form-group">
							    <label for="exampleInputPassword1">Expiration Year</label>
							    <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
							</div>
					  	</div>
					  </div>
					  <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
					  <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
					</form>
			      </div>
			    </div>
			  </div>
			</div>			
				<div class="modal" id="myModal">
				  <div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

				      <!-- Modal Header -->
				    	<div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				     	</div>

				      <!-- Modal body -->
				        <div class="modal-body">
				        	<div class="alert alert-warning alert-dismissible hidden">
							<button type="button" class="close" data-dismiss="alert">×</button>
							The primary address cannot be deleted. Please set another address as the primary address and try again. 
							</div>
							<input type="hidden" id="addType">
							<form action="" method="">
							<h3>Address Book Entries</h3>
							<div class="row adress_block">
								<div class="addressblock" style="float: left; padding: 5px; margin: 5px;">Raheel shehzad<br> Khushab , punjab , pakistan<br> khushab, 41000<br> Vatican City State (Holy See)</div>
								<div class="add-icons col-md-12">
									<div class="addressicons" style="float: left; padding: 5px;">
										<a href="" data-toggle="modal" data-target="#myModal2"><span class="ui-icon ui-icon-pencil" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-pencil-alt"></i></span></a>
										<a href="" ><span class="ui-icon ui-icon-trash" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-trash-alt"></i></span></a>
									</div>
									<div class="addressradio" style="float: right; padding-right: 15px;">
										<input type="radio" checked="checked">
									</div>
									<div class="addressprimary" style="float: right; padding: 5px; font-style: italic;">(primary address)</div>
								</div>
								<div class="clear"></div>
								<div class="addressblock" style="float: left; padding: 5px; margin: 5px;">myhawaii.social<br> Marcia C Davis<br> 1700 Makiki St #105, Honolulu, HI. 96822<br> Honolulu, 96822<br> United Kingdom (Mainland UK Only)</div>
								<div class="add-icons col-md-12">
									<div class="addressicons" style="float: left; padding: 5px;">
										<a href="" data-toggle="modal" data-target="#myModal3"><span class="ui-icon ui-icon-pencil" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-pencil-alt"></i></span></a>
										<a href=""><span class="ui-icon ui-icon-trash" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-trash-alt"></i></span></a>
									</div>
									<div class="addressradio" style="float: right; padding-right: 15px;">
										<input type="radio" >
									</div>
								</div>
							</div>
							</form>
				        </div>

				      <!-- Modal footer -->
				      	<div class="contentText">
							<div style="float: right;">
								<a id="btn16" onclick="updateADress()"  class="btn btn-default simpletextbtn"> <span class="plusthick"></span> Update </a>
							</div>
							<div style="float: right;">
								<a id="btn16" onclick="addmodal()" class="btn btn-default simpletextbtn"> <span class="plusthick"></span> Add Address</a>
							</div> 
						</div>

				    </div>
				  </div>
				</div>
			<!-- The Modal end -->
			<!-- The Modal2 -->
			<div class="modal" id="myModal2">
			  	<div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

			      <!-- Modal Header -->
			      	<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				  	</div>

			      <!-- Modal body -->
			      	<div class="modal-body">
			        	<div id="AddressForm" class="ui-dialog-content ui-widget-content" style="width: 60%; min-height: 88px; max-height: none; height: auto; margin: auto;">
			        		<form  id="addressForm" method="">
			        			<h3 id="modtitle">Edit Address</h3>
			        			<input type="hidden" name="address_id">
			        			<p class="fields">
			        				<input type="text" name="entry_firstname" placeholder="First name"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_lastname" placeholder="Last name"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_company" placeholder="Company name" class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_street_address" placeholder="Street address"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text"  name="entry_city" placeholder="City"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text"   name="entry_postcode" placeholder="Postal code"  class="form-control">
			        			</p>
			        			<p class="fields hidden">
			        				<input type="text" value="Vatican City State (Holy See)"  class="form-control">
			        			</p>
			        			<div class="contentText">
									<div style="text-align: center;" id="addressFormSubmit">
										<a onclick="addAddress('updateShippingAddress')" class="btn btn-default simpletextbtn"> Continue </a>
									</div>
								</div>
			        		</form>
			        	</div>
			      	</div>

			      <!-- Modal footer -->
			      	

			    </div>
			  </div>
			</div>
			<!-- The Modal 2 -->
			<!-- The Modal2 -->
			<div class="modal" id="myModal3">
			  	<div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

			      <!-- Modal Header -->
			      	<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				  	</div>

			      <!-- Modal body -->
			      	<div class="modal-body">
			        	<div id="AddressForm" class="ui-dialog-content ui-widget-content" style="width: 60%; min-height: 88px; max-height: none; height: auto; margin: auto;">
			        		<form  action="" method="">
			        			<h3>Edit Address</h3>
			        			<p class="fields">
			        				<input type="text" value="Add"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="shehzad"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="" placeholder="Company name" class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="Khushab , punjab , pakistan"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="khushab"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="41000"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="Vatican City State (Holy See)"  class="form-control">
			        			</p>
			        			<div class="contentText">
									<div style="text-align: center;" id="addressFormSubmit">
										<a href="" id="btn16" class="btn btn-default simpletextbtn"> Continue </a>
									</div>
								</div>
			        		</form>
			        	</div>
			      	</div>

			      <!-- Modal footer -->
			      	

			    </div>
			  </div>
			</div>
			<!-- The Modal 2 -->
			<!-- The Modal2 -->
			<div class="modal" id="myModal4">
			  	<div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

			      <!-- Modal Header -->
			      	<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				  	</div>

			      <!-- Modal body -->
			      	<div class="modal-body">
			        	<div id="AddressForm" class="ui-dialog-content ui-widget-content" style="width: 60%; min-height: 88px; max-height: none; height: auto; margin: auto;">
			        		<form  action="" method="">
			        			<h3>NEW Address</h3>
			        			<p class="fields">
			        				<input type="text" value="Raheel"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="shehzad"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="" placeholder="Company name" class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="Khushab , punjab , pakistan"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="khushab"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="41000"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="Vatican City State (Holy See)"  class="form-control">
			        			</p>
			        			<div class="contentText">
									<div style="text-align: center;" id="addressFormSubmit">
										<a href="" id="btn16" class="btn btn-default simpletextbtn"> Continue </a>
									</div>
								</div>
			        		</form>
			        	</div>
			      	</div>

			      <!-- Modal footer -->
			      	

			    </div>
			  </div>
			</div>
			<!-- The Modal 2 -->
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets';
?>
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> Checkout</a>
					</div>

					<div class="col-md-12">
						<h1 class="tit">Checkout</h1>
						<div class="contentContainer" id="checkoutPage">
							<div class="checkout">
								<div id="cart" class="contentText">
									<div class="table-responsive">
										<form action="">
											<table class="table table-striped table-condensed">
												<tbody>
													<tr>
														<th class="bo-1 bo-t">Item</th>
														<th class="bo-1 bo-t">Item price</th>
														<th class="bo-1 bo-t">Quantity</th>
														<th class="bo-1 bo-t">Subtotal</th>
													</tr>
													<?php 
													$total = 0;
													 ?>

													<?php $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php 
$total = $total + ($sing->final_price*$sing ->customers_basket_quantity);
 ?>
													<tr class="bo-2">
														<td class="bo-1">
															<span class="td-img">
																<img width="100" height="80" src="<?= url('/'); ?>/<?php echo e($sing->products_id->products_image); ?>" class="img-responsive ">
															</span> <span class="td-t"><a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->products_id->slug); ?>"><?php echo e($sing->products_id->products_name); ?></a></span><br>
														</td>
														<td class="bo-1 price-pd" valign="top">£<?php echo e($sing->final_price); ?><br><div class=" hidden you-save offer red-color">You Saved £44.99
														</div>
														</td>
                            <td valign="top" class="bo-1">
                        <div class="input_pnum" style="text-align: center;">
                            <a onclick="updateCart(<?php echo e($sing->products_id->products_id); ?>,<?php echo e($sing->products_id->nprice); ?>,'minus')" ><i class="fas fa-minus-circle"></i></a>
<!--                               <input type="number" style="width: 80px;" > -->
                            <input type="number" name="cart_quantity[]" value="<?php echo e($sing ->customers_basket_quantity); ?>" id="product<?php echo e($sing->products_id->products_id); ?>" style="width: 60px;text-align: center;" min="0" readonly>
                            <a class="plus" onclick="updateCart(<?php echo e($sing->products_id->products_id); ?>,<?php echo e($sing->products_id->nprice); ?>,'add')" ><i class="fas fa-plus-circle"></i></a>
                        </div>
                        <div class="qty-box-buttons" ><span class="shopping-update hidden"><input type="button"  name="submit" value="update" onclick="updateCart(<?php echo e($sing->products_id->products_id); ?>,<?php echo e($sing->products_id->nprice); ?>)"></span><span class="shopping-update"><input type="button" onclick="deleteCart(<?php echo e($sing->products_id->products_id); ?>)" name="submit" value="remove"></span>
                        </div>
                     </td>
														<td class="bo-1">£<?php echo e($sing->final_price*$sing ->customers_basket_quantity); ?></td>
													</tr>
													<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</tbody>
											</table>
										</form>
									</div>
								</div>

								<div class="chk-del">
									<form method="post" onsubmit="return confirm()"  action="<?php echo e(url('/payment')); ?>">
										<div class="row">
											<div id="comments" class="contentText col-md-6">
												<div class="checkout-cs">Add Comments About Your Order</div>
												<div class="panel-b col-md-12">
													<textarea class="form-control" cols="60" rows="5"></textarea> 
												</div>
											</div>
											<div id="discounts" class="contentText col-md-6">
												<div class="checkout-cs">Paste Discount Code :</div>
												<div class="panel-b col-md-12  col-xs-12">
													<div class="form-group">
														<div class="col-md-12 col-sm-12 col-xs-12 dis-cod" style="padding:0px;">
															<input type="text" id="discount_code" placeholder="Apply Discount Code Here If Applicable" size="10" class="form-control">
															<button class="btn btn-default disct-code simpletextbtn" type="button"> <span class="triangle-1-e"></span>Apply</button>
															<div id="discount_code_status"></div>
														</div>
														<div class="cong-mess">
															<p class="more-got">Congratulations ! You have qualified for EXTRA £150 discount. Apply coupon <u>FIF150</u> in discount box.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div id="totals" class="contentText col-md-12">
												<div class="checkout-cs text-rgh">Total</div>
												<div class="savetotal1 hidden">Your Total Saving Today £1,319.18</div>
												<div id="totalsContent">
											 		<table cellpadding="2" cellspacing="0" border="0">
											 			<tbody>
											 				<tr>
																<td align="right" class="main">Sub-Total:</td>
																<td align="right" class="main">£<?php echo e($total); ?></td>
															</tr>
															<?php if($dcharges > 0): ?>
															<tr>
																<td align="right" class="main">Dilivery charges:</td>
																<td id="dcharge" align="right" class="main"><?php if($dcharges > 0): ?>
																	£<?php echo e($dcharges); ?>

																	<?php 
																	$total = $total +$dcharges;
																	 ?>
																	<?php endif; ?></td>
															</tr>
															<?php endif; ?>
															<tr>
																<td align="right" class="main">Total To Pay:</td>
																<td align="right" class="main"><strong>£<?php echo e($total); ?></strong></td>
															</tr>
														</tbody>
													</table>
												</div>
												<div id="discountContent">
												</div>
												<div class="clear"></div>
											</div>
										</div>
										<div class="row" style="margin-top: 20px;">
											<div id="shipping" class="contentText col-md-6">
												<div class="panel panel-primary">
													<div class="checkout-cs">
														<div class="left">Shipping Address:</div>
														<div class="right fn-hide">Shipping Method</div>
													</div>
													<div class="panel-b row">
														<div class="col-lg-5 col-sm-5 col-md-5 col-xs-5 fn-full">
															<div id="billingAddress" class="billAddress contentText">
															<?php echo e((isset($shipAddress))?$shipAddress->entry_firstname:''); ?> <?php echo e((isset($shipAddress))?$shipAddress->entry_lastname:''); ?><br> <?php echo e((isset($shipAddress))?$shipAddress->entry_street_address:''); ?>

															</div>
															<div id="changeShippingAddress" class="contentText">
																<a id="btn12"  onclick="chngaddress('shipAddress')" class="btn btn-default simpletextbtn"> <span class="home"></span> Change Address</a> </div>
														</div>
														<div class="checkout-cs fn-show">
															<div class="left">Shipping Method</div>
														</div>
														<div id="shippingRows" class="col-lg-7 col-md-7 col-sm-7 col-xs-7 fn-full">
															<div class="contentText">This is currently the only shipping method available to use on this order.</div>
															<div class="contentText">
																<table class="table2" width="100%">
																	<tbody>
																		<tr>
																			<td colspan="3"><strong>Delivery Charge&nbsp;</strong></td>
																		</tr>
																		<tr>
																			<td colspan="3"><strong>Please email admin@furnitureinfashion.net or contact 01204 521 121 for additional delivery charge to your destination</strong></td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
														<div class="clear">&nbsp;</div>
													</div>
												</div>
											</div>
											<div id="payment" class="contentText col-md-6">
												<div class="panel panel-primary">
													<div class="checkout-cs">
														<div class="left">Billing Address:</div>
														<div class="right fn-hide">Payment Method</div>
														<div class="clear"></div>
													</div>
													<div class="panel-b row">
														<div class="col-lg-5 col-sm-5 col-md-5 col-xs-5 fn-full">
															<div id="paymentAddress" class=" shipAddress contentText">
																<?php echo e((isset($billAddress))?$billAddress->entry_firstname:''); ?> <?php echo e((isset($billAddress))?$billAddress->entry_lastname:''); ?><br> <?php echo e((isset($billAddress))?$billAddress->entry_street_address:''); ?>

															</div>
															<div id="changePaymentAddress" class="contentText">
																<a id="btn13"  onclick="chngaddress('billAddress')" class="btn btn-default simpletextbtn"> <span class="home"></span> Change Address</a>
															</div>
														</div>
														<div class="checkout-cs fn-show">
															<div class="left">Payment Method</div>
														</div>
														<div id="paymentRows" class="col-lg-7 col-md-7 col-sm-7 col-xs-7 fn-full">
															<div class="contentText">This is currently the only payment method available to use on this order.</div>
															<div class="contentText">
																<table border="0" width="100%" cellspacing="0" cellpadding="2" id="paymentTable">
																	<tbody>
																		<tr id="defaultPayment" class="moduleRowSelected">
																			<td><strong>Debit Card, Credit Card, Amex, PayPal</strong></td>
																			<td align="right">
																				<input type="hidden" value="sage_pay_form">
																			</td>
																		</tr>
																	</tbody>
																</table>
															</div>
														</div>
														<div class="clear">&nbsp;</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="margin-bottom: 20px">
											<div class="payment col-md-12">
											<div class="payment-div text-rgh">Payments</div>
												<!-- <div class="savetotal1">Your Total Saving Today £1,319.18</div> -->
												<div class="clear"></div>
												<form action>
														<div class="left_payment">
															<div class="payment_img_div">
																<img src="https://designmodo.com/demo/checkout-panel/img/visa_logo.png " class="payment_img1 text-center">
																<img src="https://designmodo.com/demo/checkout-panel/img/mastercard_logo.png" class="payment_img2 text-center">
															</div>
															<br>
															<input type="radio" name="pmethod" value="stripe">Pay £340.00 with credit card
														</div>
														<div class="right_payment">
															<div class="payment_img_div">
																<img src="https://designmodo.com/demo/checkout-panel/img/paypal_logo.png" class="payment_img3 text-center">
															</div>
															<br>
															<input type="radio" name="pmethod" value="paypal" checked="true">Pay £340.00 with PayPal
														</div>
												
							</div>
										</div>
										<div class="row" style="margin-bottom: 20px">
											<div class="termscond col-lg-10 col-sm-9 col-xs-11" style="text-align: right; line-height:60px;">
												<input type="checkbox" value="1" name="agree"required="true">
												<div class="accp_terms right">
													I have read and accept the <a style="color:#ff0000;" href="">terms &amp; conditions</a>
												</div>
											</div>
											<div class="contentText col-lg-2 col-sm-3 col-xs-12" id="processCheckout">
												<span id="formFields">
													<input type="hidden" name="shipAddress" value="<?php echo e(session('shipAddress')); ?>">
													<input type="hidden" name="billAddress" value="<?php echo e(session('billAddress')); ?>">
													</span>
												<button id="cart_btn" type="submit"  class="btn btn-success simpletextbtn">
													<span class="glyphicon glyphicon-ok"></span> Confirm Order
												</button>
											</div>
										</div>
										</form>
										<div class="row cart-continue check">
											<div class="cont-shopping-cart">
												<a href="<?= url('/');?>"><i class="fas fa-chevron-left"></i> Continue Shopping</a>	
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>