
<html>
  <?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body class="">
    <div class="main">
      <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
      <?php echo $__env->yieldContent('content'); ?>
      
      <?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>


// When the user clicks on the button, scroll to the top of the document
function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}<?php if($errors->any()): ?>
     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
         alert("<?php echo e($error); ?>");
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 <?php endif; ?>
</script>
      <?php if($errors->any()): ?>
     <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <?php if(!empty($error)): ?>
      
         alert("<?php echo e($error); ?>");
         session(['error'=> ' ']);
<?php endif; ?>
     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
 <?php endif; ?>
 <?php if(isset($error)): ?>

 <?php endif; ?>
      <script type="text/javascript">
        function hidefooter()
        {
          $('#hidefooter').attr('style',' ');
          $('#hidefooter').css('display','none !important');
        }
        function hidefooter1()
        {
          $('#foot-cookie').hide();
          // $('#foot-cookie').attr('style',' ');
          $('#foot-cookie').css('display','none !important');
        }
      	var BASE_URL = '<?= url('/'); ?>';
      	function applyCopon(code)
        {

          var settings = {
  "url": BASE_URL+"/applyCopon",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded",
    "Cookie": "laravel_session=eyJpdiI6ImhXMGJBT1JydDJUREhOY3ppQm9YMGc9PSIsInZhbHVlIjoiNU9Pb1FWbkx0MGtFQytydFFES085Vkk5MXFKWmFFT0YxQUc2clZMZkhXcHhSRG96RCt4NENsZEFWOFg5cXVrRDZISjEwODBvYUlIQjQ5MTFhYjhiY2c9PSIsIm1hYyI6IjQxNzYxMzA1OGYyNTVjZGZlMjA5OTAwNjEyM2Y1NDcyNzAyZjBlYmY2MjQ4MDhmMmM3MjAxYzJkYmQyMjBkZWYifQ%3D%3D"
  },
  "data": {
    "code": code,
  }
};
 
$.ajax(settings).done(function (response) {
  response  = JSON.parse(response);
  if(response.success == 1)
  {
    setInterval(function(){ location.reload(); }, 3000);

    swal("Good job!", response.message, "success");

    // location.reload();
  }
  else
  {
    swal("Sorry!", response.message, "error");
  }
});
        }
        <?php if(Session::has('error')): ?>
        swal("Sorry!", '<?php echo e(Session::get('error')); ?>', "error");
<?php endif; ?>

  <?php if(isset($error)): ?>
        swal("Sorry!", '<?php echo e($error); ?>', "error");
        <?php endif; ?>
        function updateCart(id,price,type = 'add')
        {
          var qty = $('#product'+id).val();
          qty = parseInt(qty);
          if(type == 'add')
            qty= qty + 1;
          else{
            qty= qty - 1 ;
          }
          var settings = {
  "url": BASE_URL+"/updateCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded",
    "Cookie": "laravel_session=eyJpdiI6ImhXMGJBT1JydDJUREhOY3ppQm9YMGc9PSIsInZhbHVlIjoiNU9Pb1FWbkx0MGtFQytydFFES085Vkk5MXFKWmFFT0YxQUc2clZMZkhXcHhSRG96RCt4NENsZEFWOFg5cXVrRDZISjEwODBvYUlIQjQ5MTFhYjhiY2c9PSIsIm1hYyI6IjQxNzYxMzA1OGYyNTVjZGZlMjA5OTAwNjEyM2Y1NDcyNzAyZjBlYmY2MjQ4MDhmMmM3MjAxYzJkYmQyMjBkZWYifQ%3D%3D"
  },
  "data": {
    "products_id": id,
    "customers_basket_quantity": qty,
    "final_price": price
  }
};

$.ajax(settings).done(function (response) {
  if(response)
  {
    location.reload();
  }
});
        }
        function deleteCart(id)
        {
          var settings = {
  "url": BASE_URL+"/deleteFromCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  if(response['success'] == 1)
  {
    location.reload();
  }
});
        }
        function deleteShippingAddress(id)
        {
          var settings = {
  "url": BASE_URL+"/deleteShippingAddress",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "address_book_id": id
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  if(response['success'] == 1)
  {
    location.reload();
  }
});
        }
        function addToCart(id, price)
      	{
      		var settings = {
  "url": BASE_URL+"/addToCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id,
    "customers_basket_quantity": "1",
    "final_price": price
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  window.location.replace(response.url);
});
      	}
        function addToList(id)
        {
          var settings = {
  "url": BASE_URL+"/addToList",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  console.log(response);
  window.location.replace(response.url);
});
        }
        function showmenu(){
          $(".nav-ul-wrapper").toggle();
        }
      </script>
      <script>
$(document).ready(function(){
  // Add smooth scrolling to all links
  $("#top").on('click', function(event) {
    window.scrollTo({top: 0, behavior: 'smooth'});
        event.preventDefault();


    // Make sure this.hash has a value before overriding default behavior
    
  });
});
        //search code
        $("#srch_input").keyup(function(){
          var str = $( this).val();
var n = str.length;
if(n >= 2)
  {
    var form = new FormData();
form.append("searchValue", str);

var settings = {
  "url": BASE_URL+"/getSearchData",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Cookie": "laravel_session=eyJpdiI6IlppaVRlNU5CSHB3Ylk1MmVrYWs1MXc9PSIsInZhbHVlIjoiMDVSeE9QSmJhbEMrR21wNkd2U3dacGY5cGFhNHl0UGtETGd4Mlc3RlVETHBrZ21NSWM1aGtNb25ZaGVSZ05UZElUY2dORGY1OVRxMDE3ZDJVZ0dmY2c9PSIsIm1hYyI6ImM2NjMwYjQ3YmFlMDBjMTI5OWI3ZmZjMTAxYTQ5MzNiZWY1NzY1YWY2YzI3MWJiMDEzMWMzNWYxNDk4NDc0N2IifQ%3D%3D"
  },
  "processData": false,
  "mimeType": "multipart/form-data",
  "contentType": false,
  "data": form
};

$.ajax(settings).done(function (response) {
  var obj = JSON.parse(response);
  var all = '';
 var pro = obj['product_data']['products'];
  for (var i = 0; i < pro.length; i++) {
    // Iterate over numeric indexes from 0 to 5, as everyone expects.
    all = all+pro[i]['html'];
}
  console.log(all);
  $('.search_result ul').html(all);
});
  }
});
</script>
      <?php echo $__env->yieldContent('script'); ?>
    </div>
  </body>
</html>