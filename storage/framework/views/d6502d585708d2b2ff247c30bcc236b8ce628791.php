<?php
  $coupon = app('app\Http\Controllers\PageController')->getPages();
?>
<div class="container">
            <center><a href="#" class="paypal-pic"><img src="<?= url('/assets'); ?>/images/paypal.jpg"></a></center>
            <center><a href="#" class="paypal-pic"><img src="<?= url('/assets'); ?>/images/pay-icons.jpg"></a></center>
            
          </div>
          
          
          <div class="ftr-links">
            
            <div class="container">
              <div class="col-md-3 single-ftr">
              <h3>
                CUSTOMER SERVICE
              </h3>
              <ul>
                <?php $__currentLoopData = $coupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php if($sing->page_type == 'customer_services'): ?>
                <li><a href="<?php echo e(url('/page/')); ?>/<?php echo e($sing->slug); ?>"><i class="fas fa-caret-right"></i><?php echo e($sing->name); ?></a>
                <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </ul>
            </div>
            
              <div class="col-md-3 single-ftr">
                <h3>
                  POPULAR LINKS
                </h3>
                <ul>
                  <?php $__currentLoopData = $coupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($sing->page_type == 'popular_link'): ?>
                  <li><a href="<?php echo e(url('/page/')); ?>/<?php echo e($sing->slug); ?>"><i class="fas fa-caret-right"></i><?php echo e($sing->name); ?></a>
                  <?php endif; ?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>

              <div class="col-md-3 single-ftr">
                <h3>
                  About
                </h3>
                <ul>
                  <?php $__currentLoopData = $coupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($sing->page_type == 'about'): ?>
                  <li><a href="<?php echo e(url('/page/')); ?>/<?php echo e($sing->slug); ?>"><i class="fas fa-caret-right"></i><?php echo e($sing->name); ?></a>
                  <?php endif; ?>
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>

              <div class="col-md-3 single-ftr show-ftr">
                <div class="social">
                  <a href="#"><i class="fab fa-facebook-f"></i></a>
                  <a href="#"><i class="fab fa-instagram"></i></a>
                  <a href="#"><i class="fab fa-pinterest-p"></i></a>
                  <a href="#"><i class="fab fa-youtube"></i></a>
                  <a href="#"><i class="fab fa-twitter"></i></a>
                </div>
                <form>
                  <input type="text" placeholder="Enter email for newsletter" class="newsletter">
                  <input type="submit" class="newsletter1">
                </form>
              </div>
            </div>
            
          </div>
      <footer>
        <div class="container clearfix">
          <div class="ftr-text1">
            <p>
              Furniture in Fashion is one the largest and best online furniture store website in the UK trading since 2007. We offer affordable, modern & contemporary home décor products for the entire home. Based on a 3.5 acre site with a fully stocked furniture warehouse. Leading supplier of living room furniture, sets & packages. We specialise in white high gloss coffee tables, sideboards, side tables & lamp tables. Trendy corner sofa collections in leather, fabric & velvet. Unique armchairs. Wool & cotton rugs to compliment all interiors. Designer and trendy TV Stands, TV units & cabinets in glass, white high gloss & wood. Wall entertainment units with LED lights are a popular choice for saving save space in your home. Dining Table Sets, Extending dining sets with upholstered dining chairs are perfect for apartments and open plan design spaces. Bedroom sets, need a comfortable bed? We supply single, double, super king & queen frames with memory foam mattresses. Sliding Wardrobes with ample hanging rails!
              <br>
              <br>
              Office Desks & chairs, computer workstations allow you to work from home! So browse our amazing furniture collection today. Lighting latest styles are available for lamp, floor, wall & ceiling lights. In chrome, brass & metal. Storage need to get organised? We have shelving, boxes, cupboards & shoe cabinets! Chest of drawers in all styles. Affordable Bathroom furniture sets are available with basins & wall units. Discounts! So that we can offer the most competitive price in the market we have excellent discount vouchers! The more you spend the more you save! We have a Furniture Sale category where we offer warehouse furniture clearance stock from all departments. We offer free standard delivery to most of UK mainland on all items which include VAT. So, what you are waiting for? Buy now from one of the UK’s largest and best discount furniture outlet.
            </p>
          </div>
        </div>
        <div class="col-md-12 ftr-links pd00">
            <p>
              © 2020 Furniture in Fashion
            </p>
        </div>
        <a href="#" class="scrollup" id="top" style="display: inline;">Scroll</a>
        <div id="foot-cookie" style="padding: 40px 0px 0px;">
          <div id="cookie-law">
            <div class="container">
              <div class="cook-left col-sm-11 col-md-9" id="hidefooter1">
                In order to give you a better service we uses cookies. By continuing to browse the site you are agreeing to our use of 
                <a href="" >Privacy &amp; Cookie Policy</a>.
              </div>
              <div class="cook-right col-sm-3">
                <a class="agree-cookie-banner" href=""><span>I Agree</span></a>
                <a class="close-cookie-banner" onclick="hidefooter1()"><span>X</span></a>
              </div>
            </div>
          </div>
        </div>
        <div id="foot-cart-cookie" class="d-md-block" style="bottom: 38px;">
          <div id="cart-cookie-law">
            <div class="container">
              <div class="col-md-12 col-sm-12 col-xs-12 text-right no-p cart-cookie-right d-sm-none d-xs-none" style="display: block !important;" id="hidefooter">
                <?php
                    $cart = app('app\Http\Controllers\PageController')->getCart();
                  ?>
                You Have Some Items Left Into Your Shopping Cart : <a href="<?= url('page/cart'); ?>"> My Basket <span class="item-count ng-binding"> <span>(</span><span class="topbar-cartcount"><?php echo e(count($cart)); ?></span><span>)</span> </span></a><a class="footcart-close" style="margin-left: 19px;" onclick="hidefooter()">X</a>
              </div>
            </div>
          </div>
        </div>
      </footer>