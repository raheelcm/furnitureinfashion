<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets';
?>
<div class="body">
        <div class="container clearfix">
          <?php echo $__env->make('includes.coupon', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
          <div class="hist-a">
            <a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"><?php echo e($detail->categories_name); ?></a>
          </div>
          
          <div class="catog-t">
            <h3>
              <?php echo e($detail->long_head); ?>

            </h3>
            <p>
              <?php echo e($detail->short_detail); ?>

            </p>
          </div>
          
          <div class="col-md-12 pd0">
            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <!--<div class="col-md-3 s-thumb">
              <a href="<?php echo e(url('category')); ?>/<?php echo e($sing->slug); ?>">
                <img src="<?php echo e(url('/')); ?>/<?php echo e($sing->categories_image); ?>">
                <h2>
                  <?php echo e($sing->categories_name); ?>

                </h2>
              </a>
            </div>-->
            <div class="col-md-3 sss-thumb">
              <div class="cat-wrap">
                <a href="<?php echo e(url('category')); ?>/<?php echo e($sing->slug); ?>">
                  <img src="<?php echo e(url('/')); ?>/<?php echo e($sing->categories_image); ?>">
                  <p>
                    <?php echo e($detail->categories_name); ?>

                  </p>
                  
                </a>
                <button>SHOP NOW</button>
              </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            
          </div>
          
          
          
          <div class="  product-det">
            <h4>
              <b><?php echo e($detail->categories_name); ?></b>

            </h4>
            <p>
              <?php echo e($detail->full_detail); ?>

            </p>
            
            <button class="continue-btn">
              Continue Shopping
            </button>
            
          </div>
          
          
        </div>
      </div>          
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>