<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets';
?>
<div class="body">
				<div class="container clearfix">
					<?php echo $__env->make('includes.coupon', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> My Wish List</a>
					</div>

					<div class="bodyContent">

						<div class="row no-m new-div"></div>

						<div class="page-header">
							<h1>My Wish List</h1>
						</div>

						<div class="contentContainer">
							<form action="" method="" class="form-horizontal">
								<table class="whislisttable tableresposive table-striped">
									<thead>
										<tr>
											<th class="text-center hidden-xs">Image</th>
											<th>Product Name</th>
											<th>Quantity</th>
											<th class="text-center">Total Price</th>
											<th class="text-center">Select</th>
										</tr>
									</thead>
									<tbody>
										<?php $__currentLoopData = $list; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<tr>
											<td class="hidden-xs">
												<a href="">
													<img width="100" height="80" src="<?= url('/'); ?>/<?php echo e($sing->products_id->products_image); ?>" class="img-responsive ">
												</a>
											</td>
											<td class="text-left">
												<strong>
													<a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->products_id->slug); ?>"><?php echo e($sing->products_id->products_name); ?></a>
												</strong>
											</td>
											<td>
												<input type="text" value="1" style="width:60px; text-align:center;" class="form-control">
											</td>
											<td class="text-center">
												<?php if($sing->products_id->nprice): ?>
												<del>£<?php echo e($sing->products_id->products_price); ?></del>
												<?php endif; ?>
												 <span class="productSpecialPrice">£<?php echo e(($sing->products_id->nprice)?$sing->products_id->nprice:$sing->products_id->products_price); ?></span>
											</td>
											<td class="text-center">
												<input type="checkbox" value="30380">
											</td>
										</tr>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</tbody>
								</table>
								<hr>
								<p class="text-right"><strong>Total Wish List Value: £199.95</strong></p>
								<div class="row">
									<div class="col-sm-6 pagenumber hidden-xs">
									Displaying <strong>1</strong> to <strong>1</strong> (of <strong>1</strong> items on this wish list) </div>
									<div class="col-sm-6">
										<div class="pull-right pagenav">
											<ul class="pagination">
												<li class="left"><span>
													<i class="fas fa-caret-left"></i>
												</span></li>
												<li class="right"><span>
													<i class="fas fa-caret-right"></i>
												</span></li>
											</ul>
										</div>
										<span class="pull-right">Result Pages:</span>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-4" style="margin-top:10px;">
										<button id="cart_btn" type="submit" name="wlaction" class="btn btn btn-danger simpletextbtn"> <span class="glyphicon glyphicon-trash"></span> Delete Checked Items <span class="hidden-xs hidden-sm hidden-md"><br>From Wish List</span></button>
									</div>
									<div class="col-sm-4 text-center" style="margin-top:10px;">
										<button id="cart_btn" type="submit" name="wlaction" class="btn btn btn-primary simpletextbtn"> <span class="glyphicon glyphicon-refresh"></span> Update Item Quantities</button>
									</div>
									<div class="col-sm-4 text-right" style="margin-top:10px;">
										<button id="cart_btn" type="submit" name="wlaction" class="btn btn btn-success simpletextbtn"> <span class="glyphicon glyphicon-shopping-cart"></span> Add <span class="hidden-xs hidden-sm hidden-md">Checked Items<br></span> To Shopping Cart</button>
									</div>					
								</div>
								<br>
								<div class="alert alert-info">
										If you would like to email your wish list to multiple friends or family, just enter their name's and email's in each row. You don't have to fill every box up, you can just fill in for however many people to whom you want to email your wish list link. Then fill out a short message you would like to include in with your email in the text box provided. This message will be added to all of the emails you send. Click the Send Wish List button to send the emails.	
								</div>
								
								<div id="EmailList">
									<div class="form-group has-feedback">
										<label for="recvName" class="control-label col-sm-3">Name:</label>
										<div class="col-sm-6">
											<input type="text" name="friend[]" id="recvName" placeholder="Name" class="form-control"><span><span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span></span>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="recvEmail" class="control-label col-sm-3">Email:</label>
										<div class="col-sm-6">
											<input type="text" name="email[]" id="recvEmail" placeholder="Email" class="form-control"><span><span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span></span>
										</div>
									</div>
									<hr>
								</div>
								<div class="button_more text-right">
									<a class="btn btn-default" role="button" href="#"><span class="glyphicon glyphicon-plus"></span> Add New Recipient</a>
								</div>
								<hr>
								<div class="form-group has-feedback">
									<label for="wishMessage" class="control-label col-sm-3">Message:</label>
									<div class="col-sm-7">
										<textarea class="form-control" name="message" cols="45" rows="5" id="wishMessage" placeholder="Message"></textarea><span><span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span></span> </div>
								</div>
								<p class="text-right"><button id="cart_btn" type="submit" name="wlaction" value="email" class="btn btn-default simpletextbtn"> <span class="glyphicon glyphicon-envelope"></span> Send Wish List Email</button></p>

							</form>
						</div>

					</div>
						
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>