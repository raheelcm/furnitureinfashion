<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets';
?>
			 
			 
<div class="body">
				<div class="container clearfix">
					<?php echo $__env->make('includes.coupon', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> Order</a>
					</div>

						<h1 class="tit">Order Details</h1>
					<div class="col-md-12 pt-5 pb-5">
						<div class="order_div">
							<h3>New Orders</h3>
							<div class="order_table">
								<table class="table_order">
									<thead>
										<tr>
											<th>Name/Address/Phone</th>
											<th>Order</th>
											<th>Total</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody class="order_table_body">
										<?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
										<tr>
											<td style="padding-left: 18px;"> <p style="font-weight: 600;"><?php echo e($sing->customers_name); ?></p> <p style="font-weight: 600;"><?php echo e($sing->customers_street_address); ?></p> </td>
											<td style="padding-left: 19px;">
												<ul>
													<?php $__currentLoopData = $sing->data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
													<li style="font-weight: bold;"><?php echo e($pro->products_name); ?></li>
													 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
												</ul>
											</td>
											<td style="font-weight: bold;">£
<?php echo e($sing->order_price); ?></td>
											<td style="padding: 20px 0px;">
												<p class="order_radius"><?php echo e($sing->orders_status); ?></p>
											</td>
										</tr>
										<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
					
					
				</div>
			</div>











<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>