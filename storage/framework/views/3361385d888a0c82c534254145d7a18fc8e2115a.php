<?php
  $coupon = app('app\Http\Controllers\PageController')->getCoupons();
?>
<div class="banner-sale  d-md-block">
            <?php $__currentLoopData = $coupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($key%2 ==  0): ?>
            <div class="w-20 bg-red">
            <?php else: ?>
            <div class="w-20 bg-grey">
            <?php endif; ?>
              <a href="#" class="bg-clr-a">
                <div>
                  <?php if($sing->discount_type ==  'percent'): ?>
                  <span class="extra-1">Extra <?php echo e($sing->amount); ?>% Off</span>
                  <?php endif; ?>
                  <?php if($sing->discount_type ==  'fixed_cart'): ?>
                  <span class="extra-1">Extra £<?php echo e($sing->amount); ?> Off</span>
                  <?php endif; ?>
                  <?php if($sing->minimum_amount >  0): ?>
                  <span class="extra-2">Spend £<?php echo e($sing->minimum_amount); ?> or more</span>
                  <?php endif; ?>
                </div>
                <div>
                </div>
              </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <div class="w-20 bg-red daily" style="height:42px;">
                  <a href="<?= url('/category/special-products'); ?>" >
                    <div class="left-rta">
                      <span class="xtra-hthree" style="color:white;">Daily Deals</span>
                    </div>
                  </a>
                </div>
          </div>
  
               