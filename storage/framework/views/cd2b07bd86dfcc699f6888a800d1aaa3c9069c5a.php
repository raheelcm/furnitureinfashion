<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets/';
?>
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">Living Room Furniture</a>&nbsp &nbsp / &nbsp &nbsp <a href="#">Coffee Tables</a>&nbsp &nbsp / &nbsp &nbsp <a href="#">Glass Coffee Tables</a>
					</div>
					
					<div class="col-md-12 pd0">
						<div class="col-md-4 pd0">
							<div class="pro-img-wrap">
								<div class="img-wrapper">
									<img src="<?= url('/'); ?>/<?php echo e($product->products_image); ?>">
								</div>
							</div>
							<div class="photoset-row cols-5" style="clear: left; display: block; overflow: hidden; height: 78px;">
								<?php $__currentLoopData = $images; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<a href="<?php echo e(url('/')); ?>/<?php echo e($sing->image); ?>" class="group1 photoset-cell highres-link cboxElement" rel="pigallery" style="float: left; display: block; line-height: 0; box-sizing: border-box; width: 20%; padding-right: 0px;"><img src="<?php echo e(url('/')); ?>/<?php echo e($sing->image); ?>" width="550" height="550" class="img-responsive" id="piGalImg_2" style="width: 100%; height: auto; margin-top: 0px;">
								</a>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								
							</div>
							<center>
								<small>
									*All products come flat pack unless otherwise stated
								</small>
							</center>
						</div>
						<div class="col-md-8 pd0">
							<div class="det-wrap">
								<div class="pro-det-t">
									<h3>
										<?php echo e($product->products_name); ?>

									</h3>
									
								</div>
								<div class="new-section-detail"><span class="smallText code"><span class="prod_code_m hidden">Product Code : </span>WEL-003+WEL-005.fl</span>
									<div class="first-section sales_cat1">
										<div class="clearfix"></div>
										<?php if($product->nprice): ?>
										<div class="rrp-div">
											<div class="rrp">WAS <del>£<?php echo e($product->products_price); ?></del></div>
										</div>
										<?php endif; ?>
										<div class="clearfix"></div>
										<?php if($product->nprice): ?>
										<div class="you-save offer red-color">
											<ul>
												
												<li>
													<span class="detail_save">Saving</span><span class="detail_pri"> £<?php echo e($product->products_price - $product->nprice); ?> Today</span>
													<br>
												</li>
											</ul>
										</div>
										<?php endif; ?>
									</div>
									<div class="second-section sales_cat2">
										<div class="ourprice-div">
											<h2 class="rprice" style="font-size: 15px; font-family: arial; margin: 0px 0 0 0; text-align: center; font-weight: bold;display:inline-block;width:100%;">OUR PRICE TODAY</h2>
											<div class="clearfix"></div> <span class="pSpecialPrice">£<?php echo e(($product->nprice)?$product->nprice:$product->products_price); ?></span>
											<div class="clearfix"></div><span class="price_vat">(Price Includes VAT)</span></div>
										<div class="clear"></div>
										<div class="stock_last"><span>BUY NOW WHILE STOCK LASTS</span></div>
									</div>
									<div class="fourth-section"><span>+</span><img src="<?= $url; ?>images/e100_offer.png" title="Extra 100 OFF" alt="Extra 100 OFF"></div>
									
								</div>
								<div class="w-100" style="margin-top:30px;">
									<div class="extra-center-text">
										EXTRA SAVINGS TODAY !!!
									</div>
								</div>
								<div class="red-tic1">
										<p><img src="<?= $url; ?>images/tic.png"> Extra £25 Off ALL ORDERS £250 OR MORE : Use Coupon Code <a href="#">FIF25</a></p>							
										<p><img src="<?= $url; ?>images/tic.png"> Extra £25 Off ALL ORDERS £250 OR MORE : Use Coupon Code <a href="#">FIF25</a></p>							
										<p><img src="<?= $url; ?>images/tic.png"> Extra £25 Off ALL ORDERS £250 OR MORE : Use Coupon Code <a href="#">FIF25</a></p>							
										<p><img src="<?= $url; ?>images/tic.png"> Extra £25 Off ALL ORDERS £250 OR MORE : Use Coupon Code <a href="#">FIF25</a></p>							
										<p><img src="<?= $url; ?>images/tic.png"> Extra £25 Off ALL ORDERS £250 OR MORE : Use Coupon Code <a href="#">FIF25</a></p>							
								</div>
								<button class="r-btn" style="float:left; font-size:16px; margin-top:20px;" onclick="addToCart(<?php echo e($product->products_id); ?>, <?php echo e($product->nprice); ?>)">
										<i class="fas fa-shopping-cart"></i> add to cart
								</button>
								<img class="payyyyy" style="margin-top:20px;margin-bottom: 20px;" src="<?= $url; ?>images/paypal-credit-mobile-19.jpg">
								<br>
								<div class="main_div_both">
									<div class="wishtext">
										<button id="btn1" class="new-wish-desk" onclick="addToList(<?php echo e($product->products_id); ?>)">
											<span class="glyphicon glyphicon-heart"></span>
										Add to WishList
										</button>
									</div>
									<div class="pipe-sign"> | </div>
									<div class="social_book_marks">
										<div class="social_bookmarks_pinfo">
											<div class="hts_bookmarks">
												<b>Share this product</b>
												<!-- AddToAny BEGIN -->
										<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
										<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
										<a class="a2a_button_facebook"></a>
										<a class="a2a_button_twitter"></a>
										<a class="a2a_button_email"></a>
										</div>
										<script async src="https://static.addtoany.com/menu/page.js"></script>
										<!-- AddToAny END -->
												<div style="display: none;">
													<div class="social_div" style="float:right;">
														<a class="social-text nn" href=""><i class="fab fa-facebook-f"></i></a>
													</div>
													<div class="social_div" style="float:right;">
														<a class="social-text nn" href=""><i class="fab fa-instagram"></i></a>
													</div>
													<div class="social_div" style="float:right;">
														<a class="social-text nn" href=""><i class="fab fa-linkedin-in"></i></a>
													</div>
													<div class="social_div" style="float:right;">
														<a class="social-text nn" href=""><i class="fab fa-pinterest-p"></i></a>
													</div>
													<div class="social_div" style="float:right;">
														<a class="social-text nn" href=""><i class="fab fa-twitter"></i></a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>							
							</div>
							<div class="prodiscrp desc_desk" id="small-des">
									<p><?php echo $product->products_description; ?></p>
									<p style="display: none;">
										<b style="color:#000;">FEATURES:</b><br>
										<i class="fas fa-chevron-right"></i>Kenia Modern Sideboard In White High Gloss And 2 Doors And 3 Drawers<br>
										<i class="fas fa-chevron-right"></i>White High Gloss<br>
										<i class="fas fa-chevron-right"></i>Structure of particle board thickness 18mm<br>
										<i class="fas fa-chevron-right"></i>Facade and top in wood relief melamine Board thickness 10mm.<br>
										<i class="fas fa-chevron-right"></i>Solid wood legs<br>
										<i class="fas fa-chevron-right"></i>Aluminum like handles<br>
										<i class="fas fa-chevron-right"></i>It features 2 Doors And 3 Drawers, provides an ample of storage<br>
										<i class="fas fa-chevron-right"></i>Also available in Red<br>
										<i class="fas fa-chevron-right"></i>Matching range furniture also available<br>
										<i class="fas fa-chevron-right"></i>Ideal for your modern living room or dining room<br>
										<i class="fas fa-chevron-right"></i>Available at an affordable price
									</p>
									<p style="display: none;">
										<b style="color:#000;">DIMENSIONS:</b><br>
										Width: 150cm<br>
										Height: 70cm<br>
										Depth: 45cm
									</p>

								</div>
						</div>
						
						<div class="col-md-12 pd0">
						
							<center>
								<h2><b>
									FIF RELATED PRODUCTS
								</b></h2>
							</center>
							<?php $__currentLoopData = $related; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key <= 4): ?>
							<div class="col-md-3 pd0">
								<div class="pricing-s">
									<a href="#" class="price-a">
										<img src="<?= url('/'); ?>/<?php echo e($sing->products_image); ?>" class="pricing-s-img">
										<p style="padding:0px; border:none;">
											<?php echo e($sing->products_name); ?>

										</p>
										
									</a>
								
									<div class="price-tag">
										<div class="w-100 f-left" style="margin-bottom:20px;">
											<span class="tagg"><small>£</small><?php echo e($sing->products_price); ?></span><br>
											<?php if($sing->nprice): ?>
											<span class="tag-under">WAS</span>&nbsp <del class="tag-under">£<?php echo e(($sing->nprice)?$sing->nprice:$sing->products_price); ?></del>
											<?php endif; ?>
										</div>
										
									</div>

									<div class="btn-wrap">
										<a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->slug); ?>" class="l-btn">
											<i class="fas fa-heart"></i> View Details
										</a>
										<button class="r-btn" onclick="addToCart(<?php echo e($sing->products_id); ?>, <?php echo e(($sing->nprice)?$sing->nprice:$sing->products_price); ?>)">
											<i class="fas fa-shopping-cart"></i> add to cart
										</button>
									</div>
								</div>
							</div>
							<?php endif; ?>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
						</div>
					
					</div>
					
					
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>