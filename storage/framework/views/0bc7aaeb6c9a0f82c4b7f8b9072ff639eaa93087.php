
<html>
  <?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body>
    <div class="main">
      <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      
      <?php echo $__env->yieldContent('content'); ?>
      
      <?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <script type="text/javascript">
      	var BASE_URL = '<?= url('/'); ?>';
      	function deleteCart(id)
        {
          var settings = {
  "url": BASE_URL+"/deleteFromCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  if(response['success'] == 1)
  {
    location.reload();
  }
});
        }
        function addToCart(id, price)
      	{
      		var settings = {
  "url": BASE_URL+"/addToCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id,
    "customers_basket_quantity": "1",
    "final_price": price
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  window.location.replace(response.url);
});
      	}
        function addToList(id)
        {
          var settings = {
  "url": BASE_URL+"/addToList",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  console.log(response);
  window.location.replace(response.url);
});
        }
      </script>
      <?php echo $__env->yieldContent('script'); ?>
    </div>
  </body>
</html>