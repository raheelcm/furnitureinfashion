<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets/';
?>
<!-- banner -->
					<?php echo $__env->make('includes.coupon', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!-- link top -->
					<div class="hist-a container-fluid">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> Dining Table And Chairs Sets </a>&nbsp &nbsp / &nbsp &nbsp <a href="#">Dining Tables</a>
					</div>
	<!-- body top content -->
					<div class="no-m new-div">
						<div class="container-fluid no-p cat-textt unique-title">
								<div class="cat-tile line-t ss">
									<h1 class="unique-keyword">
									Dining Tables UK, Extending Glass, Wooden, Gloss &amp; Marble Dining Table
									</h1>
								</div>
								<div class="top-content-main-mob">Looking for contemporary dining tables? At Furniture in Fashion we have a wide range of high quality cheap dining tables at pocket friendly prices. Add style and comfort to your dining room with our modern dining tables available in glass, high gloss, wood and marble.</div>	
						</div>
							<!-- 1002 result -->
						<div class="container-fluid">
										<div class="product_list_pagi_ttx_new">
											<strong>1002</strong>&nbsp; RESULTS
										</div>
										<div class="sortby_form_new">
											<div class="sort-list-text">
												<p>Sort by : </p>
												<ul>
													<li><a href="https://www.furnitureinfashion.net/dining-tables/?sortBy=best_selling">Most Popular</a></li>
													<li><a <?php echo e(((isset($_GET['sort']) && $_GET['sort'] == "products_date_added")?'class="active"':'')); ?> href="<?=app('app\Http\Controllers\PageController')->sortUrl(array('sort'=>'products_date_added','type'=>'less'))?>">New Arrival</a></li>
													<li><a <?php echo e(((isset($_GET['sort']) && $_GET['sort'] == "nprice" && $_GET['type'] == "less")?'class="active"':'')); ?> href="<?=app('app\Http\Controllers\PageController')->sortUrl(array('sort'=>'nprice','type'=>'less'))?>">Price Low to High</a></li>
													<li><a <?php echo e(((isset($_GET['sort']) && $_GET['sort'] == "nprice" && $_GET['type'] == "greater")?'class="active"':'')); ?> href="<?=app('app\Http\Controllers\PageController')->sortUrl(array('sort'=>'nprice','type'=>'greater'))?>">Price High to Low</a></li>
												</ul>
											</div>
											<div class="pagi-prod-new">
												<div class="product_listing_pagination_new">
														<div class="product_listing_pagination">
															<ul>
															   <?php echo e($pagi->links()); ?>


															</ul>
													</div>
												</div>
											</div>
										</div>
						</div>
							<!-- 1002 result end-->				
							<!-- products -->
						<div class="container-fluid ptb-5">
								<div class="row products">
									<?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<div class="item list-group-item col-sm-3 col-xs-6 prodt_list new-produt-design">
										<div class="productHolder equal-height" style="height: 569px;"> 
									   		<div class="piGal no-gall pintogle">
									   			<a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->slug); ?>"><img width="300" height="300" src="<?php echo e(url('/')); ?>/<?php echo e($sing->products_image); ?>" class="img-responsive img-responsive thumbnail group list-group-image" itemprop="image"></a>
											</div>
											<div class="caption-daily">
												<div class="caption">
													<div class="clearfix">
													</div>
													  <h3 class="group inner list-group-item-heading">
													  	<a href=""><?php echo e($sing->products_name); ?></a> 
													  </h3>
													  <hr>
													  <div class="price_area">
													  	<div class="row price_div">
													  		<div class="price">
													  			<div class="fprice">
													  				£<?php echo e(($sing->nprice)?$sing->nprice:$sing->products_price); ?>

													  			</div>
													  			<?php if($sing->nprice): ?>
														  		<div class="new-rrp-div">
														  			<div class="new-rrp">RRP
														  			</div> 
														  			<del>£<?php echo e($sing->products_price); ?></del>
														  		</div>
														  		<?php endif; ?>
													  		</div>
													  		<div class="saving_area">
																<div class="save_listing_child">
																	<span class="detail_save">Saving</span>
																	<span class="detail_pri"> £179.99</span>
																	<span class="detail_today">Today</span>
																</div>
																</div>
													  	</div>
													  	<div class="offers_div">
													  		<img src="<?=$url;?>images/tik.png"><a class=" btn-default btn-sm wishlist-btn appendix btn131" href="">EXTRA £50 OFF ALL ORDERS OVER £500 </a>
													  	</div>
													  	<div class="offers_div">
													  		<img src="<?=$url;?>images/tik.png"><a class=" btn-default btn-sm wishlist-btn appendix btn131" href="">FREE STANDARD DELIVERY TO MOST OF UK </a>
													  	</div>
													  	<div class="btn_list">
													  		<div class="left_btn">
													  			<a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->slug); ?>"><i class="far fa-heart"></i> VIEW DETAILES</a>
													  		</div>
													  		<div class="right_btn">
													  			<a onclick="addToCart(<?php echo e($sing->products_id); ?>, <?php echo e(($sing->nprice)?$sing->nprice:$sing->products_price); ?>)"><i class="fas fa-shopping-cart"></i> Add to cart</a>
													  		</div>
													  	</div>
													  </div>
												</div>
											</div>  
										</div>
									</div>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</div>
						</div>
							<!-- produc end -->
							<!-- 1002 result -->
						<div class="container-fluid">
										<div class="product_list_pagi_ttx_new">
											<strong>1002</strong>&nbsp; RESULTS
										</div>
										<div class="sortby_form_new">
											<div class="sort-list-text">
												<p>Sort by : </p>
												<ul>
													<li><a class="sel_opt" href="https://www.furnitureinfashion.net/dining-tables/?sortBy=best_selling">Most Popular</a></li>
													<li><a href="https://www.furnitureinfashion.net/dining-tables/?sortBy=new">New Arrival</a></li>
													<li><a href="https://www.furnitureinfashion.net/dining-tables/?sortBy=price_from">Price Low to High</a></li>
													<li><a href="https://www.furnitureinfashion.net/dining-tables/?sortBy=price_to">Price High to Low</a></li>
												</ul>
											</div>
											<div class="pagi-prod-new">
												<div class="product_listing_pagination_new">
													<div class="product_listing_pagination"><ul><li><strong>1</strong></li><li><a href="https://www.furnitureinfashion.net/dining-tables/?page=2" class="pageResults" title=" Page 2 ">2</a></li><li><a href="https://www.furnitureinfashion.net/dining-tables/?page=3" class="pageResults" title=" Page 3 ">3</a></li><li><a href="https://www.furnitureinfashion.net/dining-tables/?page=2" class="prevnext" title=" Next Page "><img src="<?=$url;?>images/nav-arrow-right-16.png" alt=" Next Page "></a></li></ul></div>
													<span><a title="View All" href="https://www.furnitureinfashion.net/dining-tables/?display=all">VIEW ALL</a></span>
												</div>
											</div>
										</div>
						</div>
							<!-- 1002 result end-->	
							<!-- resently view -->
						<div class="resently_block">
							<div class="resently_heading text-center">
								<h1>Recently Viewed</h1>
							</div>
							<?php echo $__env->make('includes.recent', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
						</div>
							<!-- resently view end-->
						<hr>
							
							<!-- customer links -->
						<div class="col-md-12 ftr-links">
							<div class="col-md-3 single-ftr">
								<h3>
									Customer Service
								</h3>
								<ul>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
								</ul>
							</div>
							
							<div class="col-md-3 single-ftr">
								<h3>
									Customer Service
								</h3>
								<ul>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
								</ul>
							</div>
							
							<div class="col-md-3 single-ftr">
								<h3>
									Customer Service
								</h3>
								<ul>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
									<li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
								</ul>
							</div>
							
							<div class="col-md-3 single-ftr show-ftr">
								<div class="social">
									<a href="#"><i class="fab fa-facebook-f"></i></a>
									<a href="#"><i class="fab fa-instagram"></i></a>
									<a href="#"><i class="fab fa-pinterest-p"></i></a>
									<a href="#"><i class="fab fa-youtube"></i></a>
									<a href="#"><i class="fab fa-twitter"></i></a>
								</div>
								<form>
									<input type="text" placeholder="Enter email for newsletter" class="newsletter">
									<input type="submit" class="newsletter1">
								</form>
							</div>
							
						</div>
					
					
					</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>