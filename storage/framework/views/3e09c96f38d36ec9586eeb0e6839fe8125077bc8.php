<?php
  $coupon = app('app\Http\Controllers\PageController')->getPages();
?>
<ul>
  <?php $__currentLoopData = $coupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
<li><a class="here" href="<?php echo e(url('/page/')); ?>/<?php echo e($sing->slug); ?>"><i class="fas fa-caret-right"></i> <?php echo e($sing->name); ?></a></li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    