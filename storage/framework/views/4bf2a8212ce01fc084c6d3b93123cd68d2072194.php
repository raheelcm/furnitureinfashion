
<html>
  <?php echo $__env->make('includes.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <body>
    <div class="main">
      <?php echo $__env->make('includes.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <div class="body">
        <div class="container clearfix">
          <?php echo $__env->make('includes.coupon', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        
          <div class="hist-a">
            <a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> <?php echo e($pageTitle); ?></a>
          </div>

          <div class="bodycontant" style="margin-top: 24px;">
            <div class="col-sm-12 info-cms">
              <div class="col-sm-3 left-section">
                <div id="navigation" class="mini article-titles">
                  <?php echo $__env->make('includes.pages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
              </div>
              <div class="col-sm-9 right-section">
                <h1 class="right-heading here"><?php echo e($pageTitle); ?></h1>
                <?php echo $__env->yieldContent('content'); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <?php echo $__env->make('includes.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

      <script type="text/javascript">
      	var BASE_URL = '<?= url('/'); ?>';
      	function applyCopon(code)
        {

          var settings = {
  "url": BASE_URL+"/applyCopon",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded",
    "Cookie": "laravel_session=eyJpdiI6ImhXMGJBT1JydDJUREhOY3ppQm9YMGc9PSIsInZhbHVlIjoiNU9Pb1FWbkx0MGtFQytydFFES085Vkk5MXFKWmFFT0YxQUc2clZMZkhXcHhSRG96RCt4NENsZEFWOFg5cXVrRDZISjEwODBvYUlIQjQ5MTFhYjhiY2c9PSIsIm1hYyI6IjQxNzYxMzA1OGYyNTVjZGZlMjA5OTAwNjEyM2Y1NDcyNzAyZjBlYmY2MjQ4MDhmMmM3MjAxYzJkYmQyMjBkZWYifQ%3D%3D"
  },
  "data": {
    "code": code,
  }
};

$.ajax(settings).done(function (response) {
  response  = JSON.parse(response);
  if(response.success == 1)
  {
    setInterval(function(){ location.reload(); }, 3000);

    swal("Good job!", response.message, "success");

    // location.reload();
  }
  else
  {
    swal("Sorry!", response.message, "error");
  }
});
        }
        function updateCart(id,price)
        {
          var qty = $('#product'+id).val();
          var settings = {
  "url": BASE_URL+"/updateCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded",
    "Cookie": "laravel_session=eyJpdiI6ImhXMGJBT1JydDJUREhOY3ppQm9YMGc9PSIsInZhbHVlIjoiNU9Pb1FWbkx0MGtFQytydFFES085Vkk5MXFKWmFFT0YxQUc2clZMZkhXcHhSRG96RCt4NENsZEFWOFg5cXVrRDZISjEwODBvYUlIQjQ5MTFhYjhiY2c9PSIsIm1hYyI6IjQxNzYxMzA1OGYyNTVjZGZlMjA5OTAwNjEyM2Y1NDcyNzAyZjBlYmY2MjQ4MDhmMmM3MjAxYzJkYmQyMjBkZWYifQ%3D%3D"
  },
  "data": {
    "products_id": id,
    "customers_basket_quantity": qty,
    "final_price": price
  }
};

$.ajax(settings).done(function (response) {
  if(response)
  {
    location.reload();
  }
});
        }
        function deleteCart(id)
        {
          var settings = {
  "url": BASE_URL+"/deleteFromCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  if(response['success'] == 1)
  {
    location.reload();
  }
});
        }
        function addToCart(id, price)
      	{
      		var settings = {
  "url": BASE_URL+"/addToCart",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id,
    "customers_basket_quantity": "1",
    "final_price": price
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  window.location.replace(response.url);
});
      	}
        function addToList(id)
        {
          var settings = {
  "url": BASE_URL+"/addToList",
  "method": "POST",
  "timeout": 0,
  "headers": {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  "data": {
    "products_id": id
  }
};

$.ajax(settings).done(function (response) {
  response = JSON.parse(response);
  console.log(response);
  window.location.replace(response.url);
});
        }
      </script>
      <?php echo $__env->yieldContent('script'); ?>
    </div>
  </body>
</html>