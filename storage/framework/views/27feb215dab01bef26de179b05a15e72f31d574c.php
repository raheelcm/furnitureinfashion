<?php
  $coupon = app('app\Http\Controllers\PageController')->getRecentProducts();
?>
<div class="row resently_item">
                <?php $__currentLoopData = $coupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-md-2 col-xs-4">
                  <div class="item_div">
                    <div class="resently_itme_img">
                      <a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->slug); ?>"><img src="<?php echo e(url('/')); ?>/<?php echo e($sing->products_image); ?>" class="img-responsive img-responsive thumbnail group list-group-image"></a>
                    </div>
                    <div class="item_title text-center">
                      <a href=""><?php echo e($sing->products_name); ?></a>
                      <div class="amazingcarousel-price"> 
                        <div class="rrp_slider">WAS 
                          <del>£<?php echo e($sing->products_price); ?></del>
                        </div>£<?php echo e(($sing->nprice)?$sing->nprice:$sing->products_price); ?></div>
                    </div>
                  </div>
                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
              </div>