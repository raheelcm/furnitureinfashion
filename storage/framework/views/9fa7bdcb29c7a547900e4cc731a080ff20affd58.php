<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets/';
?>
<?php
  $coupon = app('app\Http\Controllers\PageController')->getCoupons();
?>
<div class="body">
				<div class="container clearfix">
					<?php echo $__env->make('includes.coupon', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">Cart Contents</a>
					</div>
					
					<div class="cart-t">
						<h3>
							Your Secure Cart
						</h3>
						<a href="<?php echo e(url('/page/checkout')); ?>" class="img-btn-cart">
							<img src="<?= $url;?>images/checkout-desk.png">
						</a>
					</div>
					
					
					<div class="col-md-12 pd0">
						<div class="table-wrap">
							<table class="table table-striped table-condensed">
								<tbody>
									<tr>
										<th class="bo-1 bo-t">Item</th>
										<th class="bo-1 bo-t">Item price</th>
										<th class="bo-1 bo-t">Quantity</th>
										<th class="bo-1 bo-t">Subtotal</th>
									</tr>
									<?php 
$total = 0
 ?>
									<?php $__currentLoopData = $cart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<?php 
$total = $total + ($sing->final_price*$sing ->customers_basket_quantity);
 ?>
									<tr class="bo-2">
										<td class="bo-1"><span class="td-img"><img width="100" height="80" src="<?= url('/'); ?>/<?php echo e($sing->products_id->products_image); ?>" class="img-responsive "></span> <span class="td-t"><a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->products_id->slug); ?>"><?php echo e($sing->products_id->products_name); ?></a></span>
											<br>
										</td>
										<td class="bo-1 price-pd" valign="top">£<?php echo e($sing->final_price); ?>

											<br>
											<div class=" hidden you-save offer red-color">You Saved £137.97</div>
										</td>
                    <td valign="top" class="bo-1">
                        <div class="input_pnum" style="text-align: center;">
                            <a onclick="updateCart(<?php echo e($sing->products_id->products_id); ?>,<?php echo e($sing->products_id->nprice); ?>,'minus')" ><i class="fas fa-minus-circle"></i></a>
<!--                               <input type="number" style="width: 80px;" > -->
                            <input type="number" name="cart_quantity[]" value="<?php echo e($sing ->customers_basket_quantity); ?>" id="product<?php echo e($sing->products_id->products_id); ?>" style="width: 60px;text-align: center;" min="0" readonly>
                            <a class="plus" onclick="updateCart(<?php echo e($sing->products_id->products_id); ?>,<?php echo e($sing->products_id->nprice); ?>,'add')" ><i class="fas fa-plus-circle"></i></a>
                        </div>
                        <div class="qty-box-buttons" ><span class="shopping-update hidden"><input type="button"  name="submit" value="update" onclick="updateCart(<?php echo e($sing->products_id->products_id); ?>,<?php echo e($sing->products_id->nprice); ?>)"></span><span class="shopping-update"><input type="button" onclick="deleteCart(<?php echo e($sing->products_id->products_id); ?>)" name="submit" value="remove"></span>
                        </div>
                     </td>
										<td class="bo-1" valign="top">£<?php echo e($sing->final_price*$sing ->customers_basket_quantity); ?></td>
									</tr>
									<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								</tbody>
							</table>
						</div>
					</div>
					
					<div class="container">
						<div class="cart-ex-d">
							<div class="col-md-8">
								<div class="code-paste">
									<label class="checkout-nw">Paste Discount Code :</label> <input type="text" placeholder="Apply Discount Code Here If Applicable"><button>Apply</button>
								</div>
								<div class="w-100">
									<div class="extra-center-text">
										EXTRA SAVINGS TODAY !!!
									</div>
								</div>
								<div class="red-tic1">
									<?php $__currentLoopData = $coupon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($sing->discount_type ==  'percent'): ?>
                  <div class="red-tic1">
                  <p><img src="<?= $url;?>images/tic.png"> Extra <?php echo e($sing->amount); ?>% Off ALL ORDERS £<?php echo e($sing->minimum_amount); ?> OR MORE : Use Coupon Code <a onclick="applyCopon('<?php echo e($sing->code); ?>')"><?php echo e($sing->code); ?></a></p></div>
                  <?php elseif($sing->discount_type ==  'fixed_cart'): ?>
                  <div class="red-tic1">
                  <p><img src="<?= $url;?>images/tic.png"> Extra £<?php echo e($sing->amount); ?> Off ALL ORDERS £<?php echo e($sing->minimum_amount); ?> OR MORE : Use Coupon Code <a onclick="applyCopon('<?php echo e($sing->code); ?>')"><?php echo e($sing->code); ?></a></p></div>
                  <?php endif; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>							
								</div>
							</div>
							<div class="col-md-4">
								<div class="hidden">
									<p>Congratulations ! You have qualified for EXTRA £100 discount. Apply coupon <u style="color:red;">FIF100</u> in discount box</p>
								</div>
								<div class="savetotal hidden">
									<p>You are £222.05 away to get £150 Off</p>
								</div>
								<div class="cart-total">
									<div class="savetotal1 hidden">
										Your Total Saving Today £766.77
									</div>
									<div class="t-wrap">
										<table cellspacing="0" cellpadding="2" border="0" summary="">
											<tbody>
												<tr>
													<td align="right" class="fon">Sub-Total:</td>
													<td align="right" class="fon">£<?php echo e($total); ?></td>
												</tr>
												<?php 
												$discount = 0;
												if(Session::has('discount'))
												{
													$discount = session('discount');
												}
												 ?>
												<tr>
													<td align="right" class="fon">Discount:</td>
													<td align="right" class="fon"><font color="#ff0000">- £<?php echo e($discount); ?></font></td>
												</tr>
												<tr>
													<td align="right" class="fon">Total To Pay:</td>
													<td align="right" class="fon"><strong>£<?php echo e($total- $discount); ?></strong></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<div class="col-md-12 mtop">
								<a href="<?= url('/');?>" style="float:left;" class="continue-btn">
									Continue Shopping
								</a>
								<a href="<?php echo e(url('/page/checkout')); ?>" class="img-btn-cart">
									<img src="<?= $url;?>images/checkout-desk.png">
								</a>
							</div>
						</div>
					
						<div class="col-md-12 cart_icon">
							<div class="cart_inner">
								<div class="cart_icon_left">
									<div class="cart_icon_name newic">We accept</div>
									<img src="<?= $url;?>images/we-accept.png">
								</div>
								<div class="cart_icon_right">
									<div class="cart_icon_free">Free Standard Delivery To Most Of UK Mainland</div>
									<div class="cart_icon_fast">Fast Delivery
									</div>
									<div class="cart_icon_returd">Easy Returns </div>

									<div class="cart_icon_support">Excellent customer support
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
				</div>
			</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>