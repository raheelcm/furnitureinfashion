<div class="col-md-12">
            <center><a href="#" class="paypal-pic"><img src="<?= url('/assets'); ?>/images/paypal.jpg"></a></center>
            <center><a href="#" class="paypal-pic"><img src="<?= url('/assets'); ?>/images/pay-icons.jpg"></a></center>
            
          </div>
          
          
          <div class="col-md-12 ftr-links">
            <div class="col-md-3 single-ftr">
              <h3>
                Customer Service
              </h3>
              <ul>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
              </ul>
            </div>
            
            <div class="col-md-3 single-ftr">
              <h3>
                Customer Service
              </h3>
              <ul>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
              </ul>
            </div>
            
            <div class="col-md-3 single-ftr">
              <h3>
                Customer Service
              </h3>
              <ul>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
                <li><a href="#"><i class="fas fa-caret-right"></i>Customer Service</a>
              </ul>
            </div>
            
            <div class="col-md-3 single-ftr show-ftr">
              <div class="social">
                <a href="#"><i class="fab fa-facebook-f"></i></a>
                <a href="#"><i class="fab fa-instagram"></i></a>
                <a href="#"><i class="fab fa-pinterest-p"></i></a>
                <a href="#"><i class="fab fa-youtube"></i></a>
                <a href="#"><i class="fab fa-twitter"></i></a>
              </div>
              <form>
                <input type="text" placeholder="Enter email for newsletter" class="newsletter">
                <input type="submit" class="newsletter1">
              </form>
            </div>
            
          </div>
<footer>
        <div class="container clearfix">
          <div class="ftr-text1">
            <p>
              Furniture in Fashion is one the largest and best online furniture store website in the UK trading since 2007. We offer affordable, modern & contemporary home décor products for the entire home. Based on a 3.5 acre site with a fully stocked furniture warehouse. Leading supplier of living room furniture, sets & packages. We specialise in white high gloss coffee tables, sideboards, side tables & lamp tables. Trendy corner sofa collections in leather, fabric & velvet. Unique armchairs. Wool & cotton rugs to compliment all interiors. Designer and trendy TV Stands, TV units & cabinets in glass, white high gloss & wood. Wall entertainment units with LED lights are a popular choice for saving save space in your home. Dining Table Sets, Extending dining sets with upholstered dining chairs are perfect for apartments and open plan design spaces. Bedroom sets, need a comfortable bed? We supply single, double, super king & queen frames with memory foam mattresses. Sliding Wardrobes with ample hanging rails!
              <br>
              <br>
              Office Desks & chairs, computer workstations allow you to work from home! So browse our amazing furniture collection today. Lighting latest styles are available for lamp, floor, wall & ceiling lights. In chrome, brass & metal. Storage need to get organised? We have shelving, boxes, cupboards & shoe cabinets! Chest of drawers in all styles. Affordable Bathroom furniture sets are available with basins & wall units. Discounts! So that we can offer the most competitive price in the market we have excellent discount vouchers! The more you spend the more you save! We have a Furniture Sale category where we offer warehouse furniture clearance stock from all departments. We offer free standard delivery to most of UK mainland on all items which include VAT. So, what you are waiting for? Buy now from one of the UK’s largest and best discount furniture outlet.
            </p>
          </div>    
          <div class="col-md-12 ftr-links pd00">
            <p>
              © 2020 Furniture in Fashion
            </p>
          </div>
        </div>
      </footer>