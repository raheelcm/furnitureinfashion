<?php $__env->startSection('content'); ?>
<?php
  $url = url('/').'/assets';
?>
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="container-fluid hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">  My Account</a>
					</div>
					
					<div class="cart-heading">
						<h1>
							My Account Information
						</h1>
					</div>
					
					<div class="contentContainer">
						<div class="row">
							<div class="col-sm-12 custoom-account">
								<div class="col-sm-4 account-div">
									<div class="account-text">
										<h2>Wish List</h2>
										<div class="contentText"> 
											<ul class="accountLinkList">
											 <li><span class="glyphicon glyphicon-heart"></span> <a href="">View the wish list I made</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-4 account-div">
									<div class="account-text">
										<h2>My Account</h2>
										<div class="contentText">
											<ul class="accountLinkList">
												<li><span class="glyphicon glyphicon-user"></span> <a href="">View or change account information.</a></li> <li><span class="glyphicon glyphicon-home"></span> <a href="">View or change entries in address book.</a></li> <li><span class="glyphicon glyphicon-cog"></span> <a href="">Change account password.</a></li> <li><span class="fa fa-trash"></span> <a href="">Delete Account</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-4 account-div">
									<div class="account-text">
										<h2>My Orders</h2>
										<div class="contentText">
											<ul class="accountLinkList">
												<li><span class="glyphicon glyphicon-shopping-cart"></span> <a href="">View the orders I have made.</a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-4 account-div">
									<div class="account-text">
										<h2>E-Mail Notifications</h2>
										<div class="contentText">
											<ul class="accountLinkList">
												<li><span class="glyphicon glyphicon-envelope"></span> <a href="">Subscribe or unsubscribe from newsletters.</a></li> <li><span class=""></span> <a href=""></a></li>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-sm-4 account-div">
									<div class="account-text">
										<h2>Quick Links</h2>
										<div class="contentText">
											<ul class="accountLinkList">
												<li><span class="glyphicon glyphicon-user"></span> <a href="">View or Create Ticket</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					
					
					
					
					
				</div>
			</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>