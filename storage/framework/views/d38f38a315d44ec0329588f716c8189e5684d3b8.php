<div class="alert alert-warning alert-dismissible hidden">
							<button type="button" class="close" data-dismiss="alert">×</button>
							The primary address cannot be deleted. Please set another address as the primary address and try again. 
							</div>
							<input type="hidden" id="addType">
							<form action="" method="">
							<h3>Address Book Entries</h3>
							<?php $__currentLoopData = $address; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<div class="row adress_block">
								<div class="addressblock" id="addressHtml<?php echo e($sing->address_book_id); ?>" style="float: left; padding: 5px; margin: 5px;"><?php echo e((isset($sing))?$sing->entry_firstname:''); ?> <?php echo e((isset($sing))?$sing->entry_lastname:''); ?><br> <?php echo e((isset($sing))?$sing->entry_street_address:''); ?></div>
								<div class="add-icons col-md-12">
									<div class="addressicons" style="float: left; padding: 5px;">
										<a onclick="editmodal('<?php echo e($sing->address_book_id); ?>','<?php echo e($sing->entry_firstname); ?>','<?php echo e($sing->entry_lastname); ?>','<?php echo e($sing->entry_company); ?>','<?php echo e($sing->entry_street_address); ?>','<?php echo e($sing->entry_city); ?>','<?php echo e($sing->entry_street_address); ?>','<?php echo e($sing->entry_postcode); ?>')"><span class="ui-icon ui-icon-pencil" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-pencil-alt"></i></span></a>
										<a href="" onclick="deleteShippingAddress(<?php echo e($sing->address_book_id); ?>)" ><span class="ui-icon ui-icon-trash" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-trash-alt"></i></span></a>
									</div>
									<div class="addressradio" style="float: right; padding-right: 15px;">
										<input type="radio" name="address"<?php if($key == 0): ?>checked="true"<?php endif; ?> value="<?php echo e($sing->address_book_id); ?>">
									</div>
									<?php if($key == 0): ?>
									<div class="addressprimary" style="float: right; padding: 5px; font-style: italic;">(primary address)</div>
									<?php endif; ?>
								</div>
								<div class="clear"></div>
							</div>
							<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</form>