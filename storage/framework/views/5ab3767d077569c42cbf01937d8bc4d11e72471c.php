<?php
  $url = url('/').'/assets/';
?>
<?php $__env->startSection('content'); ?>
<div class="body">
        <div class="container clearfix">
          <div class="banner-sale">
            <div class="w-20 bg-red">
              <a href="#" class="bg-clr-a">
                <div>
                  <span class="extra-1">Extra £25 Off</span>
                  <span class="extra-2">Spend £250 or more</span>
                </div>
              </a>
            </div>
            <div class="w-20 bg-grey">
              <a href="#" class="bg-clr-a">
                <div>
                  <span class="extra-1">Extra £25 Off</span>
                  <span class="extra-2">Spend £250 or more</span>
                </div>
              </a>
            </div>
            <div class="w-20 bg-red">
              <a href="#" class="bg-clr-a">
                <div>
                  <span class="extra-1">Extra £25 Off</span>
                  <span class="extra-2">Spend £250 or more</span>
                </div>
              </a>
            </div>
            <div class="w-20 bg-grey">
              <a href="#" class="bg-clr-a">
                <div>
                  <span class="extra-1">Extra £25 Off</span>
                  <span class="extra-2">Spend £250 or more</span>
                </div>
              </a>
            </div>
            <div class="w-20 bg-red">
              <a href="#" class="bg-clr-a">
                <div>
                  <span class="extra-1">Extra £25 Off</span>
                  <span class="extra-2">Spend £250 or more</span>
                </div>
              </a>
            </div>
          </div>
        
          <?php echo $__env->make('includes.banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          
          <div class="h1-cvr">
            <h1 class="grey-bar">
              Buy High Gloss, Glass, Wood Furniture & Sets For Living Room, Dining, Bedroom, Office, Hallway & More
            </h1>
          </div>
          
          <div class="col-md-12 pd0">
          	<?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-3 s-thumb">
              <a href="<?php echo e(url('category')); ?>/<?php echo e($sing->slug); ?>">
                <img src="<?php echo e(url('/')); ?>/<?php echo e($sing->categories_image); ?>">
                <h2>
                  <?php echo e($sing->categories_name); ?>

                </h2>
              </a>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
          
          <div class="col-md-12 pd0 box-s ">
          
          
            <div class="sale-title">
              <center>
                <h2>
                  Today's Daily Deals
                </h2>
              </center>
              <span class="view-all">
                <a href="#">
                  View All
                </a>
              </span>
            </div>
            <div class="owl-carousel owl-theme">
              <!--slider-->
              <!-- Item slider-->
            <div class="container-fluid">

              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                    <div class="carousel-inner">

                      
                        <?php $__currentLoopData = $specials; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php if($key == 0): ?>
                        <div class="item active">
                          <?php else: ?>
                      <div class="item">
                        <?php endif; ?>
                        <div class="col-xs-12 col-sm-6 col-md-2">
                          <div class="slide-div">
                      <img src="<?= url('/'); ?>/<?php echo e($sing->products_image); ?>"" class="img-responsive slider-img">
                      <div class="sale-bar">Deal Of The Day</div>
                        <p class="mini-text">
                          <a href="#" title="Elgin 6 Seater High Gloss Convertible Extendable Dining Set"><?php echo e($sing->products_name); ?></a>
                        </p>
                        <p class="rate-text">
                          <span>WAS<del> £1,<?php echo e($sing->products_price); ?> </del></span>&nbsp &nbsp<span>£1,<?php echo e($sing->nprice); ?></span>
                        </p>
                      <div class="btn-wrap">
                        <a href="<?php echo e(url('/product')); ?>/<?php echo e($sing->slug); ?>" class="l-btn">
                          <i class="fas fa-heart"></i> View Details
                        </a>
                        <button class="r-btn" onclick="addToCart(<?php echo e($sing->products_id); ?>, <?php echo e($sing->nprice); ?>)">
                          <i class="fas fa-shopping-cart" ></i> add to cart
                        </button>
                      </div>
                    </div>
                        </div>
                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                    </div>

                    <div id="slider-control">
                    <a class="left1 carousel-control" href="#itemslider" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                    <a class="right1 carousel-control" href="#itemslider" data-slide="next"><i class="fas fa-chevron-right"></i></a>
                  </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Item slider end-->
            </div>
          </div>
          
          
        </div>
      </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
<script type="text/javascript">
  $(document).ready(function(){

$('#itemslider').carousel({ interval: 3000 });

$('.carousel-showmanymoveone .item').each(function(){
var itemToClone = $(this);

for (var i=1;i<6;i++) {
itemToClone = itemToClone.next();

if (!itemToClone.length) {
itemToClone = $(this).siblings(':first');
}

itemToClone.children(':first-child').clone()
.addClass("cloneditem-"+(i))
.appendTo($(this));
}
});
});

</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>