<?php $__env->startSection('content'); ?>
<div class="main">
			<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">Login</a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> Password Forgotten</a>
					</div>
					
					<div class="col-md-6">
						<div class="cover-log-t">
							<div class="log-t">
								<h3>
									Got an FIF account?
								</h3>
								<p>
									Please sign in to your FIF account.
								</p>
							</div>	
							<form action=" <?php echo e(url('/login')); ?>" method="POST">
								 	<?php if(session('error')): ?>
			 	<div class="alert alert-danger">
				<?php echo e(session('error')); ?>

			</div>
			<?php endif; ?>

							<div class="log-t">
								
									<div class="w-100">
										<label>E-Mail Address</label>
										<input type="text" name="customers_email_address" autofocus="autofocus"  title="Enter Email" id="inputEmail"  placeholder="E-Mail Address" class="form-control">
									</div>
									<div class="w-100">
										<label>Password</label>
										<input type="password" name="customers_password" autofocus="autofocus" title="Password" id="Password" placeholder="Password" class="form-control">
									</div>
									<a href="#" class="forget-a">
										Forgot password?
									</a>
									<?php if(isset($_GET['rurl'])): ?>
									<input type="hidden" name="rurl" value="<?php echo e($_GET['rurl']); ?>">
									<?php endif; ?>
								
							</div>
							
							<button type="submit" class="login-b">
								Sign In
							</button>
							</form>
						</div>
					</div>
					
					<div class="col-md-6">
						<div class="cover-log-t">
							<div class="log-t">
								<h3>
									New Customer
								</h3>
								<p>
									If you dont have an account, you can create one now.
								</p>
							</div>	
							
							
							<a href="<?= url('page/signup'); ?>" class="login-b" style="float:left;">
								Create Account
							</a>
						</div>
					</div>
					
					
					
					
				</div>
			</div>
		</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>