<header>
        <div class="top-header">
          <div class="container">
            <div class="row">
              <div class="top-header-1 col-md-6">
                <p><i class="fa fa-phone" aria-hidden="true"></i> 
                  01204 792700&nbsp &nbsp/&nbsp &nbsp0208 1447867</p>&nbsp &nbsp<button>Contact us</button>
              </div>
              <div class="top-header-2  col-md-6">
              <ul>
                <?php if(Session::has('LoginUser')): ?>
                <li><a href="<?= url('page/account'); ?>">Hello <?php echo e(Session::get('LoginUser')->customers_firstname); ?></a></li>
                <li><a href="<?= url('page/account'); ?>">Account</a></li>
                <li><a href="#">wish List</a></li>
                <li><a href="<?= url('/logout'); ?>">Sign Out</a></li>
                <?php else: ?>
                <li><a href="<?= url('page/login'); ?>">Sign in</a></li>
                <li><a href="<?= url('page/signup'); ?>">Register</a></li>
                <li><a href="#">wish List</a></li>
                <?php endif; ?>
              </ul>
              <div class="greybar-basket">
                <a href="<?= url('page/cart'); ?>">
                  <?php
                    $cart = app('app\Http\Controllers\PageController')->getCart();
                  ?>
                  <i class="fas fa-shopping-cart"></i> My Basket (<?php echo e(count($cart)); ?>)
                </a>
              </div>
            </div>
            </div>
              
            
          </div>
        </div>
        <div class="container clearfix middle_header">
          <div class="logo col-md-4 col-sm-12">
            <a href="<?php echo e(url('/')); ?>"><img alt="logo" src="<?= url('/assets'); ?>/images/logo.png"></a>
          </div>  
          <div class="col-md-4 col-sm-12">
          </div>
          <div class="col-md-4 col-sm-12" style="padding:0;">
            <form method="get" action="<?= url('/search') ?>" >
              <div class="search-wrap">
                <div class="wrap-inner">
                  <input placeholder="Search by keyword e.g. TV Stand" name="text" id="srch_input" value="<?=(isset($_GET['text'])?$_GET['text']:'')?>" type="text">
                  <span>
                    <button type="submit">
                      <i class="fas fa-search"></i>
                    </button>
                  </span>
                </div>
                <div class="search_result">
                  <ul>
                    
                  </ul>
                  </div><!--search_result-->
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="navs">
          <div class="container">
            <nav>
              <i id="nav-btn" onclick="showmenu()" class="fas fa-bars menu-bars" aria-hidden="true"></i>
              <div class="nav-ul-wrapper">
                <ul>
                  <?php
                  $categories = app('app\Http\Controllers\PageController')->product_categories();

                  ?>
                  <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sing): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                  <?php if($key <= 9): ?>
                  <li><a href="<?php echo e(url('category')); ?>/<?php echo e($sing->slug); ?>"><?php echo e($sing->categories_name); ?></a></li>
                  <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </ul>
              </div>
            </nav>
          </div>
        </div>
      
      </header>