<?php
   
namespace App\Http\Controllers;
   
use Illuminate\Http\Request;
use Session;
use Stripe;
use DB;
use Hash;
use Redirect;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
   
class StripePaymentController extends Controller
{
    public function getCart(){
      return app('App\Http\Controllers\PageController')->getCart();
        $cart_data = array();
        $customers_id                           =   '::1';//Request::ip();
;       
        //$customers_id                         =   '29';
        
        $customers_basket = DB::table('customers_basket')->where('customers_basket.customers_ip', '=', $customers_id)->where('is_order',0)->get();
        // dd($customers_basket);
        $total_carts = count($customers_basket); 
        if($total_carts > 0){
            foreach($customers_basket as $customers_basket_data){
                
                $customers_basket_attribute = DB::table('customers_basket_attributes')
                    ->join('products_options', 'products_options.products_options_id','=','customers_basket_attributes.products_options_id')
                    ->join('products_options_values', 'products_options_values.products_options_values_id','=','customers_basket_attributes.products_options_values_id')
                    ->select('products_options.products_options_name as attribute_name','products_options_values.products_options_values_name as attribute_value')
                    ->get();
                        
                $customers_basket_data->attributes = $customers_basket_attribute;
                $cart_data[] = $customers_basket_data;
            }
        }
        return $customers_basket;       
        
    }
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripe()
    {
        return view('stripe');
    }
  
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function stripePost(Request $request)
    {
        return $data = $this->addToOrder();
    }
    //addToOrder
    public function addToOrder(){
        $uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
        $shipAddress = $_POST['shipAddress'];
        $billAddress = $_POST['billAddress'];
        $pmethod = $_POST['pmethod'];
        $user = DB::table('customers')->where('customers_id', '=', $uid)->first();
        $saddress = DB::table('address_book')->where('address_book_id', '=', $shipAddress)->first();
        if(!$saddress)
        {
            return back()->withError('Please select your shipping address');
        }
        $dcharges = 0;
        if(isset($saddress->entry_zone_id) && $saddress->entry_zone_id > 0)
            {
                $zone = DB::table('zones')->where('zone_id',$saddress->entry_zone_id)->first();
                $is_dilvery = $zone->is_dilvery;
                $dcharges = $zone->charges;
                if($is_dilvery == 0)
            return back()->withError('We do not diliver in your area.');
            }
        $baddress = DB::table('address_book')->where('address_book_id', '=', $shipAddress)->first();
        
        $date_added                             =   date('Y-m-d h:i:s');
        
        $customers_id                           =   $user->customers_id;
        $customers_telephone                    =   $user->customers_telephone;
        $customers_email_address                =   $user->customers_email_address; 
            
        $delivery_firstname                     =   $saddress->entry_firstname;
        $delivery_lastname                      =   $saddress->entry_lastname;
        $delivery_street_address                =   $saddress->entry_street_address;
        $delivery_suburb                        =   $saddress->entry_suburb;
        $delivery_city                          =   $saddress->entry_city;
        $delivery_postcode                      =   $saddress->entry_postcode;
        
        $delivery = DB::table('zones')->where('zone_name', '=', 1)->get();
        
        if(count($delivery)){
            $delivery_state                         =   $delivery[0]->zone_code;
        }else{
            $delivery_state                         =   'other';
        }
           
        $delivery_country                       =   $saddress->entry_country_id;
        
        $billing_firstname                      =   $baddress->entry_firstname;
        $billing_lastname                       =   $baddress->entry_lastname;
        $billing_street_address                 =   $baddress->entry_street_address;
        $billing_suburb                         =   $baddress->entry_suburb;
        $billing_city                           =   $baddress->entry_city;
        $billing_postcode                       =   $baddress->entry_postcode;
        
        $billing = DB::table('zones')->where('zone_name', '=', 1)->get();
        
        if(count($billing)){
            $billing_state                          =   $billing[0]->zone_code;
        }else{
            $billing_state                          =   'other';
        }
        
        $billing_country                        =   $baddress->entry_country_id;
        
        $payment_method                         =   $pmethod;
        $order_information                      =   array();
        $cart = $this->getCart();
        $price = 0;
        foreach ($cart as $key => $value) {
            $price += $value->customers_basket_quantity*$value->final_price;
        }
        if(session('discount'))
        {
            $discount = session('discount');
        }

        if(isset($discount))
        {
            $price = $price - $discount;
        }
        
        $cc_type                            =   '';//$request->cc_type;
        $cc_owner                           =   '';//$request->cc_owner;
        $cc_number                          =   '';//$request->cc_number;
        $cc_expires                         =   '';//$request->cc_expires;
        $last_modified                      =   date('Y-m-d H:i:s');
        $date_purchased                     =   date('Y-m-d H:i:s');
        $order_price                        =   $price;
        $shipping_cost                      =   $dcharges;
        $shipping_method                    =   '';
        $orders_status                      =   '';
        $orders_date_finished               =   '';//$request->orders_date_finished;
        $comments                           =   'comment';//$request->comments;
        //$currency                         =   $request->currency;
        $currency                           =   'euro';
        $currency_value                     =   '';
        //$products_tax                     =   $request->products_tax;
        
        //tax info
        $total_tax                          =   '';//$request->total_tax;
        
        $products_tax                       =   1;
        //coupon info
        $is_coupon_applied                  =   0;//$request->is_coupon_applied;
        if(isset($discount))
        {
        $is_coupon_applied                  =   1;//$request->is_coupon_applied;
    }
        
        if($is_coupon_applied==1){
            
            $code = array();    
            $coupon_amount = 0; 
            $exclude_product_ids = array();
            $product_categories = array();
            $excluded_product_categories = array();
            $exclude_product_ids = array();
            $coupons = array();
            if(isset($discount))
            {
                $coupon_amount    =     $discount;//$request->coupon_amount;
                $coupons[] = session('coupon');//Session::has('coupon');
            }
            
            foreach($coupons as $coupons_data){
                
                //update coupans        
                $coupon_id = DB::statement("UPDATE `coupons` SET `used_by`= CONCAT(used_by,',$customers_id') WHERE `code` = '".$coupons_data->code."'");
                
                //dd(DB::getQueryLog());
                
                //Log::useDailyFiles(storage_path().'/logs/debug.log');
                //Log::info(['coupon_query'=>DB::getQueryLog()]);
            
            }
            $code = json_encode($coupons);
            
        }else{
            $code                               =   '';
            $coupon_amount                      =   '';
        }
        
        
        //payment methods 
        $payments_setting = DB::table('payments_setting')->get();

        
        if($payment_method == 'paypal' or $payment_method == 'card_payment'){
            
            //braintree transaction with nonce
            /*$is_transaction  = '1';           # For payment through braintree
            $nonce           =   '';//$request->nonce;
            
            if($payments_setting[0]->braintree_enviroment == '0'){
                $braintree_environment = 'sandbox'; 
            }else{
                $braintree_environment = 'production';  
            }
            
            $braintree_merchant_id = $payments_setting[0]->braintree_merchant_id;
            $braintree_public_key  = $payments_setting[0]->braintree_public_key;
            $braintree_private_key = $payments_setting[0]->braintree_private_key;
            
            //brain tree credential
            require_once app_path('braintree/Braintree.php');
            //print_r($result);
            if ($result->success) 
            {
            //print_r("success!: " . $result->transaction->id);
            if($result->transaction->id)
                {
                    $order_information = array(
                        'braintree_id'=>$result->transaction->id,
                        'status'=>$result->transaction->status,
                        'type'=>$result->transaction->type,
                        'currencyIsoCode'=>$result->transaction->currencyIsoCode,
                        'amount'=>$result->transaction->amount,
                        'merchantAccountId'=>$result->transaction->merchantAccountId,
                        'subMerchantAccountId'=>$result->transaction->subMerchantAccountId,
                        'masterMerchantAccountId'=>$result->transaction->masterMerchantAccountId,
                        //'orderId'=>$result->transaction->orderId,
                        'createdAt'=>time(),
//                      'updatedAt'=>$result->transaction->updatedAt->date,
                        'token'=>$result->transaction->creditCard['token'],
                        'bin'=>$result->transaction->creditCard['bin'],
                        'last4'=>$result->transaction->creditCard['last4'],
                        'cardType'=>$result->transaction->creditCard['cardType'],
                        'expirationMonth'=>$result->transaction->creditCard['expirationMonth'],
                        'expirationYear'=>$result->transaction->creditCard['expirationYear'],
                        'customerLocation'=>$result->transaction->creditCard['customerLocation'],
                        'cardholderName'=>$result->transaction->creditCard['cardholderName']
                    );

                    //print_r($order_information);
                    $payment_status = "success";
                    //print_r($result->transaction->status);
                }
                $payment_status = "success";
            } 
            else
                {
                    $payment_status = "failed";
                }*/
                $payment_status = "success";
                
        }else if($payment_method == 'stripe'){              #### stipe payment
            
            require_once app_path('stripe/config.php');
            
            //get token from app
            $token  = $_POST['stripeToken'];//$request->nonce;
            
            /*$customer = \Stripe\Customer::create(array(
              'email' => $customers_email_address,
              'source'  => $token
            ));*/
          
          if($order_price > 0)
          {
            try {
  Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
            $charge = Stripe\Charge::create ([
                "amount" => $order_price * 100,
                "currency" => "usd",
                "source" => $token,
                "description" => "Test payment from furniture website." 
        ]);
}

//catch exception
catch(Exception $e) {
  Session::flash('error', "Special message goes here");
        return back()->withError('Some error occur, sorry for inconvenient 291');
}
          }
          else{
            Session::flash('error', "Special message goes here");
        return back()->withError('Some error occur, sorry for inconvenient 296');
          }
            
             if($charge->paid == true){
               
                 $order_information = array(
                        'paid'=>'true',
                        'transaction_id'=>$charge->id,
                        'type'=>$charge->outcome->type,
                        'balance_transaction'=>$charge->balance_transaction,
                        'status'=>$charge->status,
                        'currency'=>$charge->currency,
                        'amount'=>$charge->amount,
                        'created'=>date('d M,Y', $charge->created),
                        'dispute'=>$charge->dispute,
                        'customer'=>$charge->customer,
                        'address_zip'=>$charge->source->address_zip,
                        'seller_message'=>$charge->outcome->seller_message,
                        'network_status'=>$charge->outcome->network_status,
                        'expirationMonth'=>$charge->outcome->type
                    );
        
                    //print_r($order_information);
                    $payment_status = "success";
                    
             }else{
                    $payment_status = "failed";  
             }
            
        }else if($payment_method == 'cash_on_delivery'){
            
            $payment_method = 'Cash on Delivery';
            $payment_status='success';
            
        } else if($payment_method == 'simplePaypal'){
            
            $payment_method = 'PayPal Express Checkout';
            $payment_status='success';
            $order_information = $request->nonce;
                
        } 
        
        // print $payment_status;
        //check if order is verified
        if($payment_status=='success'){
            //DB::enableQueryLog();
            //update database
            $orders_id = DB::table('orders')->insertGetId(
                [    'customers_id' => $customers_id,
                     'customers_name'  => $delivery_firstname.' '.$delivery_lastname,
                     'customers_street_address' => $delivery_street_address,
                     'customers_suburb'  =>  $delivery_suburb,
                     'customers_city' => $delivery_city,
                     'customers_postcode'  => $delivery_postcode,
                     'customers_state' => $delivery_state,
                     'customers_country'  =>  $delivery_country,
                     'customers_telephone' => $customers_telephone,
                     'customers_email_address'  => $customers_email_address,
                    // 'customers_address_format_id' => $delivery_address_format_id,
                     
                     'delivery_name'  =>  $delivery_firstname.' '.$delivery_lastname,
                     'delivery_street_address' => $delivery_street_address,
                     'delivery_suburb'  => $delivery_suburb,
                     'delivery_city' => $delivery_city,
                     'delivery_postcode'  =>  $delivery_postcode,
                     'delivery_state' => $delivery_state,
                     'delivery_country'  => $delivery_country,
                    // 'delivery_address_format_id' => $delivery_address_format_id,
                     
                     'billing_name'  => $billing_firstname.' '.$billing_lastname,
                     'billing_street_address' => $billing_street_address,
                     'billing_suburb'  =>  $billing_suburb,
                     'billing_city' => $billing_city,
                     'billing_postcode'  => $billing_postcode,
                     'billing_state' => $billing_state,
                     'billing_country'  =>  $billing_country,
                     //'billing_address_format_id' => $billing_address_format_id,
                     
                     'payment_method'  =>  $payment_method,
                     'cc_type' => $cc_type,
                     'cc_owner'  => $cc_owner,
                     'cc_number' =>$cc_number,
                     'cc_expires'  =>  $cc_expires,
                     'last_modified' => $last_modified,
                     'date_purchased'  => $date_purchased,
                     'order_price'  => $order_price,
                     'shipping_cost' =>$shipping_cost,
                     'shipping_method'  =>  $shipping_method,
                    // 'orders_status' => $orders_status,
                     //'orders_date_finished'  => $orders_date_finished,
                     'currency'  =>  $currency,
                     'currency_value' => $last_modified,
                     'order_information' => json_encode($order_information),
                     'coupon_code'       =>     $code,
                     'coupon_amount'     =>     $coupon_amount,
                     'total_tax'         =>     $total_tax,
                ]);
          $orders_status = 1;
            $comments = 'Order created for Stripe Payment';
             $orders_history_id = DB::table('orders_status_history')->insertGetId(
                [    'orders_id'  => $orders_id,
                     'orders_status_id' => $orders_status,
                     'date_added'  => $date_added,
                     'customer_notified' =>'1',
                     'comments'  =>  $comments
                ]);
            
             //orders status history
            
                
                //dd(DB::getQueryLog());
                
                /*$query = DB::getQueryLog();
                print_r($query);*/
                $cart = $this->getCart();
                
             foreach($cart as $products){  
                
                $pid = $products->products_id;
                $product = $this->getSingleProducts($pid);

                $orders_products_id = DB::table('orders_products')->insertGetId(
                [               
                     'orders_id'         =>     $orders_id,
                     'products_id'       =>     $pid,
                     'products_name'     =>     $product->products_name,
                     'products_price'    =>     $products->final_price,
                     'final_price'       =>     $products->final_price*$products->customers_basket_quantity,
                     'products_tax'      =>     0,
                     'products_quantity' =>     $products->customers_basket_quantity,
                ]);
                $cartId= $products->customers_basket_id;
                DB::table('customers_basket')
            ->where('customers_basket_id', $cartId)
            ->update(['is_order' => 1]);
                 
                
                /*if(!empty($products->attributes)){
                    foreach($products['attributes'] as $attribute){
                        DB::table('orders_products_attributes')->insert(
                        [
                             'orders_id' => $orders_id,
                             'products_id'  => $products['products_id'],
                             'orders_products_id'  => $orders_products_id,
                             'products_options' =>$attribute['products_options'],
                             'products_options_values'  =>  $attribute['products_options_values'],
                             'options_values_price'  =>  $attribute['options_values_price'],
                             'price_prefix'  =>  $attribute['price_prefix']
                        ]);
                        
                        
                    }
                }*/
                            
             }
          
            $responseData = array('success'=>'1', 'data'=>$orders_id, 'message'=>"Order has been placed successfully.");
            
            //send order email to user
            
            $order = DB::table('orders')
                ->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
                ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=' ,'orders_status_history.orders_status_id')
                ->where('orders.orders_id', '=', $orders_id)->orderby('orders_status_history.date_added', 'DESC')->get();
            
        //foreach
        foreach($order as $data){
            $orders_id   = $data->orders_id;
            
            $orders_products = DB::table('orders_products')
                ->join('products', 'products.products_id','=', 'orders_products.products_id')
                ->select('orders_products.*', 'products.products_image as image')
                ->where('orders_products.orders_id', '=', $orders_id)->get();
                $i = 0;
                $total_price  = 0;
                $product = array();
                $subtotal = 0;
                foreach($orders_products as $orders_products_data){
                    $product_attribute = DB::table('orders_products_attributes')
                        ->where([
                            ['orders_products_id', '=', $orders_products_data->orders_products_id],
                            ['orders_id', '=', $orders_products_data->orders_id],
                        ])
                        ->get();
                        
                    $orders_products_data->attribute = $product_attribute;
                    $product[$i] = $orders_products_data;
                    //$total_tax     = $total_tax+$orders_products_data->products_tax;
                    $total_price = $total_price+$orders_products[$i]->final_price;
                    
                    $subtotal += $orders_products[$i]->final_price;
                    
                    $i++;
                    //$orders_products_data[] = $orders_products_data;
                }
            //print_r($product);
            $data->data = $product;
            $orders_data[] = $data;
        }
        
            $orders_status_history = DB::table('orders_status_history')
                ->LeftJoin('orders_status', 'orders_status.orders_status_id', '=' ,'orders_status_history.orders_status_id')
                ->orderBy('orders_status_history.date_added', 'desc')
                ->where('orders_id', '=', $orders_id)->get();
                    
            $orders_status = DB::table('orders_status')->get();
                    
            $ordersData['orders_data']              =   $orders_data;
            $ordersData['total_price']              =   $total_price;
            $ordersData['orders_status']            =   $orders_status;
            $ordersData['orders_status_history']    =   $orders_status_history;
            $ordersData['subtotal']                 =   $subtotal;
            
           $html = view('mail.orderEmail', ['ordersData' => $ordersData])->render();
            app('App\Http\Controllers\PageController')->send_mail($customers_email_address, $html, 'Order created successfully!');
            $admin = app('App\Http\Controllers\PageController')->getSetting()->contact_us_email;
            app('App\Http\Controllers\PageController')->send_mail($admin, $html, 'New order recived!');
          $orders_history_id = DB::table('orders_status_history')->insertGetId(
                [    'orders_id'  => $orders_id,
                     'orders_status_id' => 3,
                     'date_added'  => $date_added,
                     'customer_notified' =>'1',
                     'comments'  =>  'Stripe Payment complete'
                ]);
          return redirect('page/thankyou');
            
        }else if($payment_status == "failed"){
          $orders_history_id = DB::table('orders_status_history')->insertGetId(
                [    'orders_id'  => $orders_id,
                     'orders_status_id' => 2,
                     'date_added'  => $date_added,
                     'customer_notified' =>'1',
                     'comments'  =>  'Stripe Payment cancel'
                ]);
            return back()->withError('Some error occur, sorry for inconvenient 528');
        }
        
        $orderResponse = json_encode($responseData);
        return $responseData;
    }
    public function getSingleProducts($products_id){
        
        $language_id     =   1;
        $products = DB::table('products_to_categories')
            ->leftJoin('categories', 'categories.categories_id', '=', 'products_to_categories.categories_id')
            ->leftJoin('categories_description', 'categories_description.categories_id', '=', 'products_to_categories.categories_id')
            ->leftJoin('products', 'products.products_id', '=', 'products_to_categories.products_id')
            ->leftJoin('products_description','products_description.products_id','=','products.products_id')
            ->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
            ->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
            ->LeftJoin('specials', function ($join) {
                $join->on('specials.products_id', '=', 'products.products_id')->where('status', '=', '1');
             })
            ->select('products_to_categories.*', 'categories_description.categories_name','categories.*', 'products.*','products_description.*','manufacturers.*','manufacturers_info.manufacturers_url', 'specials.specials_id', 'specials.products_id as special_products_id', 'specials.specials_new_products_price as specials_products_price', 'specials.specials_date_added as specials_date_added', 'specials.specials_last_modified as specials_last_modified', 'specials.expires_date')
            ->where('products_description.language_id','=', $language_id)
            ->where('categories_description.language_id','=', $language_id)
            ->where('products.products_id','=', $products_id)
            ->first();
            $products->nprice = $products->specials_products_price;
            
        return $products;
    }
}