<?php

  

namespace App\Http\Controllers;

  

use Illuminate\Http\Request;

use Srmklive\PayPal\Services\ExpressCheckout;

   

class PayPalController extends Controller

{

    /**

     * Responds with a welcome message with instructions

     *

     * @return \Illuminate\Http\Response

     */
        public function getCart(){
        $cart_data = array();
        $customers_id                           =   '::1';//Request::ip();
;       
        //$customers_id                         =   '29';
        
        $customers_basket = DB::table('customers_basket')->where('customers_basket.customers_ip', '=', $customers_id)->where('is_order',0)->get();
        // dd($customers_basket);
        $total_carts = count($customers_basket); 
        if($total_carts > 0){
            foreach($customers_basket as $customers_basket_data){
                
                $customers_basket_attribute = DB::table('customers_basket_attributes')
                    ->join('products_options', 'products_options.products_options_id','=','customers_basket_attributes.products_options_id')
                    ->join('products_options_values', 'products_options_values.products_options_values_id','=','customers_basket_attributes.products_options_values_id')
                    ->select('products_options.products_options_name as attribute_name','products_options_values.products_options_values_name as attribute_value')
                    ->get();
                        
                $customers_basket_data->attributes = $customers_basket_attribute;
                $cart_data[] = $customers_basket_data;
            }
        }
        return $customers_basket;       
        
    }
    //addToOrder
    public function addToOrder(){
        $uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
        $shipAddress = $_POST['shipAddress'];
        $billAddress = $_POST['billAddress'];
        $pmethod = $_POST['pmethod'];
        $user = DB::table('customers')->where('customers_id', '=', $uid)->first();
        $saddress = DB::table('address_book')->where('address_book_id', '=', $shipAddress)->first();
        if(!$saddress)
        {
            return back()->withError('Please select your shipping address');
        }
        if(isset($shippng_detail->entry_zone_id) && $saddress->entry_zone_id > 0)
            {
                $zone = DB::table('zones')->where('zone_id',$shippng_detail->entry_zone_id)->first();
                $is_dilvery = $zone->is_dilvery;
                $dcharges = $zone->charges;
                if($is_dilvery == 0)
            return back()->withError('We dont ship in '.$zone->zone_name.'.Please update shipping address');
            }
        $baddress = DB::table('address_book')->where('address_book_id', '=', $shipAddress)->first();
        
        $date_added                             =   date('Y-m-d h:i:s');
        
        $customers_id                           =   $user->customers_id;
        $customers_telephone                    =   $user->customers_telephone;
        $customers_email_address                =   $user->customers_email_address; 
            
        $delivery_firstname                     =   $saddress->entry_firstname;
        $delivery_lastname                      =   $saddress->entry_lastname;
        $delivery_street_address                =   $saddress->entry_street_address;
        $delivery_suburb                        =   $saddress->entry_suburb;
        $delivery_city                          =   $saddress->entry_city;
        $delivery_postcode                      =   $saddress->entry_postcode;
        
        $delivery = DB::table('zones')->where('zone_name', '=', 1)->get();
        
        if(count($delivery)){
            $delivery_state                         =   $delivery[0]->zone_code;
        }else{
            $delivery_state                         =   'other';
        }
           
        $delivery_country                       =   $saddress->entry_country_id;
        
        $billing_firstname                      =   $baddress->entry_firstname;
        $billing_lastname                       =   $baddress->entry_lastname;
        $billing_street_address                 =   $baddress->entry_street_address;
        $billing_suburb                         =   $baddress->entry_suburb;
        $billing_city                           =   $baddress->entry_city;
        $billing_postcode                       =   $baddress->entry_postcode;
        
        $billing = DB::table('zones')->where('zone_name', '=', 1)->get();
        
        if(count($billing)){
            $billing_state                          =   $billing[0]->zone_code;
        }else{
            $billing_state                          =   'other';
        }
        
        $billing_country                        =   $baddress->entry_country_id;
        
        $payment_method                         =   $pmethod;
        $order_information                      =   array();
        $cart = $this->getCart();
        $price = 0;
        foreach ($cart as $key => $value) {
            $price += $value->customers_basket_quantity*$value->final_price;
        }
        
        $cc_type                            =   '';//$request->cc_type;
        $cc_owner                           =   '';//$request->cc_owner;
        $cc_number                          =   '';//$request->cc_number;
        $cc_expires                         =   '';//$request->cc_expires;
        $last_modified                      =   date('Y-m-d H:i:s');
        $date_purchased                     =   date('Y-m-d H:i:s');
        $order_price                        =   $price;
        $shipping_cost                      =   '';
        $shipping_method                    =   '';
        $orders_status                      =   '';
        $orders_date_finished               =   '';//$request->orders_date_finished;
        $comments                           =   'comment';//$request->comments;
        //$currency                         =   $request->currency;
        $currency                           =   'euro';
        $currency_value                     =   '';
        //$products_tax                     =   $request->products_tax;
        
        //tax info
        $total_tax                          =   '';//$request->total_tax;
        
        $products_tax                       =   1;
        //coupon info
        $is_coupon_applied                  =   0;//$request->is_coupon_applied;
        
        if($is_coupon_applied==1){
            
            $code = array();    
            $coupon_amount = 0; 
            $exclude_product_ids = array();
            $product_categories = array();
            $excluded_product_categories = array();
            $exclude_product_ids = array();
            
            $coupon_amount    =     '';//$request->coupon_amount;
            
            foreach($request->coupons as $coupons_data){
                
                //update coupans        
                $coupon_id = DB::statement("UPDATE `coupons` SET `used_by`= CONCAT(used_by,',$customers_id') WHERE `code` = '".$coupons_data['code']."'");
                
                //dd(DB::getQueryLog());
                
                //Log::useDailyFiles(storage_path().'/logs/debug.log');
                //Log::info(['coupon_query'=>DB::getQueryLog()]);
            
            }
            $code = json_encode($request->coupons);
            
        }else{
            $code                               =   '';
            $coupon_amount                      =   '';
        }   
        
        
        //payment methods 
        $payments_setting = DB::table('payments_setting')->get();

        
        if($payment_method == 'paypal' or $payment_method == 'card_payment'){
            
            //braintree transaction with nonce
            /*$is_transaction  = '1';           # For payment through braintree
            $nonce           =   '';//$request->nonce;
            
            if($payments_setting[0]->braintree_enviroment == '0'){
                $braintree_environment = 'sandbox'; 
            }else{
                $braintree_environment = 'production';  
            }
            
            $braintree_merchant_id = $payments_setting[0]->braintree_merchant_id;
            $braintree_public_key  = $payments_setting[0]->braintree_public_key;
            $braintree_private_key = $payments_setting[0]->braintree_private_key;
            
            //brain tree credential
            require_once app_path('braintree/Braintree.php');
            //print_r($result);
            if ($result->success) 
            {
            //print_r("success!: " . $result->transaction->id);
            if($result->transaction->id)
                {
                    $order_information = array(
                        'braintree_id'=>$result->transaction->id,
                        'status'=>$result->transaction->status,
                        'type'=>$result->transaction->type,
                        'currencyIsoCode'=>$result->transaction->currencyIsoCode,
                        'amount'=>$result->transaction->amount,
                        'merchantAccountId'=>$result->transaction->merchantAccountId,
                        'subMerchantAccountId'=>$result->transaction->subMerchantAccountId,
                        'masterMerchantAccountId'=>$result->transaction->masterMerchantAccountId,
                        //'orderId'=>$result->transaction->orderId,
                        'createdAt'=>time(),
//                      'updatedAt'=>$result->transaction->updatedAt->date,
                        'token'=>$result->transaction->creditCard['token'],
                        'bin'=>$result->transaction->creditCard['bin'],
                        'last4'=>$result->transaction->creditCard['last4'],
                        'cardType'=>$result->transaction->creditCard['cardType'],
                        'expirationMonth'=>$result->transaction->creditCard['expirationMonth'],
                        'expirationYear'=>$result->transaction->creditCard['expirationYear'],
                        'customerLocation'=>$result->transaction->creditCard['customerLocation'],
                        'cardholderName'=>$result->transaction->creditCard['cardholderName']
                    );

                    //print_r($order_information);
                    $payment_status = "success";
                    //print_r($result->transaction->status);
                }
                $payment_status = "success";
            } 
            else
                {
                    $payment_status = "failed";
                }*/
                $payment_status = "success";
                
        }else if($payment_method == 'PayPal'){

            die("OK");
             if($charge->paid == true){
                 $order_information = array(
                        'paid'=>'true',
                        'transaction_id'=>$charge->id,
                        'type'=>$charge->outcome->type,
                        'balance_transaction'=>$charge->balance_transaction,
                        'status'=>$charge->status,
                        'currency'=>$charge->currency,
                        'amount'=>$charge->amount,
                        'created'=>date('d M,Y', $charge->created),
                        'dispute'=>$charge->dispute,
                        'customer'=>$charge->customer,
                        'address_zip'=>$charge->source->address_zip,
                        'seller_message'=>$charge->outcome->seller_message,
                        'network_status'=>$charge->outcome->network_status,
                        'expirationMonth'=>$charge->outcome->type
                    );
                    //print_r($order_information);
                    $payment_status = "success";
                    
             }else{
                    $payment_status = "failed";  
             }
            
        }else if($payment_method == 'cash_on_delivery'){
            
            $payment_method = 'Cash on Delivery';
            $payment_status='success';
            
        } else if($payment_method == 'simplePaypal'){
            
    public function getCart(){
        $cart_data = array();
        $customers_id                           =   '::1';//Request::ip();
;       
        //$customers_id                         =   '29';
        
        $customers_basket = DB::table('customers_basket')->where('customers_basket.customers_ip', '=', $customers_id)->where('is_order',0)->get();
        // dd($customers_basket);
        $total_carts = count($customers_basket); 
        if($total_carts > 0){
            foreach($customers_basket as $customers_basket_data){
                
                $customers_basket_attribute = DB::table('customers_basket_attributes')
                    ->join('products_options', 'products_options.products_options_id','=','customers_basket_attributes.products_options_id')
                    ->join('products_options_values', 'products_options_values.products_options_values_id','=','customers_basket_attributes.products_options_values_id')
                    ->select('products_options.products_options_name as attribute_name','products_options_values.products_options_values_name as attribute_value')
                    ->get();
                        
                $customers_basket_data->attributes = $customers_basket_attribute;
                $cart_data[] = $customers_basket_data;
            }
        }
        return $customers_basket;       
        
    }

   

}