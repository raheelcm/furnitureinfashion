<?php

namespace App\Http\Controllers;

 use Illuminate\Http\Request;
use DB;
use Hash;
// use Request;

use Session;
use Redirect;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;


class PageController extends Controller
{
	public function get_Zone_ID($pcode)
	{
		$result = substr($pcode, 0, 2);
		$zone = DB::table('zones')->where('zone_code',$result)->first();
		if(isset($zone->zone_id) && $zone->zone_id)
			return $zone->zone_id;
		else
			return 0;
	}
	public function send_mail($to, $html, $sub) {

        $data = array (

              'personalizations' =>
              array (

                0 => 

                array (

                  'to' => 

                  array (

                    0 => 

                    array (

                      'email' => $to,

                    ),

                  ),

                  'subject' => $sub,

                ),

              ),

              'from' => 

              array (

                'email' => app('App\Http\Controllers\PageController')->getSetting()->contact_us_email ,

                'name' => 'Furniture App',

              ),

              'content' => 

              array (

                0 => 

                array (

                  'type' => 'text/html',

                  'value' => $html,

                ),

              ),

            );

            

            $curl = curl_init();

            

              curl_setopt_array($curl, array(

              CURLOPT_URL => "https://api.sendgrid.com/v3/mail/send",

              CURLOPT_RETURNTRANSFER => true,

              CURLOPT_ENCODING => "",

              CURLOPT_MAXREDIRS => 10,

              CURLOPT_TIMEOUT => 30,

              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,

              CURLOPT_CUSTOMREQUEST => "POST",

              CURLOPT_POSTFIELDS => json_encode($data),

              CURLOPT_HTTPHEADER => array(

                "authorization: Bearer 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",

                "cache-control: no-cache",

                "content-type: application/json",

                "postman-token: 500e002d-9ecb-48a8-eb26-9e81cba79900"

              ),

            ));

            

            $response = curl_exec($curl);

            $err = curl_error($curl);

            

            curl_close($curl);

            

            if ($err) {

              echo $err;


            } else {

              $arr = json_decode($response);
              print_r($arr);

            }

    }
  public function getSearchData(Request $request){
		
// 		$language_id            				=   $request->language_id;
		$language_id            				=   '1';	
		$searchValue            				=   $request->text;
		$currentDate 							=   time();	
		//print_r($searchValue);
		$result = array();
		
		$mainCategories = DB::table('categories')
			->leftJoin('categories_description','categories_description.categories_id', '=', 'categories.categories_id')
			->select('categories.categories_id as id', 'categories.categories_image as image', 'categories_description.categories_name as name')
			->where('categories_description.categories_name', 'LIKE', '%'.$searchValue.'%')
			->where('categories_description.language_id', '=', $language_id)
			->where('parent_id', '0')->get();
		
		$result['mainCategories'] = $mainCategories;
		
		$subCategories = DB::table('categories')
			->leftJoin('categories_description','categories_description.categories_id', '=', 'categories.categories_id')
			->select('categories.categories_id as id', 'categories.categories_image as image', 'categories_description.categories_name as name')
			->where('categories_description.categories_name', 'LIKE', '%'.$searchValue.'%')
			->where('categories_description.language_id', '=', $language_id)
			->where('parent_id', '1')->get();
		
		$result['subCategories'] = $subCategories;
		
		$manufacturers = DB::table('manufacturers')
			->leftJoin('manufacturers_info','manufacturers_info.manufacturers_id', '=', 'manufacturers.manufacturers_id')
			->select('manufacturers.manufacturers_id as id', 'manufacturers.manufacturers_image as image',  'manufacturers.manufacturers_name as name')
			//->where('manufacturers.language_id', '=', $language_id)
			->where('manufacturers.manufacturers_name', 'LIKE', '%'.$searchValue.'%')
			->get();
    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
// 			dd($page);
		$productsAttribute = DB::table('products_to_categories')
				->join('products', 'products.products_id', '=', 'products_to_categories.products_id')
				->leftJoin('products_description','products_description.products_id','=','products.products_id')
				->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
				->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
				->leftJoin('products_attributes','products_attributes.products_id','=','products.products_id')
				->leftJoin('products_options','products_options.products_options_id','=','products_attributes.options_id')
				->leftJoin('products_options_values','products_options_values.products_options_values_id','=','products_attributes.options_values_id')				->LeftJoin('specials', function ($join) use ($currentDate) {  
					$join->on('specials.products_id', '=', 'products_to_categories.products_id')->where('status', '=', '1')->where('expires_date', '>', $currentDate);
				})->select('products.*','products_description.*', 'manufacturers.*', 'manufacturers_info.manufacturers_url', 'specials.specials_new_products_price as discount_price')
				
				->select('products.*', 'products_description.*', 'manufacturers.*', 'manufacturers_info.manufacturers_url', 'specials.specials_new_products_price as discount_price', 'specials.specials_new_products_price as discount_price', 'products_to_categories.categories_id')
				->orWhere('products_options.products_options_name', 'LIKE', '%'.$searchValue.'%')
				->orWhere('products_options_values.products_options_values_name', 'LIKE', '%'.$searchValue.'%')
				->orWhere('products_name', 'LIKE', '%'.$searchValue.'%')
				->orWhere('products_model', 'LIKE', '%'.$searchValue.'%')
				->where('products_description.language_id', '=', $language_id)
				->groupBy('products.products_id')->paginate(4,['*'], 'page',$page);
    				$sort = 'products_date_added';
					if(isset($_GET['sort']))
						$sort = $_GET['sort'];
    				$type = 'less';
					if(isset($_GET['type']))
						$type = $_GET['type'];
			$newpro= array();
			foreach ($productsAttribute as $key => $value) {
				$det = $this->getSingleProducts($value->products_id);
// 				$det->nprice = $det->specials_new_products_price;
				$newpro[] = $det;
			}
			$data['products'] = $this->bubble_sort($newpro, $sort,$type);
					$data['pagi'] = $productsAttribute; 
					// dd($data);
					return view('plisting',$data);
    
				
			$result2 = array();
			//check if record exist
			if(count($productsAttribute)>0){
				$index = 0;	
				foreach ($productsAttribute as $products_data){
				$products_id = $products_data->products_id;
				
				
				//multiple images
				$products_images = DB::table('products_images')->select('image')->where('products_id','=', $products_id)->orderBy('sort_order', 'ASC')->get();		
				$products_data->images =  $products_images;
          
				//array_push($products_data, $temp_images);
				array_push($result2,$products_data);
				$options = array();
				$attr = array();
				
				//like product
				if(!empty($request->customers_id)){
					$liked_customers_id						=	$request->customers_id;	
					$categories = DB::table('liked_products')->where('liked_products_id', '=', $products_id)->where('liked_customers_id', '=', $liked_customers_id)->get();
          $result2[$index]->html  = 	'OK';	
					//print_r($categories);
					if(count($categories)>0){
						$result2[$index]->isLiked = '1';
					}else{
						$result2[$index]->isLiked = '0';
					}
				}else{
					$result2[$index]->isLiked = '0';						
				}
				
				// fetch all options add join from products_options table for option name
				$products_attribute = DB::table('products_attributes')->where('products_id','=', $products_id)->groupBy('options_id')->get();
				if(count($products_attribute)){
					$index2 = 0;
					foreach($products_attribute as $attribute_data){
						$option_name = DB::table('products_options')->where('language_id','=', $language_id)->where('products_options_id','=', $attribute_data->options_id)->get();
						//print_r($option_name);
						if(count($option_name)>0){
						$temp = array();
						$temp_option['id'] = $attribute_data->options_id;
						$temp_option['name'] = $option_name[0]->products_options_name;
						$attr[$index2]['option'] = $temp_option;
						
						// fetch all attributes add join from products_options_values table for option value name
						
						$attributes_value_query =  DB::table('products_attributes')->where('products_id','=', $products_id)->where('options_id','=', $attribute_data->options_id)->get();
						foreach($attributes_value_query as $products_option_value){
							$option_value = DB::table('products_options_values')->where('products_options_values_id','=', $products_option_value->options_values_id)->get();
							$temp_i['id'] = $products_option_value->options_values_id;
							$temp_i['value'] = $option_value[0]->products_options_values_name;
							$temp_i['price'] = $products_option_value->options_values_price;
							$temp_i['price_prefix'] = $products_option_value->price_prefix;
							array_push($temp,$temp_i);
							
						}
						$attr[$index2]['values'] = $temp;
						$result2[$index]->attributes = 	$attr;	
						$index2++;
					}
					}
				}else{
					$result2[$index]->attributes = 	array();	
				}
					$index++;
				}
					
					
				}
		
		
		$result['products'] = $result2;
    $data['pagi'] = $result2; 
					// dd($data);
					return view('plisting',$data);
		$total_record = count($result['products']) + count($result['subCategories']) + count($result['mainCategories']);
		
		if(count($result['products'])==0 and count($result['subCategories'])==0 and count($result['mainCategories'])==0){
			$result = new  \stdClass();
			$responseData = array('success'=>'0', 'product_data'=>$result,  'message'=>"Search result is not found.", 'total_record'=>$total_record);
			
		}else{
			$responseData = array('success'=>'1', 'product_data'=>$result,  'message'=>"Returned all searched products.", 'total_record'=>$total_record);
		}	
		
		$categoryResponse = json_encode($responseData);
		print $categoryResponse;
	}
    public function payment_response($type, $order_id)
    {
    	$orders = DB::table('orders')->where('orders_id', $order_id)->first();
    	if($_GET['response'] == 'SUCCESSS')
			{
				if($type == 'pack')
        {
        	$customers_id = $orders->customers_id;
        	$pack_id = $orders->pack_id;
        	$package = DB::table('packages')->where('packID', $pack_id)->first();
        	$bids = $package->bids;
        	$customer = DB::table('customers')->where('customers_id', $customers_id)->first();
        	$nbid = $customer->bids+ $bids;
        	DB::table('customers')->where('customers_id', $customers_id)->update([
					'bids' => $nbid	
				]);
        	$date_added	=	date('Y-m-d h:i:s');
        	$orders_history_id = DB::table('orders_status_history')->insertGetId(
				[	 'orders_id'  => $order_id,
					 'orders_status_id' => 2,
					 'date_added'  => $date_added,
					 'customer_notified' =>'1',
					 'comments'  =>  'Done payment from channelsmedia gateway'
				]);
            return view('return',array('status'=>1));
        }
				
			}
			else
			{
				return view('return',array('status'=>0));
			}
        
    }
    private function payment_link($data)
	{
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://knet.gingergulf.com//api/payment_link?api_key=7f00ac303caa92bd9b447d14e3840c90",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "POST",
  CURLOPT_POSTFIELDS => "data=".json_encode($data),
  CURLOPT_HTTPHEADER => array(
    "Content-Type: application/x-www-form-urlencoded",
    "Accept: application/json",
    "authorization: key"
  ),
));

$response = curl_exec($curl);

curl_close($curl);
return json_decode($response,true);

	}
	public function buybids()
	{
	    $uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
            $user = DB::table('customers')->where('customers_id', $uid)->where('isActive', '1')->first();
		
		/*if(isset($_GET['response']))
		{
			if($_GET['response'] == 'SUCCESSS')
			{
				return view('return',array('status'=>1));
			}
			else
			{
				return view('return',array('status'=>0));
			}
		}*/
		if(isset($_GET['packID']))
		{
			$package = DB::table('packages')->where('packID', $_GET['packID'])->first();
		    $price = $package->price;

		    $date_added								=	date('Y-m-d h:i:s');
		    $orders_id = DB::table('orders')->insertGetId(
				[	 'customers_id' => $uid,
					 'customers_name'  => $user->customers_email_address,
					 'customers_telephone' => $user->customers_telephone,
					 'customers_email_address'  => $user->customers_email_address,
					 'date_purchased'=>$date_added,
					 'order_price'=> $price,
					 'pack_id'=>$_GET['packID'],
				]);
		    $calll_back = url('/').'/payment_response/pack/'.$orders_id;
		    // $calll_back = 'http://channelsmedia.net/Mazaad-Market/buybids';
		    $name = 'Mazaad customer';
		    if(!empty($user->customers_firstname) && !empty($user->customers_lastname))
		    	$name = $user->customers_firstname.' '.$user->customers_lastname;
		    $phone = '123456789';
		    if(!empty($user->customers_telephone))
		    	$phone = $user->customers_telephone;
		    $pdata = array (
						  'InvoiceValue' => $price,
						  'CustomerName' => $name,
						  'CustomerBlock' => 'Block',
						  'CustomerStreet' => 'Street',
						  'CustomerHouseBuildingNo' => 'Building no',
						  'CustomerCivilId' => '123456789124',
						  'CustomerAddress' => 'Payment Address',
						  'CustomerReference' => '\'.$t.\'',
						  'DisplayCurrencyIsoAlpha' => 'KWD',
						  'CountryCodeId' => '+965',
						  'CustomerMobile' => $phone,
						  'CustomerEmail' => $user->customers_email_address,
						  'DisplayCurrencyId' => 3,
						  'SendInvoiceOption' => 1,
						  'InvoiceItemsCreate' => 
						  array (
						    0 => 
						    array (
						      'ProductId' => NULL,
						      'ProductName' => $package->name.'  Bids Package ID'.$_GET['packID'],
						      'Quantity' => 1,
						      'UnitPrice' => $price,
						    ),
						  ),
						  'CallBackUrl' => $calll_back,
						  'Language' => 2,
						  'ExpireDate' => '2022-12-31T13:30:17.812Z',
						  'ApiCustomFileds' => 'weight=10,size=L,lenght=170',
						  'ErrorUrl' => $calll_back,
						);
				$orders_history_id = DB::table('orders_status_history')->insertGetId(
				[	 'orders_id'  => $orders_id,
					 'orders_status_id' => 1,
					 'date_added'  => $date_added,
					 'customer_notified' =>'1',
					 'comments'  =>  'Packages buy from web'
				]);
			$ret = $this->payment_link($pdata);

			if($ret['status'] == 1)
			{
				// print_r();
				header("Location: ".$ret['link']);
				exit;
			}
		}
		
		$data = array();
    	$lang = 1;
    	$data['products'] = DB::table('packages')->where('status',1)->get();
     	// dd($data['product']->products_image);
    	return view('buybids',$data);

	}
	//cart update
	public function updateCart(Request $request){
		
		$customers_ip            				=   $_SERVER['SERVER_ADDR'];
		$products_id            				=   $request->products_id;
		
		$customers_basket_quantity            	=   $request->customers_basket_quantity;
		$final_price            				=   $request->final_price;
		$customers_basket_date_added            =   date('Y-m-d H:i:s');
		
		return DB::table('customers_basket')
		->where('customers_ip','=', $customers_ip)->where('products_id','=', $products_id)->update([
			 'customers_basket_quantity' => $customers_basket_quantity,
			 'final_price' => $final_price,
			 'customers_basket_date_added' => $customers_basket_date_added,
		]);
				/*
		foreach($request->attribute as $attribute){
			//print_r($attribute['products_options_id']);	
			DB::table('customers_basket_attributes')->insert(
			[
				 'customers_id' => $customers_id,
				 'products_id'  => $products_id,
				 'products_options_id' =>$attribute['products_options_id'],
				 'products_options_values_id'  =>  $attribute['products_options_values_id']
			]);
		}*/
		
		$cartResponse = json_encode($responseData);
		print $cartResponse;
	}
	public function product_detail($slug)
	{
		$data = array();
    	$lang = 1;
    	$pro =  DB::table('products')->where('slug',$slug)->first();
    	// dd($pro);
    	$id = $pro->products_id;
    	$ret = DB::table('recent_products')->insert(
		[
			 'ip' => $_SERVER['SERVER_ADDR'],
			 'pid'  => $id,
		]);
    	$data['product'] = $sing= $this->getSingleProducts($id);
    	// dd($sing);
    	if($sing->products_name)
    	$data['pageTitle'] = $sing->products_name;
    	$data['images'] = $this->productImages($id);
    	$data['related'] = $this->getRelatedProducts($sing->categories_id ,$id);
    	// dd($data['images']);
     	
    	return view('pdetail',$data);
	}
	//deleteFromCart
	public function deleteFromCart(Request $request){
		
		$customers_id            				=   $_SERVER['SERVER_ADDR'];		
		$products_id            				=   $request->products_id;
		//$customers_id            				=   '28';		
		//$products_id            				=   '1';
		
		DB::table('customers_basket')->where([
			['customers_ip', '=', $customers_id],
			['products_id',  '=', $products_id],
		])->delete();
		/*
		DB::table('customers_basket_attributes')->where([
			['customers_id', '=', $customers_id],
			['products_id',  '=', $products_id],
		])->delete();*/
				
		$responseData = array('success'=>'1', 'data'=>array(), 'message'=>"Record is deleted.");
		$cartResponse = json_encode($responseData);
		print $cartResponse;
	}
	//addToCart
	public function addToCart(Request $request){
		$uid = 0;
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                /*$responseData = array('success'=>'0', 'data'=>array(), 'message'=>"Please login", 'url'=>url('/page/login'));
		$cartResponse = json_encode($responseData);
		return $cartResponse;*/

            }
		
		$customers_id            				=   $uid;
		if(isset($request->products_id))
		$products_id            				=   $request->products_id;
		 
		if(isset($request->customers_basket_quantity))
		$customers_basket_quantity            	=   $request->customers_basket_quantity;
		$final_price            				=   $request->final_price;
		$customers_basket_date_added            =   date('Y-m-d H:i:s');
		
		DB::table('customers_basket')->insert(
		[
			 'customers_ip' => $_SERVER['SERVER_ADDR'],
			 'products_id'  => $products_id,
			 'customers_basket_quantity' => $customers_basket_quantity,
			 'final_price' => $final_price,
			 'customers_basket_date_added' => $customers_basket_date_added,
		]);
		
		if(!empty($request->attribute)){
			foreach($request->attribute as $attribute){
				//print_r($attribute['products_options_id']);	
				DB::table('customers_basket_attributes')->insert(
				[
					 'customers_id' => $customers_id,
					 'products_id'  => $products_id,
					 'products_options_id' =>$attribute['products_options_id'],
					 'products_options_values_id'  =>  $attribute['products_options_values_id']
				]);
			}
		}
		
		$responseData = array('success'=>'1', 'data'=>array(), 'message'=>"Cart item added." , 'url'=>url('/page/cart'));
		$cartResponse = json_encode($responseData);
		print $cartResponse;
	}
	public function getIp(){
    foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
        if (array_key_exists($key, $_SERVER) === true){
            foreach (explode(',', $_SERVER[$key]) as $ip){
                $ip = trim($ip); // just to be safe
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                    return $ip;
                }
            }
        }
    }
}
//coupons
//getCart
	public function getCoupons(){
;		
		//$customers_id            				=   '29';
		
		$customers_basket = DB::table('coupons')->get();
		// dd($customers_basket);
		return $customers_basket;
		
		
	}
	public function getPages(){
;		
		//$customers_id            				=   '29';
		
		$pages = DB::table('pages')
		            ->join('pages_description', 'pages_description.page_id', '=', 'pages.page_id')->where('pages_description.language_id',1)

		->get();
		// dd($customers_basket);
		return $pages;
		
		
	}
	//getCart
	public function getCart(){
		$cart_data = array();
		$customers_id            				=   $_SERVER['SERVER_ADDR'];
;		
		//$customers_id            				=   '29';
		
		$customers_basket = DB::table('customers_basket')->where('customers_basket.customers_ip', '=', $customers_id)->where('is_order',0)->get();
		// dd($customers_basket);
		$total_carts = count($customers_basket); 
		if($total_carts > 0){
			foreach($customers_basket as $customers_basket_data){
				
				$customers_basket_attribute = DB::table('customers_basket_attributes')
					->join('products_options', 'products_options.products_options_id','=','customers_basket_attributes.products_options_id')
					->join('products_options_values', 'products_options_values.products_options_values_id','=','customers_basket_attributes.products_options_values_id')
					->select('products_options.products_options_name as attribute_name','products_options_values.products_options_values_name as attribute_value')
					->get();
						
				$customers_basket_data->attributes = $customers_basket_attribute;
				$cart_data[] = $customers_basket_data;
			}
		}
		return $customers_basket;		
		
	}
	public function addToList(Request $request){
		$uid = 0;
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                $responseData = array('success'=>'0', 'data'=>array(), 'message'=>"Please login", 'url'=>url('/page/login'));
		$cartResponse = json_encode($responseData);
		return $cartResponse;

            }
		$customers_id            				=   $uid;
		$products_id            				=   $request->products_id;
		$customers_basket_date_added            =   date('Y-m-d H:i:s');
		$already = DB::table('customers_wishlist')->where('customers_id', $uid)->where('products_id', $products_id)->get();
		if(count($already) > 0)
		{
			$responseData = array('success'=>'0', 'message'=>"Product already in List.",'url'=> url('page/wishlist'));
		$cartResponse = json_encode($responseData);
		print $cartResponse;
		}
		else
		{
		$res = DB::table('customers_wishlist')->insert(		[
			 'customers_id' => $uid,
			 'products_id'  => $products_id,
			 'customers_basket_date_added' => $customers_basket_date_added,
		]);
		$responseData = array('success'=>'0', 'message'=>"Server error.",'url'=> url('/'));
		
		if($res)
		$responseData = array('success'=>'1', 'message'=>"List item added.",'url'=> url('page/wishlist'));
		$cartResponse = json_encode($responseData);
		print $cartResponse;
	}
	}
	public function getSetting(){
		$setting = DB::table('setting')->get();
		return $setting[0];
	}
    public function home()
    {
    	
    	$data = array();
    	$lang = 1;
    	$category_id = 0;
    	// $this->getCoupons();
    	if(isset($_GET['category']))
    	{
    		$category_id = $_GET['category'];
    	}

    	$search = '';
    	if(isset($_GET['search']))
    	{
    		$search = $_GET['search'];
    	}
    	$data['banners'] = DB::table('banners')->where('status', '1')->get();
    	$data['products'] = $this->getProducts($lang,$category_id,$search);
    	$data['specials'] = $this->getSpecialProducts();
    	$data['categories'] = $this->product_categories();
    	$data['category_select'] = $category_id;
    	$data['search_keyword'] = $search;
    	$data['pageTitle'] = 'Home';
     	// dd($data['banners']);
    	return view('home',$data);
    }
    public  function slugify($text)
{
  // replace non letter or digits by -
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);

  // transliterate
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

  // remove unwanted characters
  $text = preg_replace('~[^-\w]+~', '', $text);

  // trim
  $text = trim($text, '-');

  // remove duplicate -
  $text = preg_replace('~-+~', '-', $text);

  // lowercase
  $text = strtolower($text);

  if (empty($text)) {
    return 'n-a';
  }
  else
  {

  return $text;
}
}

    public function getProducts($lang ,$category_id,$search)
    {
			
		$language_id     =   1;		
		$products = DB::table('products_to_categories')
			->leftJoin('categories', 'categories.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('categories_description', 'categories_description.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('products', 'products.products_id', '=', 'products_to_categories.products_id')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
			->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
			->LeftJoin('specials', function ($join) {
				$join->on('specials.products_id', '=', 'products.products_id')->where('status', '=', '1');
			 })
			->select('products_to_categories.*', 'categories_description.categories_name','categories.*', 'products.*','products_description.*','manufacturers.*','manufacturers_info.manufacturers_url', 'specials.specials_id', 'specials.products_id as special_products_id', 'specials.specials_new_products_price as specials_products_price', 'specials.specials_date_added as specials_date_added', 'specials.specials_last_modified as specials_last_modified', 'specials.expires_date')
			->where('products_description.language_id','=', $language_id)
			->where('categories_description.language_id','=', $language_id)
			->orderBy('products.products_id', 'DESC');
			if($category_id != 0 )
			{
				
				$products = $products->where('products_to_categories.categories_id','=', $category_id);
			}

			if($search != '' )
			{
					
				$products = $products->where('products_description.products_name', 'LIKE', '%' . $search . '%');
			}

			$products = $products->get();
			return $products;
			
		
	}
    public function getSpecialProducts()
    {
			
		$language_id     =   1;		
		$products = DB::table('specials');
			

			$products = $products->get();
			$newpro= array();
			foreach ($products as $key => $value) {
				$det = $this->getSingleProducts($value->products_id);
				$det->nprice = $value->specials_new_products_price;
				$newpro[] = $det;
			}
			// dd($newpro);
			return $newpro;
			
		
	}
    public function getRecentProducts()
    {
			
		$language_id     =   1;		
		$customers_id            				=   $_SERVER['SERVER_ADDR'];
		$products = DB::table('recent_products')->groupby('pid')->where('ip',$customers_id);
			

			$products = $products->get();
			$newpro= array();
			foreach ($products as $key => $value) {
				$det = $this->getSingleProducts($value->pid);
				$newpro[] = $det;
			}
			// dd($newpro);
			return $newpro;
			
		
	}
	//single product
	public function getSingleProducts($products_id){
		
		$language_id     =   1;
		$products = DB::table('products_to_categories')
			->leftJoin('categories', 'categories.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('categories_description', 'categories_description.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('products', 'products.products_id', '=', 'products_to_categories.products_id')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
			->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
			->LeftJoin('specials', function ($join) {
				$join->on('specials.products_id', '=', 'products.products_id')->where('status', '=', '1');
			 })
			->select('products_to_categories.*', 'categories_description.categories_name','categories.*', 'products.*','products_description.*','manufacturers.*','manufacturers_info.manufacturers_url', 'specials.specials_id', 'specials.products_id as special_products_id', 'specials.specials_new_products_price as specials_products_price', 'specials.specials_date_added as specials_date_added', 'specials.specials_last_modified as specials_last_modified', 'specials.expires_date')
			->where('products_description.language_id','=', $language_id)
			->where('categories_description.language_id','=', $language_id)
			->where('products.products_id','=', $products_id)
			->first();
			if(!$products)
				 return abort(404);
			if(isset($products->specials_products_price))
			$products->nprice = $products->specials_products_price;
			else
				$products->nprice = $products->products_price;
			
		return $products;
	}
	public function productImages($products_id)
	{

		return $products_images = DB::table('products_images')->select('image')->where('products_id','=', $products_id)->orderBy('sort_order', 'ASC')->get();
	}
	public function single_categories($slug)
	{
		$language_id     =   1;
		//creatre slug of all
		$all = DB::table('products')
				->leftJoin('products_description', 'products_description.products_id', '=', 'products.products_id')
				->where('products_description.language_id','=', $language_id)->get();
				foreach ($all as $key => $value) {
					// print_r($value);
					// die();
					if(empty($value->slug))
					{
						$title = $value->products_name;
						DB::table('products')
            ->where('products_id', $value->products_id)
            ->update(['slug' => $this->slugify($title)]);
						// die();
					}
				}
		return  DB::table('categories')
				->leftJoin('categories_description', 'categories_description.categories_id', '=', 'categories.categories_id')
				->where('categories_description.language_id','=', $language_id)
				->where('categories.slug','=', $slug)
				->orderBy('sort_order', 'ASC')->get();
	}
	public function product_categories()
	{
		$language_id     =   1;
		//creatre slug of all
		$all = DB::table('products')
				->leftJoin('products_description', 'products_description.products_id', '=', 'products.products_id')
				->where('products_description.language_id','=', $language_id)->get();
				foreach ($all as $key => $value) {
					// print_r($value);
					// die();
					if(empty($value->slug))
					{
						$title = $value->products_name;
						DB::table('products')
            ->where('products_id', $value->products_id)
            ->update(['slug' => $this->slugify($title)]);
						// die();
					}
				}
		return  DB::table('categories')
				->leftJoin('categories_description', 'categories_description.categories_id', '=', 'categories.categories_id')
				->where('categories_description.language_id','=', $language_id)
				->where('categories.parent_id','=', 0)
				->orderBy('sort_order', 'ASC')->get();
	}

	public function getRelatedProducts($Cate_id,$pID){
		
		$language_id     =   1;		
		$products = DB::table('products_to_categories')
			->leftJoin('categories', 'categories.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('categories_description', 'categories_description.categories_id', '=', 'products_to_categories.categories_id')
			->leftJoin('products', 'products.products_id', '=', 'products_to_categories.products_id')
			->leftJoin('products_description','products_description.products_id','=','products.products_id')
			->leftJoin('manufacturers','manufacturers.manufacturers_id','=','products.manufacturers_id')
			->leftJoin('manufacturers_info','manufacturers.manufacturers_id','=','manufacturers_info.manufacturers_id')
			->LeftJoin('specials', function ($join) {
				$join->on('specials.products_id', '=', 'products.products_id')->where('status', '=', '1');
			 })
			->select('products_to_categories.*', 'categories_description.categories_name','categories.*', 'products.*','products_description.*','manufacturers.*','manufacturers_info.manufacturers_url', 'specials.specials_id', 'specials.products_id as special_products_id', 'specials.specials_new_products_price as specials_products_price', 'specials.specials_date_added as specials_date_added', 'specials.specials_last_modified as specials_last_modified', 'specials.expires_date')
			->where('products_description.language_id','=', $language_id)
			->where('categories_description.language_id','=', $language_id)
			->where('products_to_categories.categories_id','=', $Cate_id)
			->where('products.products_id','!=', $pID)
			->orderBy('products.products_id', 'DESC')
			->get();
			$npro = array();
			foreach ($products as $key => $value) {
				$npro[] = $this->getSingleProducts($value->products_id);
			}
			
		return $npro;
	}
    //Login
    public function processLogin(Request $request){
		
		$customers_email_address = $request->customers_email_address;
		$customers_password      = $request->customers_password;
		
		$existUser = DB::table('customers')->where('customers_email_address', $customers_email_address)->where('isActive', '1')->get();
		$chk = 0;
		if(count($existUser)>0){
				// dd($existUser);
				if(Hash::check($customers_password, $existUser[0]->customers_password))
				{
					$chk = 1;
				}
			}
			if($chk){


			$customers_id = $existUser[0]->customers_id;
			
			//update record of customers_info			
			$existUserInfo = DB::table('customers_info')->where('customers_info_id', $customers_id)->get();
			$customers_info_id 							= $customers_id;
			$customers_info_date_of_last_logon  		= date('Y-m-d H:i:s');
			$customers_info_number_of_logons     		= '1';
			$customers_info_date_account_created 		= date('Y-m-d H:i:s');
			$global_product_notifications 				= '1';
			
			if(count($existUserInfo)>0){
				//update customers_info table
				DB::table('customers_info')->where('customers_info_id', $customers_info_id)->update([
					'customers_info_date_of_last_logon' => $customers_info_date_of_last_logon,
					'global_product_notifications' => $global_product_notifications,
					'customers_info_number_of_logons'=> DB::raw('customers_info_number_of_logons + 1')
				]);
				
			}else{
				//break;
				//insert customers_info table
				$customers_default_address_id = DB::table('customers_info')->insertGetId(
					 ['customers_info_id' => $customers_info_id,
					  'customers_info_date_of_last_logon' => $customers_info_date_of_last_logon,
					  'customers_info_number_of_logons' => $customers_info_number_of_logons,
					  'customers_info_date_account_created' => $customers_info_date_account_created,
					  'global_product_notifications' => $global_product_notifications
					 ]
				);	
				
				DB::table('customers')->where('customers_id', $customers_id)->update([
					'customers_default_address_id' => $customers_default_address_id	
				]);
			}		
			
			//check if already login or not
			$already_login = DB::table('whos_online')->where('customer_id', '=', $customers_id)->get();
									
			if(count($already_login)>0){
				DB::table('whos_online')
					->where('customer_id', $customers_id)
					->update([
							'full_name'  => $existUser[0]->customers_firstname.' '.$existUser[0]->customers_lastname,
							'time_entry'   => date('Y-m-d H:i:s'),							
					]);
			}else{
				DB::table('whos_online')
					->insert([
							'full_name'  => $existUser[0]->customers_firstname.' '.$existUser[0]->customers_lastname,
							'time_entry' => date('Y-m-d H:i:s'),
							'customer_id'    => $customers_id							
					]);
			}			
			
			//get liked products id
			$products = DB::table('liked_products')->select('liked_products_id as products_id')
			->where('liked_customers_id', '=', $customers_id)
			->get();
			
			if(count($products)>0){
				$liked_products = $products;
			}else{
				$liked_products = array();
			}
			
			$existUser[0]->liked_products = $products;
			
			
			/*$responseData = array('success'=>'1', 'data'=>$existUser, 'message'=>'Data has been returned successfully!');
			$userResponse = json_encode($responseData);*/
			$request->session()->put('LoginUser', $existUser[0]);
			                    if($request->rurl)
                               {
                                return Redirect::to($request->rurl);
                               }
                               else
                               {
                                return redirect('/page/account');
                               }
			// Session::set('LoginUser', $existUser);

			
		}else{
			$existUser = array();
			// $responseData = array('success'=>'0', 'data'=>$existUser, 'message'=>);
			$message = "Invalid email or password";
			return redirect('page/login')->with('error',$message);
		}			
	}
	 public function logout(Request $request) {
        $request->session()->flush();
        session_unset();
        return redirect('/');


    }
    //registration 
	public function processRegistration(Request $request){
		if($request->customers_cpassword != $request->customers_password)
		{
			$message = "Password not match";
			return redirect('page/signup')->with('error',$message);
		}
		$user_name            		=   $request->customers_email_address;
		 $customers_firstname            		=   $request->customers_firstname;
		 $customers_lastname           			=   $request->customers_lastname;			
		$customers_email_address    		    =   $request->customers_email_address;
		$customers_password          		    =   Hash::make($request->customers_password);	
		$customers_telephone        		    =   $request->customers_telephone;
		$customers_picture        		   		=   '';
		$customers_info_date_account_created 	=   date('y-m-d h:i:s');	
		
		
		if(!empty($customers_picture)){
			$image = substr($customers_picture, strpos($customers_picture, ",") + 1);
			$img = base64_decode($image);
			$dir="resources/assets/images/user_profile/";
			if (!file_exists($dir) and !is_dir($dir)) {
				mkdir($dir);
			} 
			$uploadfile = $dir."/pic_".time().".jpg";
			file_put_contents(base_path().'/'.$uploadfile, $img);
			$profile_photo=$uploadfile;
		}else{
			$profile_photo="resources/assets/images/user_profile/default_user.svg";	
		}
		
		//check email existance
		$existUser = DB::table('customers')->where('customers_email_address', $customers_email_address)->get();	
		
		if(count($existUser)=="1"){	
			$message = "Email already exist!";
			return redirect('page/signup')->with('error',$message);
		}else{
			$customer_data = array(
				'user_name'			 =>  $user_name,
				'customers_firstname'			 =>  $customers_firstname,
				'customers_lastname'			 =>  $customers_lastname,
				'customers_telephone'			 =>  $customers_telephone,
				'customers_email_address'		 =>  $customers_email_address,
				'customers_password'			 =>  $customers_password,
				'customers_picture'				 =>  $profile_photo,
				'isActive'						 =>  '1',
				'created_at'					 =>	 time()
			);
							
			//insert data into customer
			$customers_id = DB::table('customers')->insertGetId($customer_data);
			//address
			//address_book
			$address = array(
					'customers_id'	=>	$customers_id,
					'entry_firstname'	=>	$customers_firstname,
					'entry_lastname'	=>	$customers_lastname,
					'entry_street_address'	=>	$request->entry_street_address,
			);
			$customers_default_address_id = DB::table('address_book')->insertGetId($address);
			DB::table('customers')
            ->where('customers_id', $customers_id)
            ->update(['customers_default_address_id' => $customers_default_address_id]);
			$userData = DB::table('customers')->where('customers_id', '=', $customers_id)->first();
			$mail = view('mail.createAccount', ['userData' => $userData])->render();
			// $mail = '958';
			// $this->send_mail('raheelshehzad188@gmail.com', $mail, 'Account created successfully!');
			$this->send_mail($customers_email_address, $mail, 'Account created successfully!');
			
			$responseData = array('success'=>'1', 'data'=>$userData, 'message'=>"Sign Up successfully!");
			$message = "Sign Up successfully!";
			return redirect('page/signup')->with('message',$message);

					
		}	
	}
	public function bubble_sort($arr, $index, $type) {
    $size = count($arr)-1;
    for ($i=0; $i<$size; $i++) {
        for ($j=0; $j<$size-$i; $j++) {
            $k = $j+1;
            if($type == 'less')
            {
	            if ($arr[$k]->$index < $arr[$j]->$index) {
	                // Swap elements at indices: $j, $k
	                list($arr[$j], $arr[$k]) = array($arr[$k], $arr[$j]);
	            }
	        }
	        else
	        {
	        	if ($arr[$k]->$index > $arr[$j]->$index) {
	                // Swap elements at indices: $j, $k
	                list($arr[$j], $arr[$k]) = array($arr[$k], $arr[$j]);
	            }
	        }
        }
    }
    return $arr;
}

    public function category($slug)
    {
    	$data = array();
      if($slug == 'special-products')
      	{ 
    		$data['pageTitle'] = 'Special Products';
    				$page = 1;
					if(isset($_GET['page']))
						$page = $_GET['page'];
    				$sort = 'products_date_added';
					if(isset($_GET['sort']))
						$sort = $_GET['sort'];
    				$type = 'less';
					if(isset($_GET['type']))
						$type = $_GET['type'];
					//speciaal code
			$products = DB::table('specials')->paginate(4,['*'], 'page',$page);
			$newpro= array();
			foreach ($products as $key => $value) {
				$det = $this->getSingleProducts($value->products_id);
				$det->nprice = $value->specials_new_products_price;
				$newpro[] = $det;
			}
			$data['products'] = $this->bubble_sort($newpro, $sort,$type);
					$data['pagi'] = $products; 
					// dd($data);
					return view('plisting',$data);
				}
      
    	$detail = $this->single_categories($slug);
    	
    	if($detail[0])
    	{
    		$detail = $detail[0];
    		$data['detail'] = $detail;
    		$data['pageTitle'] = $detail->categories_name;
    		$parent_id = $detail->categories_id;
    		$language_id     =   1;
		//creatre slug of all
		$all = DB::table('categories')
				->leftJoin('categories_description', 'categories_description.categories_id', '=', 'categories.categories_id')
				->where('categories_description.language_id','=', $language_id)
				->where('categories.parent_id','=', $parent_id)
				->orderBy('sort_order', 'ASC')->get();
						$data['categories'] = $all;
				if(count($all) > 0)
				{
					
				return view('category',$data);
				}
				else
				{
					$page = 1;
					if(isset($_GET['page']))
						$page = $_GET['page'];
					$pro = DB::table('products_to_categories')->where('categories_id', $parent_id)->paginate(4,['*'], 'page',$page);
					$all = array();
					foreach ($pro as $key => $value) {
						$all[] = $this->getSingleProducts($value->products_id);
					}
					$data['products'] = $all;
					$data['pagi'] = $pro;
					// dd($data);
					return view('plisting',$data);
				}


    	}
    }
    public  function sortUrl( $arr)
    {
    	$param = $_GET;
    	foreach ($arr as $key => $value) {
    		$param[$key] = $value;
    	}
    	$actual_link = $_SERVER['REQUEST_SCHEME'] .'://'. $_SERVER['HTTP_HOST'] 
     . explode('?', $_SERVER['REQUEST_URI'], 2)[0].'?'.http_build_query( $param );
    	return $actual_link;
;
    }
    public function applyCopon(Request $request){
    	$code = $_POST['code'];
    	$data = DB::table('coupons')->where('code', $code)->first();
    	$cart = $this->getCart();
    	$price = 0;
        foreach ($cart as $key => $value) {
            $price += $value->customers_basket_quantity*$value->final_price;
        }
    	if($price >= $data->minimum_amount)
    	{
    		$request->session()->put('coupon', $data);
    		$amt = 0;
    		if($data->discount_type ==  'percent')
    		{
    			$amt = $price * ($data->amount/100);
    		}
    		elseif($data->discount_type ==  'fixed_cart')
    		{
    			$amt = $data->amount;
    		}
    		else
    		{
    			$amt = $data->amount;
    		}

    		$request->session()->put('discount', $amt);
    		$responseData = array('success'=>'1', 'data'=>$data, 'message'=>" Coupon apply successfully");
			$userResponse = json_encode($responseData);
    	}
    	else
    	{
	    	$responseData = array('success'=>'0', 'data'=>$data, 'message'=>"Sorry");
			$userResponse = json_encode($responseData);
		}
		return $userResponse;
    }
    public function updateCustomerPassword(Request $request){

    	$uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }

		$customers_id =   $uid;	
		// dd($request);
		$old = $request->old_pass;
		if($request->npass != $request->customers_password)
		{
			$message = "Password not match!";
			return redirect('page/reset-password')->with('error',$message);
		}
		$user = DB::table('customers')->where('customers_id', $uid)->where('isActive', '1')->first();
		// dd($old);
		// dd(Hash::check($old, $user->customers_password));
		if(Hash::check($old, $user->customers_password))
		{
			// die("OK");
			$customers_cpassword =   Hash::make($request->customers_password);
		//$customers_email_address    		   		=   $request->customers_email_address;
		$customers_info_date_account_last_modified 	=   date('y-m-d h:i:s');	
		
		$customer_data = array(
			'customers_password'			 =>  $customers_cpassword
		);
						
		//update into customer
		$userData = DB::table('customers')->where('customers_id', $customers_id)->update($customer_data);
				
		DB::table('customers_info')->where('customers_info_id', $customers_id)->update(['customers_info_date_account_last_modified'   =>   $customers_info_date_account_last_modified]);	
		// die("change");
		$message = "Password change successfully";
			return redirect('page/reset-password')->with('success',$message);
		}
		else
		{
			$message = "Invalid password";
			return redirect('page/reset-password')->with('error',$message);
		}
		
		
		$userData = array();
		$responseData = array('success'=>'1', 'data'=>$userData, 'message'=>"password has been updated successfully");
		$userResponse = json_encode($responseData);
		
		print $userResponse;
		
	}
    public function page($page)
    {
    	$data = array();
    	//checxk register pages
    	$pages = DB::table('pages')
		            ->join('pages_description', 'pages_description.page_id', '=', 'pages.page_id')
		            ->where('pages_description.language_id',1)
		            ->where('pages.slug',$page)->first();
		            if($pages)
		            {
		            	$data['pageTitle'] = $pages->name;
		            	$data['pageData'] = $pages->description;
		            	$page = 'page';
		            }
    	if($page == 'login')
    	{
    		$data['pageTitle'] = 'Login';
    	}
    	else if($page == 'signup')
    	{
    		$data['pageTitle'] = 'Sign Up';
    	}
    	else if($page == 'reset-password')
    	{
    		$uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
    		$data['pageTitle'] = 'Change Password';
    	}
    	else if($page == 'cart')
    	{
    		$data['pageTitle'] = 'Cart';
    		$cart = $this->getCart();
    		// dd($cart);
    		//products_id
    		foreach ($cart as $key => $value) {
    			$cart[$key]->products_id = $this->getSingleProducts($cart[$key]->products_id);
    		}
    		$data['cart'] = $cart;

    	}
    	else if($page == 'checkout')
    	{
    		$uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
    		$data['pageTitle'] = 'Checkout';
    		$cart = $this->getCart();
    		// dd($cart);
    		//products_id
    		foreach ($cart as $key => $value) {
    			$cart[$key]->products_id = $this->getSingleProducts($cart[$key]->products_id);
    		}
    		$data['cart'] = $cart;
    		//$uid
    		//get address
    		$address = DB::table('address_book')->where('address_book.customers_id', '=', $uid)->get();
    		$data['address'] = $address;
    		$shippng_detail = array();
    		if(isset($address[0]))
    		$shippng_detail = $address[0];
    		$dcharges = 0;
    		$is_dilvery = 0;
			$shipAddress = 0;
			if(!isset($_GET['shipAddress']) && !session('shipAddress') && $shippng_detail)
			{
				die("OK");
				$shipAddress= $shippng_detail->address_book_id;

			}
			else
			{
				if(isset($_GET['shipAddress']))
				{
					 $shipAddress = $_GET['shipAddress'];
				}
				else if(session('shipAddress'))
				{
					 $shipAddress = session('shipAddress');
				}
			}
			$billAddress = 0;
			if(!isset($_GET['billAddress']) && !session('billAddress'))
			{
				$billAddress= $shippng_detail->address_book_id;
			}
			else
			{
				if(isset($_GET['billAddress']))
				{
					 $billAddress = $_GET['billAddress'];
				}
				  else if(session('billAddress'))
				{
					 $billAddress = session('billAddress');
				}
			}
			session(['shipAddress'=> $shipAddress]);
			session(['billAddress'=> $billAddress]);
			$shippng_detail = DB::table('address_book')->where('address_book.address_book_id', '=', $shipAddress)->first();
			$data['shipAddress'] = $shippng_detail;
			if(isset($shippng_detail->entry_zone_id) && $shippng_detail->entry_zone_id > 0)
			{

				$zone = DB::table('zones')->where('zone_id',$shippng_detail->entry_zone_id)->first();
				$is_dilvery = $zone->is_dilvery;
				$dcharges = $zone->charges;
				if($is_dilvery == 0)
				{
				 $data['error'] = 'We do not diliver in your area.';
				}
			}
			$billing_detail = DB::table('address_book')->where('address_book.address_book_id', '=', $billAddress)->first();
			$data['shipAddress'] = $shippng_detail;
			$data['billAddress'] = $billing_detail;
			$data['dcharges']= $dcharges;
			$data['is_dilvery']= $is_dilvery;
			// dd($data);

    	}
    	else if($page == 'addressess')
    	{
    		$uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
    		$data['pageTitle'] = 'Address Book';
    		$cart = $this->getCart();
    		// dd($cart);
    		//products_id
    		foreach ($cart as $key => $value) {
    			$cart[$key]->products_id = $this->getSingleProducts($cart[$key]->products_id);
    		}
    		$data['cart'] = $cart;
    		//$uid
    		//get address
    		$address = DB::table('address_book')->where('address_book.customers_id', '=', $uid)->get();
    		// dd($address);
    		$data['address'] = $address;

    	}
    	else if($page == 'orderh')
    	{
    		$data['pageTitle'] = 'Order History';
    		$uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
    		$orders = $this->getOrders($uid);
    		// dd($orders);
    		$data['orders'] = $orders;

    	}
    	else if($page == 'adress')
    	{
    		$uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{

            }
    		
    		$address = DB::table('address_book')->where('address_book.customers_id', '=', $uid)->get();
    		$data['address'] = $address;

    	}
    	else if($page == 'editaddress')
    	{
    		$id = $_GET['id'];
    		$address = DB::table('address_book')->where('address_book.address_book_id', '=', $id)->first();
    		$data['address'] = $address;
    	}
    	else if($page == 'wishlist')
    	{
    		$uid = 0;
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
    		$data['pageTitle'] = 'wishlist';
    		$cart = DB::table('customers_wishlist')->where('customers_wishlist.customers_id', '=', $uid)->get();
    		// dd($cart);
    		//products_id
    		foreach ($cart as $key => $value) {
    			$cart[$key]->products_id = $this->getSingleProducts($cart[$key]->products_id);
    		}
    		$data['list'] = $cart;

    	}
    	return view($page,$data);
    }
    public function getOrders($uid){
		$customers_id            				=   $uid;
		$language_id             				=   1;		
		//$customers_id            				=   '47';		
		
		$order = DB::table('orders')->orderBy('customers_id', 'desc')
				->where([
					['customers_id', '=', $customers_id],
					//['orders_status', '=', '1'],
				])->get();
		//print_r($order);
		if(count($order) > 0){		
			//foreach
			$index = '0';
			foreach($order as $data){
				
				if(!empty($data->coupon_code)){
					//$product_ids = explode(',', $coupons[0]->product_ids);	
					$coupon_code =  $data->coupon_code;
					$order[$index]->coupons = json_decode($coupon_code);
				}
				else{
					$coupon_code =  array();
					$order[$index]->coupons = $coupon_code;
				}
				
				unset($data->coupon_code);
				
				$orders_id	 = $data->orders_id;
				
				$orders_status_history = DB::table('orders_status_history')
						->LeftJoin('orders_status', 'orders_status.orders_status_id', '=', 'orders_status_history.orders_status_id')
						->select('orders_status.orders_status_name', 'orders_status.orders_status_id', 'orders_status_history.comments')
						->where('orders_id', '=', $orders_id)->orderby('orders_status_history.orders_status_history_id', 'ASC')->get();
						
				//print_r($orders_status_history);
				$order[$index]->orders_status_id = $orders_status_history[0]->orders_status_id;
				$order[$index]->orders_status = $orders_status_history[0]->orders_status_name;
				$order[$index]->customer_comments = $orders_status_history[0]->comments;
				
				$total_comments = count($orders_status_history);
				$i = 1;
				
				foreach($orders_status_history as $orders_status_history_data){
					
					/*if($total_comments == $i and !empty($orders_status_history_data->comments)){
						$order[$index]->orders_status_id = $orders_status_history_data->orders_status_id;
						$order[$index]->orders_status = $orders_status_history_data->orders_status_name;
						$order[$index]->customer_comments = $orders_status_history_data->comments;
					}else
					*/
					if($total_comments == $i && $i != 1){
						$order[$index]->orders_status_id = $orders_status_history_data->orders_status_id;
						$order[$index]->orders_status = $orders_status_history_data->orders_status_name;
						$order[$index]->admin_comments = $orders_status_history_data->comments;
					}else{
						$order[$index]->admin_comments = '';
					}
					
					$i++;
				}
								
				$orders_products = DB::table('orders_products')
				->join('products', 'products.products_id','=', 'orders_products.products_id')
				->LeftJoin('products_to_categories','products_to_categories.products_id','=','products.products_id')
				->LeftJoin('categories_description','categories_description.categories_id','=','products_to_categories.categories_id')
				->select('orders_products.*', 'products.products_image as image', 'categories_description.*')
				->where('orders_products.orders_id', '=', $orders_id)->where('categories_description.language_id','=', $language_id)->get();
				$k = 0;
				$product = array();
				foreach($orders_products as $orders_products_data){
					
					$product_attribute = DB::table('orders_products_attributes')
						->where([
							['orders_products_id', '=', $orders_products_data->orders_products_id],
							['orders_id', '=', $orders_products_data->orders_id],
						])
						->get();
						
					$orders_products_data->attributes = $product_attribute;
					$product[$k] = $orders_products_data;
					$k++;
				}
				$data->data = $product;
				$orders_data[] = $data;
			$index++;
			}
				//print_r($product);
				$responseData = array('success'=>'1', 'data'=>$orders_data, 'message'=>"Returned all orders.");
		}else{
				$orders_data = array();
				$responseData = array('success'=>'0', 'data'=>$orders_data, 'message'=>"Order is not placed yet.");
		}
		
		return $orders_data;
	}
    public function payment(Request $request)
    {
    	$pmethod = $_POST['pmethod'];
    	$data = array();
    	$data['post'] = $_POST;
    	if($pmethod == 'stripe')
    	{
    		$page = 'stripe';
    		return view($page,$data);
    	}
    	elseif($pmethod == 'paypal')
    	{
        

    		 app('App\Http\Controllers\PaymentController')->createPayment($request);
        Session::flash('error', "Special message goes here");
        return back()->withError('Some error occur, sorry for inconvenient');
    	}
    	if(session('discount'))
        {
        	$request->session()->forget('discount');

            Session::forget('discount'); // Removes a specific variable

        }

    	

    }
    //addToOrder
	public function addToOrder(){
		$uid = 0;
		dd($_POST);
        
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        $uid = 0;
        
            $value = session('LoginUser');
            // dd($value);
        if(!empty($value)) 
        {
      $uid =  $value->customers_id;
         } else{
                return redirect('page/login?rurl='.$actual_link);

            }
		$shipAddress = $_POST['shipAddress'];
		$billAddress = $_POST['billAddress'];
		$pmethod = $_POST['pmethod'];
		$user = DB::table('customers')->where('customers_id', '=', $uid)->first();
		$saddress = DB::table('address_book')->where('address_book_id', '=', $shipAddress)->first();
		$baddress = DB::table('address_book')->where('address_book_id', '=', $shipAddress)->first();
		
		$date_added								=	date('Y-m-d h:i:s');
		
		$customers_id            				=   $user->customers_id;
		$customers_telephone            		=   $user->customers_telephone;
		$customers_email_address            	=   $user->customers_email_address;	
			
		$delivery_firstname  	          		=   $saddress->entry_firstname;
		$delivery_lastname            			=   $saddress->entry_lastname;
		$delivery_street_address            	=   $saddress->entry_street_address;
		$delivery_suburb            			=   $saddress->entry_suburb;
		$delivery_city            				=   $saddress->entry_city;
		$delivery_postcode            			=   $saddress->entry_postcode;
		
		$delivery = DB::table('zones')->where('zone_name', '=', 1)->get();
		
		if(count($delivery)){
			$delivery_state            				=   $delivery[0]->zone_code;
		}else{
			$delivery_state            				=   'other';
		}
		   
		$delivery_country            			=   $saddress->entry_country_id;
		
		$billing_firstname            			=   $baddress->entry_firstname;
		$billing_lastname            			=   $baddress->entry_lastname;
		$billing_street_address            		=   $baddress->entry_street_address;
		$billing_suburb	            			=   $baddress->entry_suburb;
		$billing_city            				=   $baddress->entry_city;
		$billing_postcode            			=   $baddress->entry_postcode;
		
		$billing = DB::table('zones')->where('zone_name', '=', 1)->get();
		
		if(count($billing)){
			$billing_state            				=   $billing[0]->zone_code;
		}else{
			$billing_state            				=   'other';
		}
		
		$billing_country            			=   $baddress->entry_country_id;
		
		$payment_method            				=   $pmethod;
		$order_information 						=	array();
		
		$cc_type            				=   '';//$request->cc_type;
		$cc_owner            				=   '';//$request->cc_owner;
		$cc_number            				=   '';//$request->cc_number;
		$cc_expires            				=   '';//$request->cc_expires;
		$last_modified            			=   date('Y-m-d H:i:s');
		$date_purchased            			=   date('Y-m-d H:i:s');
		$order_price						=   '';
		$shipping_cost            			=   '';
		$shipping_method            		=   '';
		$orders_status            			=   '';
		$orders_date_finished            	=   '';//$request->orders_date_finished;
		$comments            				=   'comment';//$request->comments;
		//$currency            				=   $request->currency;
		$currency            				=   'euro';
		$currency_value            			=   '';
		//$products_tax						=	$request->products_tax;
		
		//tax info
		$total_tax							=	'';//$request->total_tax;
		
		$products_tax 						= 	1;
		//coupon info
		$is_coupon_applied            		=   0;//$request->is_coupon_applied;
		
		if($is_coupon_applied==1){
			
			$code = array();	
			$coupon_amount = 0;	
			$exclude_product_ids = array();
			$product_categories = array();
			$excluded_product_categories = array();
			$exclude_product_ids = array();
			
			$coupon_amount    =		'';//$request->coupon_amount;
			
			foreach($request->coupons as $coupons_data){
				
				//update coupans		
				$coupon_id = DB::statement("UPDATE `coupons` SET `used_by`= CONCAT(used_by,',$customers_id') WHERE `code` = '".$coupons_data['code']."'");
				
				//dd(DB::getQueryLog());
				
				//Log::useDailyFiles(storage_path().'/logs/debug.log');
		 		//Log::info(['coupon_query'=>DB::getQueryLog()]);
			
			}
			$code = json_encode($request->coupons);
			
		}else{
			$code            					=   '';
			$coupon_amount            			=   '';
		}	
		
		
		//payment methods 
		$payments_setting = DB::table('payments_setting')->get();

		
		if($payment_method == 'paypal' or $payment_method == 'card_payment'){
			
			//braintree transaction with nonce
			/*$is_transaction  = '1'; 			# For payment through braintree
			$nonce    		 =   '';//$request->nonce;
			
			if($payments_setting[0]->braintree_enviroment == '0'){
				$braintree_environment = 'sandbox';	
			}else{
				$braintree_environment = 'production';	
			}
			
			$braintree_merchant_id = $payments_setting[0]->braintree_merchant_id;
			$braintree_public_key  = $payments_setting[0]->braintree_public_key;
			$braintree_private_key = $payments_setting[0]->braintree_private_key;
			
			//brain tree credential
			require_once app_path('braintree/Braintree.php');
			//print_r($result);
			if ($result->success) 
			{
			//print_r("success!: " . $result->transaction->id);
			if($result->transaction->id)
				{
					$order_information = array(
						'braintree_id'=>$result->transaction->id,
						'status'=>$result->transaction->status,
						'type'=>$result->transaction->type,
						'currencyIsoCode'=>$result->transaction->currencyIsoCode,
						'amount'=>$result->transaction->amount,
						'merchantAccountId'=>$result->transaction->merchantAccountId,
						'subMerchantAccountId'=>$result->transaction->subMerchantAccountId,
						'masterMerchantAccountId'=>$result->transaction->masterMerchantAccountId,
						//'orderId'=>$result->transaction->orderId,
						'createdAt'=>time(),
//						'updatedAt'=>$result->transaction->updatedAt->date,
						'token'=>$result->transaction->creditCard['token'],
						'bin'=>$result->transaction->creditCard['bin'],
						'last4'=>$result->transaction->creditCard['last4'],
						'cardType'=>$result->transaction->creditCard['cardType'],
						'expirationMonth'=>$result->transaction->creditCard['expirationMonth'],
						'expirationYear'=>$result->transaction->creditCard['expirationYear'],
						'customerLocation'=>$result->transaction->creditCard['customerLocation'],
						'cardholderName'=>$result->transaction->creditCard['cardholderName']
					);

					//print_r($order_information);
					$payment_status = "success";
					//print_r($result->transaction->status);
				}
				$payment_status = "success";
			} 
			else
				{
					$payment_status = "failed";
				}*/
				$payment_status = "success";
				
		}else if($payment_method == 'stripe'){				#### stipe payment
			
			require_once app_path('stripe/config.php');
			
			//get token from app
			$token  = 'stripe token';//$request->nonce;
			
			$customer = \Stripe\Customer::create(array(
			  'email' => $customers_email_address,
			  'source'  => $token
			));
			
			$charge = \Stripe\Charge::create(array(
			  'customer' => $customer->id,
			  'amount'   => 100*$order_price,
			  'currency' => 'usd'
			));
			
			 if($charge->paid == true){
				 $order_information = array(
						'paid'=>'true',
						'transaction_id'=>$charge->id,
						'type'=>$charge->outcome->type,
						'balance_transaction'=>$charge->balance_transaction,
						'status'=>$charge->status,
						'currency'=>$charge->currency,
						'amount'=>$charge->amount,
						'created'=>date('d M,Y', $charge->created),
						'dispute'=>$charge->dispute,
						'customer'=>$charge->customer,
						'address_zip'=>$charge->source->address_zip,
						'seller_message'=>$charge->outcome->seller_message,
						'network_status'=>$charge->outcome->network_status,
						'expirationMonth'=>$charge->outcome->type
					);
					//print_r($order_information);
					$payment_status = "success";
					
			 }else{
					$payment_status = "failed";	 
			 }
			
		}else if($payment_method == 'cash_on_delivery'){
			
			$payment_method = 'Cash on Delivery';
			$payment_status='success';
			
		} else if($payment_method == 'simplePaypal'){
			
			$payment_method = 'PayPal Express Checkout';
			$payment_status='success';
			$order_information = $request->nonce;
				
		} 
		
		// print $payment_status;
		//check if order is verified
		if($payment_status=='success'){
			//DB::enableQueryLog();
			//update database
			$orders_id = DB::table('orders')->insertGetId(
				[	 'customers_id' => $customers_id,
					 'customers_name'  => $delivery_firstname.' '.$delivery_lastname,
					 'customers_street_address' => $delivery_street_address,
					 'customers_suburb'  =>  $delivery_suburb,
					 'customers_city' => $delivery_city,
					 'customers_postcode'  => $delivery_postcode,
					 'customers_state' => $delivery_state,
					 'customers_country'  =>  $delivery_country,
					 'customers_telephone' => $customers_telephone,
					 'customers_email_address'  => $customers_email_address,
					// 'customers_address_format_id' => $delivery_address_format_id,
					 
					 'delivery_name'  =>  $delivery_firstname.' '.$delivery_lastname,
					 'delivery_street_address' => $delivery_street_address,
					 'delivery_suburb'  => $delivery_suburb,
					 'delivery_city' => $delivery_city,
					 'delivery_postcode'  =>  $delivery_postcode,
					 'delivery_state' => $delivery_state,
					 'delivery_country'  => $delivery_country,
					// 'delivery_address_format_id' => $delivery_address_format_id,
					 
					 'billing_name'  => $billing_firstname.' '.$billing_lastname,
					 'billing_street_address' => $billing_street_address,
					 'billing_suburb'  =>  $billing_suburb,
					 'billing_city' => $billing_city,
					 'billing_postcode'  => $billing_postcode,
					 'billing_state' => $billing_state,
					 'billing_country'  =>  $billing_country,
					 //'billing_address_format_id' => $billing_address_format_id,
					 
					 'payment_method'  =>  $payment_method,
					 'cc_type' => $cc_type,
					 'cc_owner'  => $cc_owner,
					 'cc_number' =>$cc_number,
					 'cc_expires'  =>  $cc_expires,
					 'last_modified' => $last_modified,
					 'date_purchased'  => $date_purchased,
					 'order_price'  => $order_price,
					 'shipping_cost' =>$shipping_cost,
					 'shipping_method'  =>  $shipping_method,
					// 'orders_status' => $orders_status,
					 //'orders_date_finished'  => $orders_date_finished,
					 'currency'  =>  $currency,
					 'currency_value' => $last_modified,
					 'order_information' => json_encode($order_information),
					 'coupon_code'		 =>		$code,
					 'coupon_amount' 	 =>		$coupon_amount,
				 	 'total_tax'		 =>		$total_tax,
				]);
			
			 //orders status history
			 $orders_history_id = DB::table('orders_status_history')->insertGetId(
				[	 'orders_id'  => $orders_id,
					 'orders_status_id' => $orders_status,
					 'date_added'  => $date_added,
					 'customer_notified' =>'1',
					 'comments'  =>  $comments
				]);
				
				//dd(DB::getQueryLog());
				
				/*$query = DB::getQueryLog();
				print_r($query);*/
				
			 foreach($request->products as $products){	
				
				$orders_products_id = DB::table('orders_products')->insertGetId(
				[		 		
					 'orders_id' 		 => 	$orders_id,
					 'products_id' 	 	 =>		$products['products_id'],
					 'products_name'	 => 	$products['products_name'],
					 'products_price'	 =>  	$products['price'],
					 'final_price' 		 =>  	$products['final_price']*$products['customers_basket_quantity'],
					 'products_tax' 	 =>  	$products_tax,
					 'products_quantity' =>  	$products['customers_basket_quantity'],
				]);
				 
				
				if(!empty($products['attributes'])){
					foreach($products['attributes'] as $attribute){
						DB::table('orders_products_attributes')->insert(
						[
							 'orders_id' => $orders_id,
							 'products_id'  => $products['products_id'],
							 'orders_products_id'  => $orders_products_id,
							 'products_options' =>$attribute['products_options'],
							 'products_options_values'  =>  $attribute['products_options_values'],
							 'options_values_price'  =>  $attribute['options_values_price'],
							 'price_prefix'  =>  $attribute['price_prefix']
						]);
						
						
					}
				}
							
			 }
			
			//Log::useDailyFiles(storage_path().'/logs/debug.log');
		 	//Log::info(['orders'=>DB::getQueryLog()]);
			$responseData = array('success'=>'1', 'data'=>array(), 'message'=>"Order has been placed successfully.");
			
			//send order email to user
			
			$order = DB::table('orders')
				->LeftJoin('orders_status_history', 'orders_status_history.orders_id', '=', 'orders.orders_id')
				->LeftJoin('orders_status', 'orders_status.orders_status_id', '=' ,'orders_status_history.orders_status_id')
				->where('orders.orders_id', '=', $orders_id)->orderby('orders_status_history.date_added', 'DESC')->get();
			
		//foreach
		foreach($order as $data){
			$orders_id	 = $data->orders_id;
			
			$orders_products = DB::table('orders_products')
				->join('products', 'products.products_id','=', 'orders_products.products_id')
				->select('orders_products.*', 'products.products_image as image')
				->where('orders_products.orders_id', '=', $orders_id)->get();
				$i = 0;
				$total_price  = 0;
				$product = array();
				$subtotal = 0;
				foreach($orders_products as $orders_products_data){
					$product_attribute = DB::table('orders_products_attributes')
						->where([
							['orders_products_id', '=', $orders_products_data->orders_products_id],
							['orders_id', '=', $orders_products_data->orders_id],
						])
						->get();
						
					$orders_products_data->attribute = $product_attribute;
					$product[$i] = $orders_products_data;
					//$total_tax	 = $total_tax+$orders_products_data->products_tax;
					$total_price = $total_price+$orders_products[$i]->final_price;
					
					$subtotal += $orders_products[$i]->final_price;
					
					$i++;
					//$orders_products_data[] = $orders_products_data;
				}
			//print_r($product);
			$data->data = $product;
			$orders_data[] = $data;
		}
		
			$orders_status_history = DB::table('orders_status_history')
				->LeftJoin('orders_status', 'orders_status.orders_status_id', '=' ,'orders_status_history.orders_status_id')
				->orderBy('orders_status_history.date_added', 'desc')
				->where('orders_id', '=', $orders_id)->get();
					
			$orders_status = DB::table('orders_status')->get();
					
			$ordersData['orders_data']		 	 	=	$orders_data;
			$ordersData['total_price']  			=	$total_price;
			$ordersData['orders_status']			=	$orders_status;
			$ordersData['orders_status_history']    =	$orders_status_history;
			$ordersData['subtotal']    				=	$subtotal;
			
			Mail::send('/mail/orderEmail', ['ordersData' => $ordersData], function($m) use ($ordersData){
				$m->to($ordersData['orders_data'][0]->customers_email_address)->subject('Ecommerce App: Your order has been placed"')->getSwiftMessage()
				->getHeaders()
				->addTextHeader('x-mailgun-native-send', 'true');	
			});
			
		}else if($payment_status == "failed"){
			$responseData = array('success'=>'0', 'data'=>array(), 'message'=>"Error while placing order.");	
		}
		
		$orderResponse = json_encode($responseData);
		print $orderResponse;
	}
}

