@extends('layouts.master')
<?php
  $url = url('/').'/assets/';
?>
@section('content')
<div class="body">
        <div class="container clearfix">
          @include('includes.coupon')
        
          @include('includes.banner')
          
          <div class="h1-cvr">
            <h1 class="grey-bar">
              Buy High Gloss, Glass, Wood Furniture & Sets For Living Room, Dining, Bedroom, Office, Hallway & More
            </h1>
          </div>
          
          <div class="row">
          	@foreach ($categories as $key=>$sing)
            
              <div class="col-md-3 s-thumb">
                <a href="{{ url('category')}}/{{$sing->slug}}">
                  <img src="{{url('/')}}/{{$sing->categories_image}}">
                  <h2>
                    {{$sing->categories_name}}
                  </h2>
                </a>
              
            </div>
            @endforeach
          </div>
          <div class="col-md-12 pd0 box-s ">    
            <div class="sale-title">
              <center>
                <h2>
                  Today's Daily Deals
                </h2>
              </center>
              <span class="view-all">
                <a href="<?= url('/category/special-products') ?>">
                  View All
                </a>
              </span>
            </div>
            <div class="owl-carousel owl-theme">
              <!--slider-->
              <!-- Item slider-->
            <div style="padding: 22px 38px; 0">

              <div class="row">
                <div >
                  <div class="carousel carousel-showmanymoveone slide" id="itemslider">
                    <div class="carousel-inner">

                      
                        @foreach ($specials as $key=>$sing)
                        @if($key == 0)
                        <div class="item active">
                          @else
                      <div class="item">
                        @endif
                        <div class="col-xs-12 col-sm-12 col-md-2">
                          <div class="slide-div">
                      <img src="<?= url('/'); ?>/{{$sing->products_image}}"" class="img-responsive slider-img">
                      <div class="sale-bar">Deal Of The Day</div>
                        <p class="mini-text">
                          <a href="#" title="Elgin 6 Seater High Gloss Convertible Extendable Dining Set">{{$sing->products_name}}</a>
                        </p>
                        <p class="rate-text">
                          <span>WAS<del> £1,{{$sing->products_price}} </del></span>&nbsp &nbsp<span>£1,{{$sing->nprice}}</span>
                        </p>
                      <div class="btn-wrap">
                        <a href="{{ url('/product')}}/{{$sing->slug}}" class="l-btn">
                          <i class="fas fa-heart"></i> View Details
                        </a>
                        <button class="r-btn" onclick="addToCart({{$sing->products_id}}, {{$sing->nprice}})">
                          <i class="fas fa-shopping-cart" ></i> add to cart
                        </button>
                      </div>
                    </div>
                        </div>
                      </div>
                      @endforeach

                    </div>

                    <div id="slider-control">
                    <a class="left1 carousel-control" href="#itemslider" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                    <a class="right1 carousel-control" href="#itemslider" data-slide="next"><i class="fas fa-chevron-right"></i></a>
                  </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Item slider end-->
            </div>
          </div>
      </div>
@endsection
@section('script')
<script type="text/javascript">
  $(document).ready(function(){

$('#itemslider').carousel({ interval: 3000 });

$('.carousel-showmanymoveone .item').each(function(){
var itemToClone = $(this);

for (var i=1;i<6;i++) {
itemToClone = itemToClone.next();

if (!itemToClone.length) {
itemToClone = $(this).siblings(':first');
}

itemToClone.children(':first-child').clone()
.addClass("cloneditem-"+(i))
.appendTo($(this));
}
});
});

</script>
@endsection