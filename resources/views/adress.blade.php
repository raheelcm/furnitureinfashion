<div class="alert alert-warning alert-dismissible hidden">
							<button type="button" class="close" data-dismiss="alert">×</button>
							The primary address cannot be deleted. Please set another address as the primary address and try again. 
							</div>
							<input type="hidden" id="addType">
							<form action="" method="">
							<h3>Address Book Entries</h3>
							@foreach ($address as $key=>$sing)
							<div class="row adress_block">
								<div class="addressblock" id="addressHtml{{$sing->address_book_id}}" style="float: left; padding: 5px; margin: 5px;">{{(isset($sing))?$sing->entry_firstname:''}} {{(isset($sing))?$sing->entry_lastname:''}}<br> {{(isset($sing))?$sing->entry_street_address:''}}</div>
								<div class="add-icons col-md-12">
									<div class="addressicons" style="float: left; padding: 5px;">
										<a onclick="editmodal('{{$sing->address_book_id}}','{{$sing->entry_firstname}}','{{$sing->entry_lastname}}','{{$sing->entry_company}}','{{$sing->entry_street_address}}','{{$sing->entry_city}}','{{$sing->entry_street_address}}','{{$sing->entry_postcode}}')"><span class="ui-icon ui-icon-pencil" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-pencil-alt"></i></span></a>
										<a href="" onclick="deleteShippingAddress({{$sing->address_book_id}})" ><span class="ui-icon ui-icon-trash" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-trash-alt"></i></span></a>
									</div>
									<div class="addressradio" style="float: right; padding-right: 15px;">
										<input type="radio" name="address"@if($key == 0)checked="true"@endif value="{{$sing->address_book_id}}">
									</div>
									@if($key == 0)
									<div class="addressprimary" style="float: right; padding: 5px; font-style: italic;">(primary address)</div>
									@endif
								</div>
								<div class="clear"></div>
							</div>
							@endforeach
							</form>