@extends('layouts.master')
@section('content')
<?php
  $url = url('/').'/assets';
?>
<div class="body">
        <div class="container clearfix">
          @include('includes.coupon')
        
          <div class="hist-a">
            <a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">{{$detail->categories_name}}</a>
          </div>
          
          <div class="catog-t">
            <h3>
              {{$detail->long_head}}
            </h3>
            <p>
              {{$detail->short_detail}}
            </p>
          </div>
          
          <div class="col-md-12 pd0">
            @foreach ($categories as $key=>$sing)
            <!--<div class="col-md-3 s-thumb">
              <a href="{{ url('category')}}/{{$sing->slug}}">
                <img src="{{url('/')}}/{{$sing->categories_image}}">
                <h2>
                  {{$sing->categories_name}}
                </h2>
              </a>
            </div>-->
            <div class="col-md-3 sss-thumb">
              <div class="cat-wrap">
                <a href="{{ url('category')}}/{{$sing->slug}}">
                  <img src="{{url('/')}}/{{$sing->categories_image}}">
                  <p>
                    {{$detail->categories_name}}
                  </p>
                  
                </a>
                <button>SHOP NOW</button>
              </div>
            </div>
            @endforeach
            
          </div>
          
          
          
          <div class="  product-det">
            <h4>
              <b>{{$detail->categories_name}}</b>

            </h4>
            <p>
              {{$detail->full_detail}}
            </p>
            
            <a href="<?= url('/');?>" class="continue-btn text-white">
              Continue Shopping
            </a>
            
          </div>
          
          
        </div>
      </div>          
@endsection