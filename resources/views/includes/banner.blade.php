<div id="myCarousel" class="carousel slide mt-4" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
              <li data-target="#myCarousel" data-slide-to="1"></li>
              <li data-target="#myCarousel" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
            	@foreach ($banners as $key=>$sing)
            	@if ($key == 1)
            	<div class="item active">
              <img src="{{url('/')}}/{{$sing->banners_image}}" alt="" style="width:100%;">
              </div>
@else
    <div class="item">
  <img src="{{url('/')}}/{{$sing->banners_image}}" alt="" style="width:100%;">
  </div>
@endif
              @endforeach
            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
              <span class="sr-only">Next</span>
            </a>
            </div>
                
        