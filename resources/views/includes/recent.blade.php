<?php
  $coupon = app('app\Http\Controllers\PageController')->getRecentProducts();
?>
<div class="row resently_item">
                @foreach ($coupon as $key=>$sing)
                <div class="col-md-2 col-xs-6">
                  <div class="item_div">
                    <div class="resently_itme_img">
                      <a href="{{ url('/product')}}/{{$sing->slug}}"><img src="{{url('/')}}/{{$sing->products_image}}" class="img-responsive img-responsive thumbnail group list-group-image"></a>
                    </div>
                    <div class="item_title text-center">
                      <a href="">{{$sing->products_name}}</a>
                      <div class="amazingcarousel-price"> 
                        <div class="rrp_slider">WAS 
                          <del>£{{$sing->products_price}}</del>
                        </div>£{{($sing->nprice)?$sing->nprice:$sing->products_price}}</div>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>