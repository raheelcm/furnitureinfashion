<?php
  $coupon = app('app\Http\Controllers\PageController')->getCoupons();
?>
<div class="banner-sale  d-md-block">
            @foreach ($coupon as $key=>$sing)
            @if($key%2 ==  0)
            <div class="w-20 bg-red">
            @else
            <div class="w-20 bg-grey">
            @endif
              <a href="#" class="bg-clr-a">
                <div>
                  @if($sing->discount_type ==  'percent')
                  <span class="extra-1">Extra {{$sing->amount}}% Off</span>
                  @endif
                  @if($sing->discount_type ==  'fixed_cart')
                  <span class="extra-1">Extra £{{$sing->amount}} Off</span>
                  @endif
                  @if($sing->minimum_amount >  0)
                  <span class="extra-2">Spend £{{$sing->minimum_amount}} or more</span>
                  @endif
                </div>
                <div>
                </div>
              </a>
            </div>
            @endforeach
                <div class="w-20 bg-red daily" style="height:42px;">
                  <a href="<?= url('/category/special-products'); ?>" >
                    <div class="left-rta">
                      <span class="xtra-hthree" style="color:white;">Daily Deals</span>
                    </div>
                  </a>
                </div>
          </div>
  
               