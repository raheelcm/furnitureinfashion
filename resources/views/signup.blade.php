@extends('layouts.master')
@section('content')
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">Create an account</a>
					</div>
					
					<div class="col-md-12 pd0">
						<div class="">
							<div class="log-t">
								<h2>
									Customer Registration
								</h2>
							</div>	
							<div class="log-t">
								<div class="log-t">
									<p class="thin-text">
										Your Personal Details
									</p>
								</div>
								<form action=" {{url('/register')}}" method="POST">
									 	@if(session('error'))
			 	<div class="alert alert-danger">
				{{ session('error') }}
			</div>
			@endif

				@if(session('message'))
			 	<div class="alert alert-success">
				{{ session('message') }}
			</div>
			@endif

									<div class="ww-50">
										<label>First Name</label>
										<input type="text" name="customers_firstname" required="*" placeholder="First Name" >
									</div>
									<div class="ww-50">
										<label>Last Name</label>
										<input type="text" required="*" name="customers_lastname" placeholder="Last Name" >
									</DIV>
									<div class="ww-50">
										<label>E-mail Address</label>
										<input type="email" required="*" name="customers_email_address" placeholder="E-mail Address" >
									</div>
								
								<div class="log-t">
									<p class="thin-text">
										Your Address
									</p>
								</div>
									<div class="ww-50">
										<label>Street Address</label>
										<input type="text" required="*" name="entry_street_address" placeholder="Street Address" >
									</div>
									<div class="ww-50">
										<label>City</label>
										<input type="text" required="*" placeholder="City" name="entry_city" >
									</DIV>
									<div class="ww-50">
										<label>Post Code</label>
										<input type="text" required="*" name="entry_postcode" placeholder="Post Code" >
									</div>
									
									<div class="ww-50">
										<label>Country</label>
										<input type="text" required="*" placeholder="Country" >
									</div>
								
								<div class="log-t">
									<p class="thin-text">
										Your Contact Information
									</p>
								</div>
									<div class="ww-50">
										<label>Telephone Number</label>
										<input type="tel"  name="customers_telephone" required="*" placeholder="Telephone Number ">
									</div>
								
								<div class="log-t">
									<p class="thin-text">
										Your Password
									</p>
								</div>
									<div class="ww-50">
										<label>Password</label>
										<input type="password" name="customers_password" required="*" placeholder="Password">
									</div>
									<div class="ww-50">
										<label>Password Confirmation</label>
										<input type="password" name="customers_cpassword" required="*" placeholder="Password Confirmation">
									</div>
								
								<p class="agreemnt">
								The data you have provided us above will be used to create an account so you can shop online with Furniture in Fashion.<br>
								If you would like to be the first to hear about sales, offers and promotions, via our marketing e-mails, please tick this box. <input class="check" type="checkbox"><br><br>
								By creating an account you indicate that you have read and accept our Terms/Conditions & Cookies/Privacy Policy. Please tick this box <input  class="check" type="checkbox">
								</p>
							</div>
							
							
							
							<button type="submit" class="login-b">
								Sign Up
							</button>
							</form>
						</div>
					</div>
					
					
				</div>
			</div>
<!--
<div class="container">
		<h1>Registration</h1>
		<div class="formaraa">
			 <form action=" {{url('/register')}}" method="POST">
			 	@if(session('message'))
			 	<div class="alert alert-success">
				{{ session('message') }}
			</div>
				@endif

				  <div class="formBack">
				  	<h2>Register to </h2>
					  <div class="form-group">
					    <label for="">Username</label>
					    <input type="text" class="form-control" id="" placeholder="Pick a username." name="user_name">
					  </div>
					  <div class="form-group">
					    <label for="">Email</label>
					    <input type="Email" class="form-control" id="" placeholder="We won’t share it." name="customers_email_address">
					  </div>
					  <div class="form-group">
					    <label for="pwd">Password</label>
					    <input type="password" class="form-control" id="pwd" placeholder="At least 4 characters." name="customers_password">
					  </div>
				  </div>
				  <div class="checkbox">
				    <label><input type="checkbox"> I have read and agree to  <a href="#">Terms of Use</a> and <a href="#"> Privacy Policy</a>. I also agree that I am at least 18 years of age and to only open one account per person.</label>
				  </div>
				  <input  type="submit" class="btn btn-default" value="REGISTER">
			</form> 
		</div>
	</div>
-->
@endsection