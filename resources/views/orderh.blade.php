@extends('layouts.master')
@section('content')
<?php
  $url = url('/').'/assets';
?>
			 
			 
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> Order</a>
					</div>

						<h1 class="tit">Order Details</h1>
					<div class="pt-5 pb-5">
						<div class="order_div">
							<h3>New Orders</h3>
							<div class="order_table">
								<table class="table_order">
									<thead>
										<tr>
											<th>Name/Address/Phone</th>
											<th>Order</th>
											<th>Total</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody class="order_table_body">
										@foreach ($orders as $key=>$sing)
										<tr>
											<td class="order_name"> <p style="font-weight: 600;margin: 0px;">{{$sing->customers_name}}</p> <p style="font-weight: 600;margin: 0px;">{{$sing->customers_street_address}}</p> </td>
											<td style="padding-left: 19px;">
												<ul>
													@foreach ($sing->data as $key=>$pro)
													<li style="font-weight: bold;">{{$pro->products_name}}</li>
													 @endforeach
												</ul>
											</td>
											<td style="font-weight: bold;padding: 8px;">£
{{$sing->order_price}}</td>
											<td style="padding: 20px 2px;">
												<p class="order_radius">{{$sing->orders_status}}</p>
<!--                         <p class="order_com">Compelete</p> -->
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
					
					
				</div>
			</div>











@endsection