@extends('layouts.master')
@section('script')
<script>
		$(document).ready(function(){
		  $(".menu-bars").click(function(){
			$(".nav-ul-wrapper").slideToggle();
		  });
		  
		});
		</script>
<!--Model-->
<script type="text/javascript">

	function confirm()
	{
		var pmethod = $("input[name='pmethod']:checked").val();
		if(pmethod == 'stripe')
		{
			var html = $('#formFields').html();
			$('#exampleModal').modal('show');
			
			$('#popupFields').html(html);
			return false;
		}
		else
		{
			return true;
			 $('#paypalForm').submit();
		}
	}
	function updateADress()
	{
		var address = $("input[name='address']:checked").val();
		var id = $('#addType').val();
		var html = $('#addressHtml'+address).html();
		$('#'+id).val(address );
		$('.'+id).html(html);
		$("input[name='"+id+"']").val(address);
		$('#myModal').modal('hide');
	}
	function editAddress(id)
	{
		var url = BASE_URL+'/page/editaddress?id='+id;
		//
		$.get(url, function(data, status){
			$('#myModal2').modal('show');
    $('#AddressForm_edit_id').html(data);
  });
	}
	function chngaddress(type)
	{

			$.get(BASE_URL+"/page/adress", function(data, status){
    $('#myModal').modal('show');
    $('#myModal .modal-body').html(data);
    $('#addType').val(type);

  });
	}
	function addmodal()
	{
		$('#modtitle').text('Add Address');
		$('#myModal2').modal('show');

	}
	$('#addressForm').on("submit", "form", function(e){
    e.preventDefault();
    alert('it works!');
    return  false;
});
	function addAddress(id)
	{
		alert(id);
		var datastring = $("#"+id).serialize();
		// console.log(datastring);
		//updateShippingAddress
$.ajax({
    type: "POST",
   	url: BASE_URL+"/"+id,
    data: datastring,
    dataType: "json",
    success: function(data) {
    	location.reload();
    	console.log(data);
    },
    error: function() {
        alert('error handling here');
    }
});
		//addressForm
	}
</script>
<!---stripe script strt--->	
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
  
<script type="text/javascript">
$(function() {
    var $form         = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form         = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                         'input[type=text]', 'input[type=file]',
                         'textarea'].join(', '),
        $inputs       = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid         = true;
        $errorMessage.addClass('hide');
 
        $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
      var $input = $(el);
      if ($input.val() === '') {
        $input.parent().addClass('has-error');
        $errorMessage.removeClass('hide');
        e.preventDefault();
      }
    });
  
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
        number: $('.card-number').val(),
        cvc: $('.card-cvc').val(),
        exp_month: $('.card-expiry-month').val(),
        exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  
  });
  
  function stripeResponseHandler(status, response) {
        if (response.error) {
            $('.error')
                .removeClass('hide')
                .find('.alert')
                .text(response.error.message);
        } else {
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            $form.find('input[type=text]').empty();
            $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
            $form.get(0).submit();
        }
    }
  
});
</script>
<!---stripe script end--->	
<!-- Modal -->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			  <div class="modal-dialog1" role="document" style="max-width: 500px;margin: 23px auto !important;">
			    <div class="modal-content1 bg-white" style="padding: 15px;">
			      <div class="modal-header1">
			        <h3 class="modal-title" id="exampleModalLabel" style="float: left;">Payment Details</h3>
			        <img class="payment_pic" style="margin-left: 88px;" src="http://i76.imgup.net/accepted_c22e0.png">
			      </div>
			      <div class="modal-body1">
			        <form role="form" action="{{ route('stripe.post') }}" method="post" class="require-validation"
                                                     data-cc-on-file="false"
                                                    data-stripe-publishable-key="{{ env('STRIPE_KEY') }}"
                                                    id="payment-form">
                                                    <span id="popupFields"></span>
                                                    <input type="hidden" name="pmethod" value="stripe">
					  <div class="form-group">
					    <label for="exampleInputEmail1">Name on Card</label>
					    <input
                                    class='form-control' size='4' type='text'>
					  </div>
					  <div class="form-group">
					    <label for="exampleInputPassword1">Card Number</label>
					    <input
                                    autocomplete='off' class='form-control card-number' size='20'
                                    type='text'>
					  </div>
					  <div class="row">
					  	<div class="col-md-4">
					  		<div class="form-group">
							    <label for="exampleInputPassword1">CVC</label>
							    <input autocomplete='off'
                                    class='form-control card-cvc' placeholder='ex. 311' size='4'
                                    type='text'>
							</div>
					  	</div>
					  	<div class="col-md-4">
					  		<div class="form-group">
							    <label for="exampleInputPassword1">Expiriaton Month</label>
							    <input
                                    class='form-control card-expiry-month' placeholder='MM' size='2'
                                    type='text'>
							</div>
					  	</div>
					  	<div class="col-md-4">
					  		<div class="form-group">
							    <label for="exampleInputPassword1">Expiration Year</label>
							    <input
                                    class='form-control card-expiry-year' placeholder='YYYY' size='4'
                                    type='text'>
							</div>
					  	</div>
					  </div>
					  <div class='form-row row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>Please correct the errors and try
                                    again.</div>
                            </div>
                        </div>
					  <button class="btn btn-primary btn-lg btn-block" type="submit">Pay Now</button>
					</form>
			      </div>
			    </div>
			  </div>
			</div>			
				<div class="modal" id="myModal">
				  <div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

				      <!-- Modal Header -->
				    	<div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				     	</div>

				      <!-- Modal body -->
				        <div class="modal-body">
				        	<div class="alert alert-warning alert-dismissible">
							<button type="button" class="close" data-dismiss="alert">×</button>
							The primary address cannot be deleted. Please set another address as the primary address and try again. 
							</div>
							<input type="text" id="addType">
							<form action="" method="">
							<h3>Address Book Entries</h3>
							<div class="row adress_block">
								<div class="addressblock" style="float: left; padding: 5px; margin: 5px;">Raheel shehzad<br> Khushab , punjab , pakistan<br> khushab, 41000<br> Vatican City State (Holy See)</div>
								<div class="add-icons col-md-12">
									<div class="addressicons" style="float: left; padding: 5px;">
										<a href="" data-toggle="modal" data-target="#myModal2"><span class="ui-icon ui-icon-pencil" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-pencil-alt"></i></span></a>
										<a href="" ><span class="ui-icon ui-icon-trash" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-trash-alt"></i></span></a>
									</div>
									<div class="addressradio" style="float: right; padding-right: 15px;">
										<input type="radio" checked="checked">
									</div>
									<div class="addressprimary" style="float: right; padding: 5px; font-style: italic;">(primary address)</div>
								</div>
								<div class="clear"></div>
								<div class="addressblock" style="float: left; padding: 5px; margin: 5px;">myhawaii.social<br> Marcia C Davis<br> 1700 Makiki St #105, Honolulu, HI. 96822<br> Honolulu, 96822<br> United Kingdom (Mainland UK Only)</div>
								<div class="add-icons col-md-12">
									<div class="addressicons" style="float: left; padding: 5px;">
										<a href="" data-toggle="modal" data-target="#myModal3"><span class="ui-icon ui-icon-pencil" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-pencil-alt"></i></span></a>
										<a href=""><span class="ui-icon ui-icon-trash" style="display:inline-block;color: black;font-size: 11px;"><i class="fas fa-trash-alt"></i></span></a>
									</div>
									<div class="addressradio" style="float: right; padding-right: 15px;">
										<input type="radio" >
									</div>
								</div>
							</div>
							</form>
				        </div>

				      <!-- Modal footer -->
				      	<div class="contentText">
							<div style="float: right;">
								<a id="btn16" onclick="updateADress()"  class="btn btn-default simpletextbtn"> <span class="plusthick"></span> Update </a>
							</div>
							<div style="float: right;">
								<a id="btn16" onclick="addmodal()" class="btn btn-default simpletextbtn"> <span class="plusthick"></span> Add Address</a>
							</div> 
						</div>

				    </div>
				  </div>
				</div>
			<!-- The Modal end -->
			<!-- The Modal2 -->
			<div class="modal" id="myModal2">
			  	<div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

			      <!-- Modal Header -->
			      	<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				  	</div>

			      <!-- Modal body -->
			      	<div class="modal-body">
			        	<div id="AddressForm_edit_id" class="ui-dialog-content ui-widget-content" style="width: 60%; min-height: 88px; max-height: none; height: auto; margin: auto;">
			        		<form  id="AddressForm_edit" method="">
			        			<h3 id="modtitle">Edit Address</h3>
			        			<p class="fields">
			        				<input type="text" name="entry_firstname" placeholder="First name"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_lastname" placeholder="Last name"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_company" placeholder="Company name" class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_street_address" placeholder="Street address"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text"  name="entry_city" placeholder="City"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text"   name="entry_postcode" placeholder="Postal code"  class="form-control">
			        			</p>
			        			<p class="fields hidden">
			        				<input type="text" value="Vatican City State (Holy See)"  class="form-control">
			        			</p>
			        			<div class="contentText">
									<div style="text-align: center;" id="addressFormSubmit"click="addAddress('updateShippingAddress')" class="btn btn-default simpletextbtn"> Continue </a>
									</div>
								</div>
			        		</form>
			        	</div>
			      	</div>

			      <!-- Modal footer -->
			      	

			    </div>
			  </div>
			</div>
			<!-- The Modal 2 -->
			<!-- The Modal2 -->
			<div class="modal" id="myModal3">
			  	<div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

			      <!-- Modal Header -->
			      	<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				  	</div>

			      <!-- Modal body -->
			      	<div class="modal-body">
			        	<div id="AddressForm_edit" class="ui-dialog-content ui-widget-content" style="width: 60%; min-height: 88px; max-height: none; height: auto; margin: auto;">
			        		<form  action="" method="">
			        			<h3>Edit Address</h3>
			        			<p class="fields">
			        				<input type="text" value="Raheel"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="shehzad"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="" placeholder="Company name" class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="Khushab , punjab , pakistan"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="khushab"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="41000"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" value="Vatican City State (Holy See)"  class="form-control">
			        			</p>
			        			<div class="contentText">
									<div style="text-align: center;" click="addAddress('updateShippingAddress')" id="btn16" class="btn btn-default simpletextbtn"> Continue </a>
									</div>
								</div>
			        		</form>
			        	</div>
			      	</div>

			      <!-- Modal footer -->
			      	

			    </div>
			  </div>
			</div>
			<!-- The Modal 2 -->
			<!-- The Modal2 -->
			<div class="modal" id="myModal4">
			  	<div class="modal-dialog">
				    <div class="modal-content" style="padding: 5px 5px 16px 5px;background: #eee;">

			      <!-- Modal Header -->
			      	<div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal">&times;</button>
				  	</div>

			      <!-- Modal body -->
			      	<div class="modal-body">
			        	<div id="" class="ui-dialog-content ui-widget-content" style="width: 60%; min-height: 88px; max-height: none; height: auto; margin: auto;">
			        		<form id="addShippingAddress"  action="" method="">
			        			<h3>NEW Address</h3>
			        			<p class="fields">
			        				<input type="text" name="entry_firstname" placeholder="First name"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_lastname" placeholder="Last name"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_company" placeholder="Company name" class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text" name="entry_street_address" placeholder="Street address"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text"  name="entry_city" placeholder="City"  class="form-control">
			        			</p>
			        			<p class="fields">
			        				<input type="text"   name="entry_postcode" placeholder="Postal code"  class="form-control">
			        			</p>
			        			<p class="fields hidden">
			        				<input type="text" value="Vatican City State (Holy See)"  class="form-control">
			        			</p>
			        			<div class="contentText">
									<div style="text-align: center;" id="addressFormSubmit"
										 onclick="addAddress('addShippingAddress')" id="btn16" class="btn btn-default simpletextbtn"> Continue </a>
									</div>
								</div>
			        		</form>
			        	</div>
			      	</div>

			      <!-- Modal footer -->
			      	

			    </div>
			  </div>
			</div>
			<!-- The Modal 2 -->
@endsection
@section('content')
<?php
  $url = url('/').'/assets';
?>
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="container-fluid hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">   My Account</a></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">   Address Book</a>
					</div>
					
					<div class="bodyContent">
						<div class="row no-m new-div">
						</div>
						<div class="page-header  register-header-title">
								<div class="acc-head my-account">My Personal Address Book</div>
								<div class="bottom-line"></div>
						</div>
						
						<div class="contentContainer">
							<div class="col-sm-6 no-add">
								<div class="primary-add">Primary Address</div>
									<div class="contentText row">
										<div class="col-sm-12">
											<div class="alert alert-warning">This address is used as the pre-selected shipping and billing address for orders placed on this store.<br><br>This address is also used as the base for product and service tax calculations.
											</div>
										</div>
										@if(isset($address[0]))
										<div class="col-sm-12">
											<div class="panel panel-primary">
												<div class="primary-address-book left-add-box">
													<div class="panel-body">
														<div class="primary-address-box">
								{{$address[0]->entry_firstname}} {{$address[0]->entry_lastname}}<br>{{$address[0]->entry_street_address}}<br> {{$address[0]->entry_city}}, {{$address[0]->entry_postcode}}
														</div>
													</div>
												</div>
											</div>
										</div>
										@endif
									</div>
							</div>
							<div class="col-sm-6 no-add">
								<div class="primary-add">Address Book Entries</div>
									<div class="alert alert-warning"><span class="inputRequirement">NOTE:</span> A maximum of 100 address book entries allowed.
									</div>
									<div class="contentText row">
										@foreach ($address as $key=>$sing)
										@if($key > 0)
										<div class="col-sm-6">
											<div class="panel panel-primary">
												<div class="primary-address-book top-add">
													<div class="primary-address-book-box">
														<div class="panel-body">
								{{$sing->entry_firstname}} {{$sing->entry_lastname}}<br>{{$sing->entry_street_address}}<br> {{$sing->entry_city}}, {{$sing->entry_postcode}}
														</div>
													</div>
													<div class="panel-footer text-center book-edit-bt book-btn">
														<a id="btn1" onclick="editAddress({{$sing->address_book_id}})" class="btn btn-default simpletextbtn"> <span class="glyphicon glyphicon-file"></span> Edit</a> <a id="btn2" onclick="deleteShippingAddress({{$sing->address_book_id}})" class="btn btn-default simpletextbtn"> <span class="glyphicon glyphicon-trash"></span> Delete</a>
													</div>
												</div>
											</div>
										</div>
										@endif
										@endforeach
									</div>
							</div>
							<div class="clearfix"></div>
							<div class="buttonSet row add-book-bottom">
								<div class="col-xs-6">
									<a id="btn1" href="" class="btn btn-default simpletextbtn"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
								</div>
								<div class="col-xs-6 text-right">
									<button id="cart_btn" data-toggle="modal" data-target="#myModal4" class="btn btn-success simpletextbtn"> <span class="glyphicon glyphicon-chevron-right"></span> Add Address</button>
								</div>
							</div>
						</div>
						
						
					</div>
					
					
					
					
					
					
					
					
					
				</div>
			</div>
@endsection