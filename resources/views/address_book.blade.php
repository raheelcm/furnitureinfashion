@extends('layouts.master')
@section('content')
<?php
  $url = url('/').'/assets';
?>
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="container-fluid hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">   My Account</a></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">   Address Book</a>
					</div>
					
					<div class="bodyContent">
						<div class="row no-m new-div">
						</div>
						<div class="page-header  register-header-title">
								<div class="acc-head my-account">My Personal Address Book</div>
								<div class="bottom-line"></div>
						</div>
						
						<div class="contentContainer">
							<div class="col-sm-6 no-add">
								<div class="primary-add">Primary Address</div>
									<div class="contentText row">
										<div class="col-sm-12">
											<div class="alert alert-warning">This address is used as the pre-selected shipping and billing address for orders placed on this store.<br><br>This address is also used as the base for product and service tax calculations.
											</div>
										</div>
										@if(isset($address[0]))
										<div class="col-sm-12">
											<div class="panel panel-primary">
												<div class="primary-address-book left-add-box">
													<div class="panel-body">
														<div class="primary-address-box">
								{{$address[0]->entry_firstname}}1 {{$address[0]->entry_lastname}}<br>{{$address[0]->entry_street_address}}<br> {{$address[0]->entry_city}}, {{$address[0]->entry_postcode}}
														</div>
													</div>
												</div>
											</div>
										</div>
										@endif
									</div>
							</div>
							<div class="col-sm-6 no-add">
								<div class="primary-add">Address Book Entries</div>
									<div class="alert alert-warning"><span class="inputRequirement">NOTE:</span> A maximum of 100 address book entries allowed.
									</div>
									<div class="contentText row">
										@if(isset($address[0]))
										<div class="col-sm-6">
											<div class="panel panel-primary">
												<div class="primary-address-book top-add">
													<div class="primary-address-book-box">
														<div class="panel-body">
															OK
								{{$address[0]->entry_firstname}} {{$address[0]->entry_lastname}}<br>{{$address[0]->entry_street_address}}<br> {{$address[0]->entry_city}}, {{$address[0]->entry_postcode}}
														</div>
													</div>
													<div class="panel-footer text-center book-edit-bt book-btn">
														<a id="btn1" href="https://www.furnitureinfashion.net/address_book_process.php?edit=327462" class="btn btn-default simpletextbtn"> <span class="glyphicon glyphicon-file"></span> Edit</a> <a id="btn2" href="https://www.furnitureinfashion.net/address_book_process.php?delete=327462" class="btn btn-default simpletextbtn"> <span class="glyphicon glyphicon-trash"></span> Delete</a>
													</div>
												</div>
											</div>
										</div>
										@endif
									</div>
							</div>
							<div class="clearfix"></div>
							<div class="buttonSet row add-book-bottom">
								<div class="col-xs-6">
									<a id="btn1" href="" class="btn btn-default simpletextbtn"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>
								</div>
								<div class="col-xs-6 text-right">
									<button id="cart_btn" type="submit" class="btn btn-success simpletextbtn"> <span class="glyphicon glyphicon-chevron-right"></span> Continue</button>
								</div>
							</div>
						</div>
						
						
					</div>
					
					
					
					
					
					
					
					
					
				</div>
			</div>
@endsection