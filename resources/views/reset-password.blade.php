@extends('layouts.master')
@section('content')
<?php
  $url = url('/').'/assets';
?>
<div class="body">
				<div class="container clearfix">
					<div class="banner-sale">
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-grey">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
						<div class="w-20 bg-red">
							<a href="#" class="bg-clr-a">
								<div>
									<span class="extra-1">Extra £25 Off</span>
									<span class="extra-2">Spend £250 or more</span>
								</div>
							</a>
						</div>
					</div>
				
					<div class="container-fluid hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">  My Account</a>&nbsp &nbsp / &nbsp &nbsp <a href="#"> Change Password</a>
					</div>
					
					<div class="row no-m new-div"></div>
					<div class="page-header  register-header-title">
						<div class="acc-head my-account">My Password</div>
						<div class="bottom-line"></div>
					</div>

					<form action="<?= url('/updateCustomerPassword'); ?>" method="post"  class="form-horizontal">
						<div class="contentContainer">
							<div class="row">
							<div class="col-sm-9 acc-info-i">
							@if(session('error'))
			 	<div class="alert alert-danger">
				{{ session('error') }}
			</div>
			@endif
							@if(session('success'))
			 	<div class="alert alert-success">
				{{ session('success') }}
			</div>
			@endif
			</div>
			</div>
							<p class="inputRequirement text-right">
								<span class="glyphicon glyphicon-asterisk inputRequirement"></span> Required information
							</p>
							<div class="contentText">
								<div class="form-group has-feedback">
									<label for="inputCurrent" class="control-label col-sm-2">Current Password</label>
									<div class="col-sm-9 acc-info-i">
										<input type="password" name="old_pass" required=""  autofocus="autofocus" id="inputCurrent" placeholder="Current Password" class="form-control" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAASCAYAAABSO15qAAAAAXNSR0IArs4c6QAAAUBJREFUOBGVVE2ORUAQLvIS4gwzEysHkHgnkMiEc4zEJXCMNwtWTmDh3UGcYoaFhZUFCzFVnu4wIaiE+vvq6+6qTgthGH6O4/jA7x1OiCAIPwj7CoLgSXDxSjEVzAt9k01CBKdWfsFf/2WNuEwc2YqigKZpK9glAlVVwTTNbQJZlnlCkiTAZnF/mePB2biRdhwHdF2HJEmgaRrwPA+qqoI4jle5/8XkXzrCFoHg+/5ICdpm13UTho7Q9/0WnsfwiL/ouHwHrJgQR8WEwVG+oXpMPaDAkdzvd7AsC8qyhCiKJjiRnCKwbRsMw9hcQ5zv9maSBeu6hjRNYRgGFuKaCNwjkjzPoSiK1d1gDDecQobOBwswzabD/D3Np7AHOIrvNpHmPI+Kc2RZBm3bcp8wuwSIot7QQ0PznoR6wYSK0Xb/AGVLcWwc7Ng3AAAAAElFTkSuQmCC&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;"> <span><span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span></span>
									</div>
							</div>
							<div class="form-group has-feedback">
								<label for="inputNew" class="control-label col-sm-2">New Password</label>
								<div class="col-sm-9 acc-info-i">
									<input type="password" name="npass" required="" id="inputNew" placeholder="New Password" class="form-control" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;"> <span><span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span></span>
								</div>
							</div>
							<div class="form-group has-feedback">
								<label for="inputConfirmation" class="control-label col-sm-2">Password Confirmation</label>
								<div class="col-sm-9 acc-info-i">
									<input type="password" name="customers_password" required="" id="inputConfirmation" placeholder="Password Confirmation" class="form-control" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACIUlEQVQ4EX2TOYhTURSG87IMihDsjGghBhFBmHFDHLWwSqcikk4RRKJgk0KL7C8bMpWpZtIqNkEUl1ZCgs0wOo0SxiLMDApWlgOPrH7/5b2QkYwX7jvn/uc//zl3edZ4PPbNGvF4fC4ajR5VrNvt/mo0Gr1ZPOtfgWw2e9Lv9+chX7cs64CS4Oxg3o9GI7tUKv0Q5o1dAiTfCgQCLwnOkfQOu+oSLyJ2A783HA7vIPLGxX0TgVwud4HKn0nc7Pf7N6vV6oZHkkX8FPG3uMfgXC0Wi2vCg/poUKGGcagQI3k7k8mcp5slcGswGDwpl8tfwGJg3xB6Dvey8vz6oH4C3iXcFYjbwiDeo1KafafkC3NjK7iL5ESFGQEUF7Sg+ifZdDp9GnMF/KGmfBdT2HCwZ7TwtrBPC7rQaav6Iv48rqZwg+F+p8hOMBj0IbxfMdMBrW5pAVGV/ztINByENkU0t5BIJEKRSOQ3Aj+Z57iFs1R5NK3EQS6HQqF1zmQdzpFWq3W42WwOTAf1er1PF2USFlC+qxMvFAr3HcexWX+QX6lUvsKpkTyPSEXJkw6MQ4S38Ljdbi8rmM/nY+CvgNcQqdH6U/xrYK9t244jZv6ByUOSiDdIfgBZ12U6dHEHu9TpdIr8F0OP692CtzaW/a6y3y0Wx5kbFHvGuXzkgf0xhKnPzA4UTyaTB8Ph8AvcHi3fnsrZ7Wore02YViqVOrRXXPhfqP8j6MYlawoAAAAASUVORK5CYII=&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: auto;"> <span><span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span></span>
								</div>
							</div>
							</div>
							<div class="buttonSet row add-book-bottom">
								<div class="col-xs-6">
									<a id="btn1" href="<?= url('/page/account');?>" class="btn btn-default simpletextbtn"> <span class="glyphicon glyphicon-chevron-left"></span> Back</a>
								</div>
								<div class="col-xs-6 text-right">
									<button id="cart_btn" type="submit" class="btn btn-success simpletextbtn"> <span class="glyphicon glyphicon-chevron-right"></span> Continue
									</button>
								</div>
							</div>
						</div>
					</form>
					
					
				</div>
			</div>
@endsection