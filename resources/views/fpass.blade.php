@extends('layouts.master')
@section('content')
<div class="main">
			<div class="body">

				<div class="container clearfix">
					@include('includes.coupon')
				
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#">Login</a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> Password Forgotten</a>
					</div>
					<div class="row" id="alert" ></div>

					<div id="bodyWrapper" class="container no-p ">
						<div id="bodyContent">

							<div class="forgot-l col-md-6 col-sm-12">
								<div class="page-header">
 									<div class="page-head-forgot col-sm-12 ">I've Forgotten My Password!
										<div class="page-head-forgot-info forgot-text">If you've forgotten your password, enter your e-mail address below and we'll send you instructions on how to securely change your password.</div>
									</div>
									<form class="form-horizontal">
											<div class="contentContainer">
												<div class="contentText" style="overflow: hidden;
    width: 100%;">
													<div class="form-group has-feedback forgot-l-c col-sm-12">
														<label class="control-label col-sm-6 col-lg-4 col-xs-12">E-Mail Address</label>
														<div class="col-sm-6 pull-right col-lg-8 col-xs-12">
															<input type="email" id="forgt_email" placeholder="E-Mail Address" class="form-control">
															<span><span class="glyphicon glyphicon-asterisk form-control-feedback inputRequirement"></span></span>
														</div>
													</div>
												</div>
												<div class="buttonSet row forgot-l-btn" style="overflow: hidden;width: 100%;display: block;">
													<div class="col-xs-6">
														<div class="back-f-btn">
															<a id="btn1" href="" class="btn btn-default simpletextbtn">
																<span class="glyphicon glyphicon-chevron-left"></span> Back
															</a>
														</div>
													</div>
													<div class="col-xs-6 text-right">
														<button type="button" onclick="forget_pass()"> <span class="glyphicon glyphicon-chevron-right"></span> Continue
														</button>
													</div>
												</div>  
											</div>
									</form>
								</div>
							</div>

							<div class="forgot-r col-md-6  col-sm-12">
								<div class="create-account-link col-sm-12 col-md-12">
									<div class="panel panel-info">
										<div class="panel-body login-t-b">
											<div class="login-f-h">
												<h1>New Customer</h1>
											</div>

											<div class="intro-copy intro-r">
												<p>By creating an account at New Furniture in Fashion you will be able to shop faster, be up to date on an orders status, and keep track of the orders you have previously made.</p><p></p>
											</div>
											<div class="form-l-b acc-d-l no-mtop16">
												<p class="text-right regstr-btn pull-left">
													<a class="btn btn-primary btn-block" href="" id="btn2">
														<span class="log-sign"></span> <span class="glyphicon glyphicon-chevron-right"></span> Continue OK
													</a>
												</p>
											</div>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>
					
					
					
					
					
				</div>
			</div>
		</div>
@endsection    
@section('script')
<script type="text/javascript">
function forget_pass()
{
	var url = BASE_URL+"/processForgotPassword?customers_email_address="+$('#forgt_email').val();
	var settings = {
  "url": url,
  "method": "POST",
  "timeout": 0,
};
$('#alert').html('');
$.ajax(settings).done(function (response) {
	// alert(response);
var html = "";
var obj = JSON.parse(response);
	console.log(obj);
  if(obj.success == "1")
  {
  	html = '<div class="alert alert-success" style="width: 100%;" role="alert">'+obj['message']+'</div>';
  }
  else
  {
  	html = '<div class="alert alert-danger"  style="width: 100%;" role="alert">'+obj['message']+'</div>';
  }
  $('#alert').html(html);
});
return false;
}
</script>
@endsection