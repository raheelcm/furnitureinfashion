@extends('layouts.master')
@section('content')
<?php
  $url = url('/').'/assets/';
?>
<div class="body">
  <div class="container clearfix">
      <!-- banner -->
					@include('includes.coupon')
      <!-- link top -->
					<div class="hist-a">
						<a href="#"><i class="fas fa-home"></i></a> &nbsp &nbsp / &nbsp &nbsp <a href="#"> Dining Table And Chairs Sets </a>&nbsp &nbsp / &nbsp &nbsp <a href="#">Dining Tables</a>
					</div>
	    <!-- body top content -->
					<div class="no-m new-div">
						<div class="pt-4 pb-4 cat-textt unique-title">
								<div class="cat-tile line-t ss">
									<h1 class="unique-keyword">
									Dining Tables UK, Extending Glass, Wooden, Gloss &amp; Marble Dining Table
									</h1>
								</div>
								<div class="top-content-main-mob">Looking for contemporary dining tables? At Furniture in Fashion we have a wide range of high quality cheap dining tables at pocket friendly prices. Add style and comfort to your dining room with our modern dining tables available in glass, high gloss, wood and marble.</div>	
						</div>
							<!-- 1002 result -->
						<div class="result_pages">
										<div class="product_list_pagi_ttx_new">
											<strong>1002</strong>&nbsp; RESULTS
										</div>
										<div class="sortby_form_new">
											<div class="sort-list-text">
												<p>Sort by : </p>
												<ul>
													<li><a href="">Most Popular</a></li>
													<li><a class="{{((isset($_GET['sort']) && $_GET['sort'] == "products_date_added")?'sel_opt':'')}}" href="<?=app('app\Http\Controllers\PageController')->sortUrl(array('sort'=>'products_date_added','type'=>'less'))?>">New Arrival</a></li>
													<li><a class="{{((isset($_GET['sort']) && $_GET['sort'] == "nprice" && $_GET['type'] == "less")?'sel_opt':'')}}" href="<?=app('app\Http\Controllers\PageController')->sortUrl(array('sort'=>'nprice','type'=>'less'))?>">Price Low to High</a></li>
													<li><a class="{{((isset($_GET['sort']) && $_GET['sort'] == "nprice" && $_GET['type'] == "greater")?'sel_opt':'')}}" href="<?=app('app\Http\Controllers\PageController')->sortUrl(array('sort'=>'nprice','type'=>'greater'))?>">Price High to Low</a></li>
												</ul>
											</div>
											<div class="pagi-prod-new">
												<div class="product_listing_pagination_new">
														<div class="product_listing_pagination">
															<ul>
															   {{ $pagi->appends(request()->input())->links() }}

															</ul>
													</div>
												</div>
											</div>
										</div>
						</div>
							<!-- 1002 result end-->				
							<!-- products -->
						<div class="pt-4 pb-4">
								<div class="row products">
									@foreach ($products as $key=>$sing)
									<div class="item list-group-item col-sm-3 col-xs-12 prodt_list new-produt-design">
										<div class="productHolder equal-height" style="height: 569px;"> 
									   		<div class="piGal no-gall pintogle">
									   			<a href="{{ url('/product')}}/{{$sing->slug}}"><img width="300" height="300" src="{{url('/')}}/{{$sing->products_image}}" class="img-responsive img-responsive thumbnail group list-group-image" itemprop="image"></a>
											</div>
											<div class="caption-daily">
												<div class="caption">
													<div class="clearfix">
													</div>
													  <h3 class="group inner list-group-item-heading">
													  	<a href="">{{$sing->products_name}}</a> 
													  </h3>
													  <hr>
													  <div class="price_area">
													  	<div class="row price_div">
													  		<div class="price">
													  			<div class="fprice">
													  				£{{($sing->nprice)?$sing->nprice:$sing->products_price}}
													  			</div>
													  			@if($sing->nprice)
														  		<div class="new-rrp-div">
														  			<div class="new-rrp">RRP
														  			</div> 
														  			<del>£{{$sing->products_price}}</del>
														  		</div>
														  		@endif
													  		</div>
													  		<div class="saving_area">
																<div class="save_listing_child">
																	<span class="detail_save">Saving</span>
																	<span class="detail_pri"> £179.99</span>
																	<span class="detail_today">Today</span>
																</div>
																</div>
													  	</div>
													  	<div class="offers_div">
													  		<img src="<?=$url;?>images/tik.png"><a class=" btn-default btn-sm wishlist-btn appendix btn131" href="">EXTRA £50 OFF ALL ORDERS OVER £500 </a>
													  	</div>
													  	<div class="offers_div">
													  		<img src="<?=$url;?>images/tik.png"><a class=" btn-default btn-sm wishlist-btn appendix btn131" href="">FREE STANDARD DELIVERY TO MOST OF UK </a>
													  	</div>
													  	<div class="btn_list">
													  		<div class="left_btn">
													  			<a href="{{ url('/product')}}/{{$sing->slug}}"><i class="far fa-heart"></i> VIEW DETAILES</a>
													  		</div>
													  		<div class="right_btn">
													  			<a onclick="addToCart({{$sing->products_id}}, {{($sing->nprice)?$sing->nprice:$sing->products_price}})"><i class="fas fa-shopping-cart"></i> Add to cart</a>
													  		</div>
													  	</div>
													  </div>
												</div>
											</div>  
										</div>
									</div>
									@endforeach
								</div>
						</div>
							<!-- produc end -->
							<!-- 1002 result -->
						<div class="result_pages">
										<div class="product_list_pagi_ttx_new">
											<strong>1002</strong>&nbsp; RESULTS
										</div>
										<div class="sortby_form_new">
											<div class="sort-list-text">
												<p>Sort by : </p>
												<ul>
													<li><a class="sel_opt" href="">Most Popular</a></li>
													<li><a href="">New Arrival</a></li>
													<li><a href="">Price Low to High</a></li>
													<li><a href="">Price High to Low</a></li>
												</ul>
											</div>
											<div class="pagi-prod-new">
												<div class="product_listing_pagination_new">
													<div class="product_listing_pagination"><ul><li><strong>1</strong></li><li><a href="" class="pageResults" title=" Page 2 ">2</a></li><li><a href="" class="pageResults" title=" Page 3 ">3</a></li><li><a href="" class="prevnext" title=" Next Page "><img src="<?=$url;?>images/nav-arrow-right-16.png" alt=" Next Page "></a></li></ul></div>
													<span><a title="View All" href="">VIEW ALL</a></span>
												</div>
											</div>
										</div>
						</div>
							<!-- 1002 result end-->	
							<!-- resently view -->
						<div class="resently_block">
							<div class="resently_heading text-center">
								<h1>Recently Viewed</h1>
							</div>
							@include('includes.recent')
						</div>
					</div>
  </div>        
</div>

@endsection